enum RoomType { dorm, rent, shared, whole, apartment }
enum SortType { newest, lowToHigh, highToLow }
enum Gender { all, male, female }
enum RoomUtility {pet, tivi, closet, bed, mezz, wash, frid, kit, heat, wc, park, secure, window, wifi, free, privacy}
enum FilterType {price, facility, type, capacity, sort}

extension PrettyEnum on Object {
  String get enumValue => this.toString().split('.').last;
}

extension FormatNumberPrice on double {
  String get formattedValue => this.toStringAsFixed(this.truncateToDouble() == this ? 0 : 2);
}