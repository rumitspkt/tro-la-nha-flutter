import 'dart:io';

import 'package:flutter/material.dart';

class Const {
  static const double MARGIN = 12;
  static const double MARGIN_LARGE = 25;
  static const int secondaryColor = 0xffceed49;
  static const int primaryColor = 0xff5fbbfd;
  static const int lightPrimaryColor = 0xffb2dffe;

  static const TextStyle styleH2 = TextStyle(color: Colors.black, fontSize: 18);
  static const TextStyle styleH3 = TextStyle(color: Colors.grey, fontSize: 13);
  static const TextStyle styleHint = TextStyle(color: Colors.blueAccent, fontSize: 11);
  static final String appId = Platform.isAndroid
      ? 'ca-app-pub-5685714240703103~5415901723'
      : 'ca-app-pub-5685714240703103~7563552910';


  // Key shared preference
  static const String KEY_CACHED_USER = 'KEY_CACHED_USER';
  static const String KEY_SELECTED_CITY = 'KEY_SELECTED_CITY';
  static const String KEY_HISTORY_SEARCH = 'KEY_HISTORY_SEARCH';

}