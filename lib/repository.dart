import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tiengviet/tiengviet.dart';
import 'package:trolanha/models/simple_model.dart';
import 'package:trolanha/shared_preferences_helper.dart';

import 'Const.dart';

class Repository {
  // singleton
  static final Repository _singleton = Repository._internal();

  factory Repository() => _singleton;

  Repository._internal();

  static Repository get shared => _singleton;

  // Address
  List<SimpleModel> cities = [];
  SimpleModel selectedCity;

  SimpleModel city(String cityId) {
    for (int i = 0; i < cities.length; i++) {
      if (cities[i].id == cityId) {
        return cities[i];
      }
    }
    return null;
  }

  List<SimpleModel> districts(String cityId) {
    for (int i = 0; i < cities.length; i++) {
      if (cities[i].id == cityId) {
        return cities[i].items;
      }
    }
    return [];
  }

  SimpleModel district(String cityId, String districtId) {
    List<SimpleModel> districts = this.districts(cityId);
    for (int i = 0; i < districts.length; i++) {
      if (districts[i].id == districtId) {
        return districts[i];
      }
    }
    return null;
  }

  List<String> getAddressIds(String city, String district) {
    String unSignCity = tiengviet(city).toLowerCase();
    String unSignDistrict = tiengviet(district).toLowerCase();

    List<String> ids = [];
    for (int i = 0; i < cities.length; i++) {
      SimpleModel city = cities[i];
      if (city.text == unSignCity ||
          city.text.contains(unSignCity) ||
          unSignCity.contains(city.text)) {
        ids.add(city.id);
        for (int j = 0; j < city.items.length; j++) {
          SimpleModel district = city.items[j];
          if (district.text == unSignDistrict ||
              district.text.contains(unSignDistrict) ||
              unSignDistrict.contains(district.text)) {
            ids.add(district.id);
            return ids;
          }
        }
      }
    }
    return ids;
  }

  Future<void> fetchCitiesAndCache() async {
    print("okok ne empty");
    List<SimpleModel> cities = [];
    QuerySnapshot docs =
        await Firestore.instance.collection("cities").getDocuments();
    for (int i = 0; i < docs.documents.length; i++) {
      DocumentSnapshot doc = docs.documents[i];
      SimpleModel city = SimpleModel.fromMap(doc.data, doc.documentID);
      print('alo alo: ${city.displayText}');
      QuerySnapshot subDocs =
          await doc.reference.collection("districts").getDocuments();

      List<SimpleModel> districts = [];
      subDocs.documents.forEach((subDoc) {
        SimpleModel district =
            SimpleModel.fromMap(subDoc.data, subDoc.documentID);
        districts.add(district);
      });

      print("okok dis ${districts.length}");
      city.items = districts;
      cities.add(city);
    }

    this.cities.clear();
    this.cities.addAll(cities);

    String selectedCityId = await PreferencesHelper.getString(Const.KEY_SELECTED_CITY);
    if (selectedCityId.isEmpty) {
      this.selectedCity = cities[0];
      await PreferencesHelper.setString(Const.KEY_SELECTED_CITY, this.selectedCity.id);
      print('day neeeeeee: ${this.selectedCity.banners.length}');
    } else {
      for (int i = 0 ; i < cities.length; i ++) {
        if (selectedCityId == cities[i].id) {
          this.selectedCity = cities[i];
          return;
        }
      }
    }

  }

//===================

}
