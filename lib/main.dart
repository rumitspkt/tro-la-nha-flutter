import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:edge_alert/edge_alert.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:trolanha/customs/fab_bottom_app_bar.dart';
import 'package:trolanha/customs/fab_layout.dart';
import 'package:trolanha/customs/fab_with_icons.dart';
import 'package:trolanha/repository.dart';
import 'package:trolanha/widgets/account/login_widget.dart';
import 'package:trolanha/widgets/home_account_widget.dart';
import 'package:trolanha/widgets/home_friend_widget.dart';
import 'package:trolanha/widgets/home_map_widget.dart';
import 'package:trolanha/widgets/home_search_widget.dart';
import 'package:trolanha/widgets/room_detail/room_detail_widget.dart';
import 'package:trolanha/widgets/room_post/post_room_widget.dart';
import 'package:trolanha/widgets/room_search/room_search_widget.dart';

import 'Const.dart';
import 'models/room_model.dart';
import 'models/user_model.dart';
import 'shared_preferences_helper.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  // Set `enableInDevMode` to true to see reports while in debug mode
  // This is only to be used for confirming that reports are being
  // submitted as expected. It is not intended to be used for everyday
  // development.
  Crashlytics.instance.enableInDevMode = true;

  // Pass all uncaught errors from the framework to Crashlytics.
  FlutterError.onError = Crashlytics.instance.recordFlutterError;

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics analytics = FirebaseAnalytics();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    // Request location permission
    requestLocationPermission();
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
    );
  }

  void requestLocationPermission() async {
    if (!await Permission.location.isGranted) {
      [
        Permission.location,
        Permission.locationAlways,
        Permission.locationWhenInUse
      ].request();
    }
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;

  List<StatefulWidget> _children;
  final CollectionReference _users = Firestore.instance.collection("users");
  final FirebaseAnalytics analytics = FirebaseAnalytics();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  void _selectedTab(int index) async {
    if (index == 3) {
      FirebaseUser user = await FirebaseAuth.instance.currentUser();
      if (user == null) {
        bool result = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => LoginWidget()),
        );
        if (result == null || !result) {
          _fabBottomAppBarKey.currentState.updateIndex(_selectedIndex);
          return;
        }
        _homeAccountKey.currentState.getUserCached();
      } else {
        _users.document(user.uid).get().then((value) async {
          if (value.exists) {
            UserModel user = UserModel.fromMap(value.data, value.documentID);
            await PreferencesHelper.setString(
                Const.KEY_CACHED_USER, jsonEncode(user.toMap()));
            _homeAccountKey.currentState.getUserCached();
          }
        });
      }
    }
    setState(() {
      _selectedIndex = index;
    });
  }

  void _selectedFab(int index) {
    switch (index) {
      case 0:
        break;
      case 1:
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PostRoomWidget()),
        );
        break;
      case 2:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => RoomSearchWidget(),
        ));
        break;
    }
  }

  final GlobalKey<HomeSearchState> _homeSearchKey =
      GlobalKey<HomeSearchState>();

  final GlobalKey<HomeFriendState> _homeFriendKey =
      GlobalKey<HomeFriendState>();

  final GlobalKey<HomeMapState> _homeMapKey = GlobalKey<HomeMapState>();

  final GlobalKey<HomeAccountState> _homeAccountKey =
      GlobalKey<HomeAccountState>();

  final GlobalKey<FABBottomAppBarState> _fabBottomAppBarKey =
      GlobalKey<FABBottomAppBarState>();

  @override
  void initState() {
    this._children = [
      HomeSearchWidget(
        key: _homeSearchKey,
        onSearchClick: () {},
        onPostClick: handleClickPostRoom,
        onMapClick: () {
          analytics.logEvent(name: 'view_search_near_by');
          setState(() {
            _selectedIndex = 2;
            _fabBottomAppBarKey.currentState.updateIndex(_selectedIndex);
          });
        },
      ),
      HomeFriendWidget(
        key: _homeFriendKey,
      ),
      HomeMapWidget(
        key: _homeMapKey,
      ),
//      HomeMessageWidget(),
      HomeAccountWidget(
        key: _homeAccountKey,
        onSignOut: () {
          setState(() {
            _selectedIndex = 0;
            _fabBottomAppBarKey.currentState.updateIndex(_selectedIndex);
          });
        },
      )
    ];
    super.initState();
    initAppData();
    FirebaseAdMob.instance.initialize(appId: Const.appId);

    // Firebase cloud messaging
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("fcm onMessage: $message");
        EdgeAlert.show(context,
            backgroundColor: Colors.blueAccent,
            duration: EdgeAlert.LENGTH_VERY_LONG,
            title: message['notification']['title'],
            description: message['notification']['body'],
            gravity: EdgeAlert.TOP);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("fcm onLaunch: $message");
        handleClickNotification(message);
//        _navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("fcm onResume: $message");
        handleClickNotification(message);
//        _navigateToItemDetail(message);
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) async {
      assert(token != null);
      print("fcm token: $token");
      FirebaseUser user = await FirebaseAuth.instance.currentUser();
      if (user != null) {
        Firestore.instance.collection("users").document(user.uid).updateData({
          'fcmToken': token
        });
      }
    });
  }

  void handleClickNotification(Map<String, dynamic> message) async {
    String action = message['data']['action'];
    print("fcm action: $action");
    if (action == 'schedule') {
      setState(() {
        _selectedIndex = 3;
        _fabBottomAppBarKey.currentState.updateIndex(_selectedIndex);
        _homeAccountKey.currentState.handleOpenManageRoom();
      });
    } else if (action == 'room_detail') {
      String roomId = message['data']['payload'];
      print('fcm room id ne: $roomId');
      await Future.delayed(Duration(seconds: 1));
      DocumentSnapshot doc =
          await Firestore.instance.collection("rooms").document(roomId).get();
      if (doc.exists) {
        RoomModel room = RoomModel.fromMap(doc.data, doc.documentID);
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => RoomDetailWidget(
                    room: room,
                  )),
        );
      }
    }
  }

  void initAppData() async {
    await Repository.shared.fetchCitiesAndCache();
    _homeSearchKey.currentState.handleSelectedCity();
    _homeFriendKey.currentState.fetchFriendList();
    _homeMapKey.currentState.fetchAllRooms();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      primary: false,
      appBar: EmptyAppBar(),
      extendBody: true,
      body: IndexedStack(
        index: _selectedIndex,
        children: _children,
      ),
      bottomNavigationBar: FABBottomAppBar(
        key: _fabBottomAppBarKey,
        color: Colors.black26,
        selectedColor: Colors.blueAccent,
        notchedShape: CircularNotchedRectangle(),
        onTabSelected: _selectedTab,
        items: [
          FABBottomAppBarItem(iconData: Icons.search, text: 'Tìm kiếm'),
          FABBottomAppBarItem(iconData: Icons.people, text: 'Tìm bạn'),
          FABBottomAppBarItem(iconData: Icons.map, text: 'Bản đồ'),
          FABBottomAppBarItem(iconData: Icons.account_box, text: 'Tài khoản'),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Visibility(
        visible: true,
        child: _buildFab(
          context,
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget _buildFab(BuildContext context) {
    final icons = [Icons.home, Icons.create, Icons.search];
    return AnchoredOverlay(
      showOverlay: false,
      overlayBuilder: (context, offset) {
        return CenterAbout(
          position: Offset(offset.dx, offset.dy - icons.length * 35.0),
          child: FabWithIcons(
            icons: icons,
            onIconTapped: _selectedFab,
          ),
        );
      },
      child: FloatingActionButton(
        onPressed: handleClickPostRoom,
        backgroundColor: Colors.blueAccent,
        child: Icon(Icons.home),
        elevation: 10,
      ),
    );
  }

  void handleClickPostRoom() async {
    analytics.logEvent(name: 'view_search_post_room');
    if (await FirebaseAuth.instance.currentUser() == null) {
      bool result = await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginWidget()),
      );
      if (result == null) {
        return;
      }
      _homeAccountKey.currentState.getUserCached();
    }
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => PostRoomWidget()),
    );
  }
}

class EmptyAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }

  @override
  Size get preferredSize => Size(0.0, 0.0);
}
