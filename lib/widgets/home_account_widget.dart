import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:trolanha/Const.dart';
import 'package:trolanha/shared_preferences_helper.dart';
import 'package:trolanha/models/user_model.dart';
import 'package:trolanha/widgets/account/account_detail_widget.dart';
import 'package:trolanha/widgets/account/account_room_widget.dart';
import 'package:trolanha/widgets/account/approval_room_widget.dart';

class HomeAccountWidget extends StatefulWidget {
  const HomeAccountWidget({Key key, this.onSignOut}) : super(key: key);

  final VoidCallback onSignOut;

  @override
  HomeAccountState createState() => HomeAccountState();
}

class HomeAccountState extends State<HomeAccountWidget> {

  UserModel _user;

  void getUserCached() async {
    String cachedUser = await PreferencesHelper.getString(Const.KEY_CACHED_USER);
    if (cachedUser.isEmpty) return;
    UserModel user = UserModel.fromMap(
        jsonDecode(cachedUser),
        '');
    setState(() {
      _user = user;
    });
  }

  void handleOpenManageRoom() {
    if (_user != null && _user.isPostedRoom != null && _user.isPostedRoom) {
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => AccountRoomWidget(),
      ));
    }
  }

  @override
  void initState() {
    super.initState();
    getUserCached();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          height: 40,
        ),
        InkWell(
          onTap: () async {
            UserModel user = await Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => AccountDetailWidget(),
            ));
            if (user !=  null) {
              setState(() {
                _user = user;
              });
            }
          },
          child: Container(
            padding: EdgeInsets.all(Const.MARGIN_LARGE),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  child: Image.network(
                    _user != null && _user.avatar != null &&
                        _user.avatar.isNotEmpty
                        ? _user.avatar
                        : "https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png",
                    fit: BoxFit.cover,
                    width: 60,
                    height: 60,
                  ),
                ),
                Container(
                  height: 60,
                  padding: EdgeInsets.only(left: Const.MARGIN),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        _user != null ? '${_user.firstName} ${_user.lastName}' : '',
                        style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                            color: Colors.black87),
                      ),
                      Text(
                        "Xem hồ sơ",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.blueAccent),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Container(
          height: 10,
          decoration: BoxDecoration(
              color: Colors.grey,
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment(0, 0.1),
                colors: [Colors.grey.withAlpha(120), Colors.transparent],
              )),
        ),
        if (_user != null && _user.isPostedRoom != null && _user.isPostedRoom) Container(
          margin: EdgeInsets.only(
              left: Const.MARGIN, right: Const.MARGIN, top: 10, bottom: 10),
          child: FloatingActionButton.extended(
              heroTag: "my_room",
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => AccountRoomWidget(),
                ));
              },
              icon: Icon(
                Icons.assignment,
              ),
              label: Text(
                "Lịch hẹn - Phòng của tôi",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              )),
        ),
        if (_user != null && _user.isAdmin != null && _user.isAdmin) Container(
          margin: EdgeInsets.only(
              left: Const.MARGIN, right: Const.MARGIN, top: 10, bottom: 10),
          child: FloatingActionButton.extended(
              heroTag: "control",
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => ApprovalRoomWidget(),
                ));
              },
              icon: Icon(
                Icons.playlist_add_check,
              ),
              label: Text(
                "Duyệt phòng",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              )),
        ),
        _buildSettingLine("Ngôn ngữ", Icons.language),
        _buildSettingLine("Thông báo", Icons.notifications_none),
        _buildSettingLine("Thiết lập khác", Icons.settings),
        _buildSettingLine("Gửi phản hồi", Icons.feedback),
        InkWell(
          child: _buildSettingLine("Đăng xuất", Icons.exit_to_app),
          onTap: () async {
            await FirebaseAuth.instance.signOut();
            SharedPreferences prefs = await SharedPreferences.getInstance();
            prefs.remove(Const.KEY_CACHED_USER);
            widget.onSignOut();
          },
        ),
        Container(
          padding: EdgeInsets.all(Const.MARGIN_LARGE),
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(80)),
                child: Image.asset("assets/logo.png", width: 160, height: 160),
              ),
              Container(
                padding: EdgeInsets.only(top: 20),
                child: Text(
                  "VERSION 1.0.0 (1)",
                  style: TextStyle(
                      color: Colors.black45,
                      fontSize: 15,
                      fontWeight: FontWeight.w500),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: Text(
                  "Copyright © 2020 Rum Nguyen",
                  style: TextStyle(
                      color: Colors.black87,
                      fontSize: 15,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildSettingLine(String title, IconData icon) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: Const.MARGIN_LARGE),
          padding: EdgeInsets.symmetric(vertical: Const.MARGIN),
          decoration: BoxDecoration(),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  title,
                  style: TextStyle(
                      color: Colors.black45,
                      fontSize: 20,
                      fontWeight: FontWeight.w300),
                ),
              ),
              Icon(
                icon,
                size: 25,
                color: Colors.black45,
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: Const.MARGIN_LARGE),
          height: 1,
          decoration: BoxDecoration(color: Colors.grey[200]),
        )
      ],
    );
  }
}
