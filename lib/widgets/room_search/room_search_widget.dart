import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:intl/intl.dart';
import 'package:trolanha/Const.dart';
import 'package:trolanha/models/address_model.dart';
import 'package:trolanha/models/friend_model.dart';
import 'package:trolanha/models/room_model.dart';
import 'package:trolanha/models/simple_model.dart';
import 'package:trolanha/models/user_model.dart';
import 'package:trolanha/shared_preferences_helper.dart';
import 'package:trolanha/widgets/room_detail/room_detail_widget.dart';

import '../../enums.dart';
import '../../helper.dart';
import '../../repository.dart';

class RoomUtilityEntry {
  const RoomUtilityEntry(this.name, this.key, this.icon);

  final String name;
  final RoomUtility key;
  final IconData icon;
}

class RoomSearchWidget extends StatefulWidget {
  final SimpleModel initDistrict;

  const RoomSearchWidget({Key key, this.initDistrict}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _RoomSearchState();
  }
}

class _RoomSearchState extends State<RoomSearchWidget> {
  SimpleModel selectedCity = Repository.shared.selectedCity;
  List<String> _selectedAddresses = [];
  List<String> historyList = [];
  Position _cachedCurrentPosition;

  List<RoomModel> _rooms = [];

  final _places =
      new GoogleMapsPlaces(apiKey: "AIzaSyBB-sESaK33kjh5mPFHIVYWmAaNL3PfbkA");
  List<Prediction> _predictions = [];

  final _searchController = TextEditingController();
  final FocusNode _searchFocusNode = FocusNode();

  bool _isSelectedLocation = false;
  bool _isApplied = false;

  FilterType _selectedFilter = FilterType.price;

  var _filterPriceRange = RangeValues(5, 100);
  RoomType _filterRoomType;
  SortType _filterSortType = SortType.newest;
  Gender _filterGenderType = Gender.all;
  int _filterCapacity = 0;
  List<RoomUtility> _filterFacilities = <RoomUtility>[];

  final List<RoomUtilityEntry> _cast = <RoomUtilityEntry>[
    const RoomUtilityEntry('Thú cưng', RoomUtility.pet, Icons.pets),
    const RoomUtilityEntry('Tivi', RoomUtility.tivi, Icons.tv),
    const RoomUtilityEntry('Tủ đồ', RoomUtility.closet, Icons.inbox),
    const RoomUtilityEntry('Giường', RoomUtility.bed, Icons.hotel),
    const RoomUtilityEntry('Gác lửng', RoomUtility.mezz, Icons.trending_up),
    const RoomUtilityEntry('Máy giặt', RoomUtility.wash, Icons.crop_portrait),
    const RoomUtilityEntry('Tủ lạnh', RoomUtility.frid, Icons.ac_unit),
    const RoomUtilityEntry('Nhà bếp', RoomUtility.kit, Icons.kitchen),
    const RoomUtilityEntry('Máy nước nóng', RoomUtility.heat, Icons.whatshot),
    const RoomUtilityEntry('WC riêng', RoomUtility.wc, Icons.wc),
    const RoomUtilityEntry('Chỗ để xe', RoomUtility.park, Icons.local_parking),
    const RoomUtilityEntry('An ninh', RoomUtility.secure, Icons.lock),
    const RoomUtilityEntry('Cửa sổ', RoomUtility.window, Icons.crop_square),
    const RoomUtilityEntry('Wifi', RoomUtility.wifi, Icons.wifi),
    const RoomUtilityEntry('Tự do', RoomUtility.free, Icons.motorcycle),
    const RoomUtilityEntry(
        'Chủ riêng', RoomUtility.privacy, Icons.account_circle),
  ];

  String getDisplayTextUtility(RoomUtility utility) {
    switch (utility) {
      case RoomUtility.pet:
        return "Thú cưng";
      case RoomUtility.tivi:
        return "Tivi";
      case RoomUtility.closet:
        return "Tủ đồ";
      case RoomUtility.bed:
        return "Giường";
      case RoomUtility.mezz:
        return "Gác lửng";
      case RoomUtility.wash:
        return "Máy giặt";
      case RoomUtility.frid:
        return "Tủ lạnh";
      case RoomUtility.kit:
        return "Nhà bếp";
      case RoomUtility.heat:
        return "Máy nước nóng";
      case RoomUtility.wc:
        return "WC riêng";
      case RoomUtility.park:
        return "Chỗ để xe";
      case RoomUtility.secure:
        return "An ninh";
      case RoomUtility.window:
        return "Cửa sổ";
      case RoomUtility.wifi:
        return "Wifi";
      case RoomUtility.free:
        return "Tự do";
      case RoomUtility.privacy:
        return "Chủ riêng";
    }
    return "";
  }

  @override
  void initState() {
    super.initState();
    if (widget.initDistrict != null) {
      _selectedAddresses.add(selectedCity.id);
      _selectedAddresses.add(widget.initDistrict.id);
      _searchController.text = widget.initDistrict.displayText;
      setState(() {
        _isSelectedLocation = true;
      });
    }
    getHistoryList();
    Geolocator().getCurrentPosition().then((value) {
      _cachedCurrentPosition = value;
    });
  }

  void getHistoryList() async {
    List<String> list =
        await PreferencesHelper.getStringList(Const.KEY_HISTORY_SEARCH);
    if (list != null && list.isNotEmpty) {
      print(list.length);
      setState(() {
        historyList = list;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(top: 5),
          child: Column(
            children: [
              _buildSearchBarWidget(),
              if (_isSelectedLocation) _buildFilterWidget(),
              Expanded(
                child: _buildMainContent(),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildMainContent() {
    if (_isSelectedLocation) {
      if (_isApplied) {
        return ListView.builder(
          itemBuilder: (context, index) {
            return _buildPostItem(index, _rooms[index]);
          },
          itemCount: _rooms.length,
        );
      } else {
        return Container(
          alignment: Alignment.topCenter,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(Const.MARGIN * 2),
          decoration: BoxDecoration(color: Colors.black.withAlpha(20)),
          child: Text(
            "Chọn tiêu chí, sau đó bấm \"TroLaNha\" để tìm kiếm",
            style: TextStyle(
              color: Colors.grey[600],
              fontSize: 16,
            ),
            textAlign: TextAlign.center,
          ),
        );
      }
    } else {
      return ListView(
        children: [
          if (_predictions.length > 0) _buildSuggestWidget(),
          if (historyList.length > 0) _buildHistoryWidget(),
        ],
      );
    }
  }

  Widget _buildPostItem(int index, RoomModel room) {
    double height = 120;

    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => RoomDetailWidget(
                    room: room,
                  )),
        );
      },
      child: Container(
        height: height,
        margin: EdgeInsets.only(
            left: Const.MARGIN,
            right: Const.MARGIN,
            bottom: Const.MARGIN,
            top: Const.MARGIN),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(width: 1, color: Colors.grey[200]),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  child: Image.network(
                    room.images[0],
                    fit: BoxFit.cover,
                    width: 120,
                    height: height,
                  ),
                ),
                Container(
                  width: 120,
                  height: height,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10)),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment(0, 0.1),
                        colors: [
                          Colors.black.withAlpha(120),
                          Colors.transparent
                        ],
                      )),
                ),
              ],
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 10, right: 5),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                        bottomRight: Radius.circular(10))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Row(
                        children: [
                          Text(
                            "${Helper.getDisplayTextRoomType(room.type).toUpperCase()} . ",
                            style: TextStyle(
                              color: Colors.grey[400],
                              fontSize: 12,
                            ),
                          ),
                          Text(
                            "${room.capacity} ${Helper.getDisplayTextUnicodeGender(room.gender)}",
                            style: TextStyle(
                              color: Colors.grey[400],
                              fontSize: 12,
                            ),
                          ),
                          Expanded(
                            child: Container(),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
                      child: Text(
                        "${room.title}",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w600),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
                      child: Text(
                        "${(room.price / 1000000).formattedValue} M₫/${Helper.getUnitTextRoomType(room.type)}",
                        style: TextStyle(
                          color: Colors.blueAccent,
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.fromLTRB(0, 6, 0, 10),
                        child: Text(
                          "${room.address.district != null ? room.address.district.displayText : ''}",
                          style: TextStyle(
                            color: Colors.grey[600],
                            fontSize: 13,
                          ),
                          maxLines: 2,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  // filter widgets
  Widget _buildFilterWidget() {
    List<Widget> selectedFilterWidget = getSelectedFilterWidget();
    return Container(
      child: Column(
        children: [
          _buildFilterTypeWidget(),
          _buildFilterSelectedWidget(selectedFilterWidget),
          if (!_isApplied) _buildSelectedFilterWidget(),
          if (_isApplied) _buildResultFilterWidget(),
          if (!_isApplied) _buildApplyButtonWidget(),
        ],
      ),
    );
  }

  Widget _buildResultFilterWidget() {
    return Container(
      height: 40,
      alignment: Alignment.center,
      child: Text(
        "${_rooms.length} kết quả",
        style: TextStyle(
            fontSize: 15,
            color: Colors.blueAccent,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildSelectedFilterWidget() {
    switch (_selectedFilter) {
      case FilterType.price:
        return _buildFilterTypePriceWidget();
      case FilterType.facility:
        return _buildFilterUtilityWidget();
      case FilterType.type:
        return _buildFilterTypeRoomWidget();
      case FilterType.capacity:
        return _buildFilterGenderWidget();
      case FilterType.sort:
        return _buildFilterSortWidget();
    }
    return Container();
  }

  Widget _buildGenderItemWidget(Gender gender) {
    return Expanded(
      child: InkWell(
        onTap: () {
          setState(() {
            _filterGenderType = gender;
          });
        },
        child: Container(
          alignment: Alignment.center,
          height: 40,
          decoration: _filterGenderType == gender
              ? BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  border: Border.all(width: 1, color: Colors.blueAccent),
                )
              : BoxDecoration(),
          child: Text(
            Helper.getDisplayTextGender(gender),
            style: TextStyle(
                color: _filterGenderType == gender
                    ? Colors.blueAccent
                    : Colors.black),
          ),
        ),
      ),
    );
  }

  Widget _buildFilterGenderWidget() {
    return Container(
      margin: EdgeInsets.only(
          left: Const.MARGIN, right: Const.MARGIN, top: Const.MARGIN),
      child: Column(
        children: [
          Container(
            height: 50,
            child: Row(
              children: [
                Container(
                  child: Text(
                    "Số người",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                Expanded(
                  child: Container(),
                ),
                InkWell(
                  onTap: () {
                    if (_filterCapacity == 0) return;
                    setState(() {
                      _filterCapacity--;
                    });
                  },
                  child: Icon(
                    Icons.remove_circle_outline,
                    size: 30,
                    color: _filterCapacity == 0
                        ? Colors.blueAccent.withAlpha(60)
                        : Colors.blueAccent,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Text("$_filterCapacity"),
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      _filterCapacity++;
                    });
                  },
                  child: Icon(
                    Icons.add_circle_outline,
                    size: 30,
                    color: Colors.blueAccent,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 50,
            child: Row(
              children: [
                Container(
                  padding: EdgeInsets.only(right: 20),
                  child: Text(
                    "Giới tính",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        _buildGenderItemWidget(Gender.all),
                        _buildGenderItemWidget(Gender.male),
                        _buildGenderItemWidget(Gender.female),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildFilterUtilityWidget() {
    return Container(
      padding: EdgeInsets.only(top: Const.MARGIN),
      child: Wrap(
        children: actorWidgets.toList(),
      ),
    );
  }

  Iterable<Widget> get actorWidgets sync* {
    for (final RoomUtilityEntry utility in _cast) {
      yield Container(
        margin: EdgeInsets.only(right: Const.MARGIN),
        child: FilterChip(
          selectedColor: Colors.blueAccent,
          backgroundColor: Colors.grey[200],
          padding: EdgeInsets.all(5),
          avatar: CircleAvatar(
              child: Icon(
            utility.icon,
            size: 12,
          )),
          label: Text(utility.name),
          selected: _filterFacilities.contains(utility.key),
          onSelected: (bool value) {
            setState(() {
              if (value) {
                _filterFacilities.add(utility.key);
              } else {
                _filterFacilities.removeWhere((RoomUtility name) {
                  return name == utility.key;
                });
              }
            });
          },
        ),
      );
    }
  }

  void handleUpdateFriendSearch() async {
    FirebaseUser fbUser = await FirebaseAuth.instance.currentUser();
    String cachedUser =
        await PreferencesHelper.getString(Const.KEY_CACHED_USER);
    if (cachedUser.isEmpty) return;
    UserModel user = UserModel.fromMap(jsonDecode(cachedUser), '');

    AddressModel address = AddressModel(
        city: selectedCity,
        district: Repository.shared
            .district(_selectedAddresses[0], _selectedAddresses[1]));
    FriendModel friend = FriendModel(
        gender: _filterGenderType,
        lastUpdatedAt: Timestamp.now(),
        address: address,
        minimumPrice: _filterPriceRange.start.toInt() * 100000,
        maximumPrice: _filterPriceRange.end.toInt() * 100000,
        nameSnippet: '${user.firstName} ${user.lastName}',
        avatarSnippet: user.avatar,
        jobSnippet: user.job,
        birthDateSnippet: Timestamp.fromMillisecondsSinceEpoch(user.birthDateInMillis),
        utilities: _filterFacilities);
    Firestore.instance.collection("friends").document(fbUser.uid).setData(friend.toMap());
  }

  void handleSearchRoom() {
    Query rooms = Firestore.instance
        .collection("rooms")
        .where('isVerified', isEqualTo: true)
        .where('isApproved', isEqualTo: true);

    if (_selectedAddresses.length > 1) {
      rooms = rooms.where('addresses', arrayContains: _selectedAddresses[1]);
    }
    rooms = rooms
        .where('price',
            isGreaterThanOrEqualTo: _filterPriceRange.start * 100000)
        .where('price', isLessThanOrEqualTo: _filterPriceRange.end * 100000);
    if (_filterRoomType != null) {
      rooms = rooms.where('type', isEqualTo: _filterRoomType.enumValue);
    }
    if (_filterGenderType != Gender.all) {
      rooms = rooms.where('gender', isEqualTo: _filterGenderType.enumValue);
    }
    if (_filterCapacity != 0) {
//      rooms = rooms.where('capacity', isGreaterThanOrEqualTo: _filterCapacity);
    }
    switch (_filterSortType) {
      case SortType.newest:
//        rooms =  rooms.orderBy('lastUpdatedAt', descending: true);
        break;
      case SortType.highToLow:
        rooms = rooms.orderBy('price', descending: true);
        break;
      case SortType.lowToHigh:
        rooms = rooms.orderBy('price', descending: false);
        break;
    }
    rooms.getDocuments().then((value) {
      List<RoomModel> rooms = [];
      for (int i = 0; i < value.documents.length; i++) {
        DocumentSnapshot doc = value.documents[i];
        RoomModel room = RoomModel.fromMap(doc.data, doc.documentID);
        if (room.capacity >= _filterCapacity) {
          rooms.add(room);
        }
      }
      if (_filterSortType == SortType.newest) {
        rooms.sort((a, b) => b.lastUpdatedAt.millisecondsSinceEpoch
            .compareTo(a.lastUpdatedAt.millisecondsSinceEpoch));
      }
      setState(() {
        _rooms = rooms;
        _isApplied = true;
      });
    });
  }

  Widget _buildApplyButtonWidget() {
    return InkWell(
      onTap: () {
        handleSearchRoom();
        handleUpdateFriendSearch();
      },
      child: Container(
        margin: EdgeInsets.all(Const.MARGIN),
        padding: EdgeInsets.all(Const.MARGIN),
        decoration: BoxDecoration(
          color: Colors.blueAccent,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
//      width: MediaQuery.of(context).size.width - 2 * Const.MARGIN,
        child: Center(
          child: Text(
            "TroLaNha",
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
        ),
      ),
    );
  }

  Widget _buildFilterSortWidget() {
    return Container(
      padding: EdgeInsets.only(top: Const.MARGIN),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildFilterSortTypeItemWidget(SortType.newest),
          _buildFilterSortTypeItemWidget(SortType.lowToHigh),
          _buildFilterSortTypeItemWidget(SortType.highToLow),
        ],
      ),
    );
  }

  String getDisplayTextSortType(SortType sortType) {
    switch (sortType) {
      case SortType.newest:
        return "Mới nhất";
      case SortType.lowToHigh:
        return "Giá thấp đến cao";
      case SortType.highToLow:
        return "Giá cao xuống thấp";
    }
    return "";
  }

  Widget _buildFilterSortTypeItemWidget(SortType sortType) {
    return RadioListTile<SortType>(
      title: Text(getDisplayTextSortType(sortType)),
      value: sortType,
      groupValue: _filterSortType,
      onChanged: (SortType value) {
        setState(() {
          _filterSortType = value;
        });
      },
    );
  }

  Widget _buildFilterTypeRoomWidget() {
    return Container(
      padding: EdgeInsets.only(top: Const.MARGIN),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildFilterRoomTypeItemWidget(RoomType.dorm),
          _buildFilterRoomTypeItemWidget(RoomType.rent),
          _buildFilterRoomTypeItemWidget(RoomType.shared),
          _buildFilterRoomTypeItemWidget(RoomType.whole),
          _buildFilterRoomTypeItemWidget(RoomType.apartment),
        ],
      ),
    );
  }

  Widget _buildFilterRoomTypeItemWidget(RoomType roomType) {
    return RadioListTile<RoomType>(
      title: Text(Helper.getDisplayTextRoomType(roomType)),
      value: roomType,
      groupValue: _filterRoomType,
      onChanged: (RoomType value) {
        setState(() {
          _filterRoomType = value;
        });
      },
    );
  }

  String formatPrice(double price) {
    var formatter = new NumberFormat("##,###,###");
    return formatter.format(price * 100000) + " VNĐ";
  }

  Widget _buildFilterTypePriceWidget() {
    return Container(
      padding: EdgeInsets.only(
          left: Const.MARGIN, right: Const.MARGIN, top: Const.MARGIN * 2),
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("${formatPrice(_filterPriceRange.start)}"),
                Text("${formatPrice(_filterPriceRange.end)}"),
              ],
            ),
          ),
          Container(
            height: 70,
            child: RangeSlider(
                min: 5,
                max: 100,
                divisions: 19,
                values: _filterPriceRange,
                labels: RangeLabels('${formatPrice(_filterPriceRange.start)}',
                    '${formatPrice(_filterPriceRange.end)}'),
                onChanged: (RangeValues newRange) {
                  setState(() {
                    _filterPriceRange = newRange;
                  });
                }),
          ),
        ],
      ),
    );
  }

  Widget _buildFilterSelectedWidget(List<Widget> list) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.black.withAlpha(20),
      ),
      height: 40,
      child: Row(
        children: [
          InkWell(
            onTap: () {
              setState(() {
                _filterPriceRange = RangeValues(5, 100);
                _filterRoomType = null;
                _filterSortType = SortType.newest;
                _filterGenderType = Gender.all;
                _filterCapacity = 0;
                _filterFacilities = <RoomUtility>[];
                _selectedFilter = FilterType.price;
              });
              handleSearchRoom();
            },
            child: Container(
              padding: EdgeInsets.only(left: Const.MARGIN, right: Const.MARGIN),
              child: Icon(
                Icons.remove_circle,
                size: 25,
                color: Colors.grey[500],
              ),
            ),
          ),
          Expanded(
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: list,
            ),
          )
        ],
      ),
    );
  }

  List<Widget> getSelectedFilterWidget() {
    List<Widget> list = [];
    if (_filterPriceRange.start != 5 || _filterPriceRange.end != 100) {
      list.add(_buildFilterSelectedItemWidget(FilterType.price, 0));
    }
    _filterFacilities.asMap().forEach((index, value) {
      list.add(_buildFilterSelectedItemWidget(FilterType.facility, index));
    });
    if (_filterRoomType != null) {
      list.add(_buildFilterSelectedItemWidget(FilterType.type, 0));
    }
    if (_filterCapacity > 0 || _filterGenderType != Gender.all) {
      list.add(_buildFilterSelectedItemWidget(FilterType.capacity, 0));
    }
    if (_filterSortType != SortType.newest) {
      list.add(_buildFilterSelectedItemWidget(FilterType.sort, 0));
    }
    return list;
  }

  Widget _buildFilterSelectedItemWidget(FilterType filter, int index) {
    String title;
    switch (filter) {
      case FilterType.price:
        title = formatPrice(_filterPriceRange.start) +
            " - " +
            formatPrice(_filterPriceRange.end);
        break;
      case FilterType.facility:
        title = getDisplayTextUtility(_filterFacilities[index]);
        break;
      case FilterType.type:
        title = Helper.getDisplayTextRoomType(_filterRoomType);
        break;
      case FilterType.capacity:
        title = (_filterCapacity > 0 ? _filterCapacity.toString() + "  " : "") +
            (_filterGenderType == Gender.all
                ? "Nam/Nữ"
                : Helper.getDisplayTextGender(_filterGenderType));
        break;
      case FilterType.sort:
        title = getDisplayTextSortType(_filterSortType);
        break;
    }

    return InkWell(
      onTap: () {
        switch (filter) {
          case FilterType.price:
            setState(() {
              _filterPriceRange = RangeValues(5, 100);
            });
            break;
          case FilterType.facility:
            setState(() {
              _filterFacilities.removeAt(index);
            });
            break;
          case FilterType.type:
            setState(() {
              _filterRoomType = null;
            });
            break;
          case FilterType.capacity:
            setState(() {
              _filterCapacity = 0;
              _filterGenderType = Gender.all;
            });
            break;
          case FilterType.sort:
            setState(() {
              _filterSortType = SortType.newest;
            });
            break;
        }
        handleSearchRoom();
      },
      child: Container(
        margin: EdgeInsets.all(7),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(20))),
        padding: EdgeInsets.only(left: Const.MARGIN, right: Const.MARGIN),
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(right: 5),
              child: Text(
                title,
                style: TextStyle(color: Colors.black),
              ),
            ),
            Icon(
              Icons.remove_circle,
              color: Colors.grey[500],
              size: 12,
            )
          ],
        ),
      ),
    );
  }

  Widget _buildFilterTypeWidget() {
    return Container(
      padding: EdgeInsets.only(left: Const.MARGIN, right: Const.MARGIN),
      height: 40,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          _buildFilterTypeItemWidget("Giá", FilterType.price),
          _buildFilterTypeItemWidget("Tiện ích", FilterType.facility),
          _buildFilterTypeItemWidget("Loại phòng", FilterType.type),
          _buildFilterTypeItemWidget("Số người", FilterType.capacity),
          _buildFilterTypeItemWidget("Sắp xếp", FilterType.sort),
        ],
      ),
    );
  }

  Widget _buildFilterTypeItemWidget(String title, FilterType filterType) {
    return InkWell(
      onTap: () {
        setState(() {
          _selectedFilter = filterType;
          _isApplied = false;
        });
      },
      child: Container(
        padding: EdgeInsets.only(right: Const.MARGIN),
        child: Row(
          children: [
            Container(
              child: Text(
                title,
                style: TextStyle(
                    color: _selectedFilter == filterType
                        ? Colors.blueAccent
                        : Colors.black),
              ),
            ),
            Icon(
              _selectedFilter == filterType
                  ? Icons.keyboard_arrow_up
                  : Icons.keyboard_arrow_down,
              color: _selectedFilter == filterType
                  ? Colors.blueAccent
                  : Colors.black,
              size: 20,
            ),
          ],
        ),
      ),
    );
  }

  // =================

  Widget _buildSearchBarWidget() {
    return Container(
      padding: EdgeInsets.only(left: Const.MARGIN, right: Const.MARGIN),
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 40,
              decoration: BoxDecoration(
                  color: Colors.black.withAlpha(10),
                  border: Border.all(color: Colors.blueAccent),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: _buildSearchBarRow(),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Container(
              padding: EdgeInsets.all(10),
              child: Text(
                "Huỷ",
                style: TextStyle(color: Colors.grey, fontSize: 15),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSuggestItemWidget(int index) {
    return InkWell(
      onTap: () {
        Prediction prediction = _predictions[index];
        _places
            .getDetailsByPlaceId(prediction.placeId, language: "vi")
            .then((value) {
          String city, district;
          for (int i = 0; i < value.result.addressComponents.length; i++) {
            AddressComponent addressComponent =
                value.result.addressComponents[i];
            if (addressComponent.types
                .contains("administrative_area_level_1")) {
              // City
              city = addressComponent.longName;
            } else if (addressComponent.types
                .contains("administrative_area_level_2")) {
              // District
              district = addressComponent.longName;
            }
          }
          print(city);
          print(district);
          if (city != null && district != null) {
            print(Repository.shared.getAddressIds(city, district));
            _selectedAddresses =
                Repository.shared.getAddressIds(city, district);

            if (historyList.length >= 6) {
              historyList.removeLast();
            }
            historyList.insert(0, prediction.description);
            PreferencesHelper.setStringList(
                Const.KEY_HISTORY_SEARCH, historyList);

            setState(() {
              _isSelectedLocation = true;
              _searchController.text = prediction.description;
              _searchFocusNode.nextFocus();
            });
          }
        });
      },
      child: Container(
        decoration: BoxDecoration(),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 12, bottom: 12),
              child: Row(
                children: [
                  Icon(
                    Icons.location_on,
                    color: Colors.grey,
                    size: 15,
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: Text(
                        _predictions[index].description,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(color: Colors.grey, fontSize: 13),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 0.5,
              decoration: BoxDecoration(
                color: Colors.grey.withAlpha(50),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSuggestWidget() {
    return Container(
      margin: EdgeInsets.only(
          left: Const.MARGIN, right: Const.MARGIN, top: Const.MARGIN),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 4),
            child: Text(
              "Kết quả tìm kiếm",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.w600),
            ),
          ),
          Container(
            height: 250,
            child: ListView.builder(
              itemBuilder: (context, index) {
                return _buildSuggestItemWidget(index);
              },
              itemCount: _predictions.length,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildHistoryWidget() {
    return Container(
      margin: EdgeInsets.only(
          left: Const.MARGIN, right: Const.MARGIN, top: Const.MARGIN),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 4),
            child: Text(
              "Lịch sử tìm kiếm",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.w600),
            ),
          ),
          Container(
            height: 200,
            child: ListView(
              children:
                  historyList.map((e) => _buildHistoryItemWidget(e)).toList(),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildHistoryItemWidget(String history) {
    return InkWell(
      onTap: () {
        _searchController.text = history;
        handleSearchText(history);
      },
      child: Container(
        decoration: BoxDecoration(),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 12, bottom: 12),
              child: Row(
                children: [
                  Icon(
                    Icons.schedule,
                    color: Colors.grey,
                    size: 15,
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: Text(
                        history,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(color: Colors.grey, fontSize: 13),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 0.5,
              decoration: BoxDecoration(
                color: Colors.grey.withAlpha(50),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void handleSearchText(String text) {
    _places
        .autocomplete(
      text,
      radius: 50000,
      language: "vi",
      location: Location(selectedCity.centerPoint.latitude,
          selectedCity.centerPoint.longitude),
    )
        .then(
      (value) {
        setState(() {
          _predictions = value.predictions;
        });
      },
    );
  }

  Row _buildSearchBarRow() {
    return Row(
      children: [
        Container(
          height: 40,
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            color: Colors.black.withAlpha(20),
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Icon(
                Icons.person_pin_circle,
                color: Colors.blueAccent,
                size: 15,
              ),
              Text(
                selectedCity == null ? "" : selectedCity.displayText,
                style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w900,
                    color: Colors.blueAccent),
              )
            ],
          ),
        ),
        Expanded(
          child: Container(
            decoration: BoxDecoration(),
            height: 40,
            child: TextFormField(
              autofocus: true,
              focusNode: _searchFocusNode,
              onTap: () {
                setState(() {
                  _isSelectedLocation = false;
                });
              },
              onEditingComplete: () {
                FocusScope.of(context).nextFocus();
              },
              onChanged: (value) {
                handleSearchText(_searchController.text);
              },
              maxLines: 1,
              maxLengthEnforced: false,
              controller: _searchController,
              style: TextStyle(fontSize: 15),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(left: 5, right: 5, bottom: 10),
                border: InputBorder.none,
                hintStyle: TextStyle(color: Colors.grey, fontSize: 15),
              ),
            ),
          ),
        )
      ],
    );
  }
}
