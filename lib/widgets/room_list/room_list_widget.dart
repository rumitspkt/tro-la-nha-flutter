import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:trolanha/customs/dialogs.dart';
import 'package:trolanha/enums.dart';
import 'package:trolanha/models/room_model.dart';
import 'package:trolanha/models/simple_model.dart';
import 'package:trolanha/repository.dart';
import 'package:trolanha/widgets/room_detail/room_detail_widget.dart';

import '../../Const.dart';
import '../../helper.dart';

class RoomListWidget extends StatefulWidget {
  final String type;
  final RoomModel sameRoom;
  final List<RoomModel> ownerRooms;

  const RoomListWidget({Key key, this.type, this.sameRoom, this.ownerRooms}) : super(key: key);

  @override
  State<StatefulWidget> createState() => RoomListState();
}

class RoomListState extends State<RoomListWidget> {
  List<RoomModel> rooms = [];
  final CollectionReference _rooms = Firestore.instance.collection("rooms");
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final SimpleModel selectedCity = Repository.shared.selectedCity;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () async {
      switch (widget.type) {
        case 'good_price':
          Dialogs.showLoadingDialog(context, _keyLoader);
          await fetchHomeListGoodPriceRoom();
          break;
        case 'newest':
          Dialogs.showLoadingDialog(context, _keyLoader);
          await fetchHomeListNewestRoom();
          break;
        case 'same_room':
          Dialogs.showLoadingDialog(context, _keyLoader);
          await fetchSuggestListRoom();
          break;
        case 'owner':
          setState(() {
            this.rooms = widget.ownerRooms;
          });
          break;
      }
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
    });
  }

  Future<void> fetchHomeListGoodPriceRoom() async {
    QuerySnapshot query = await _rooms
        .where('isVerified', isEqualTo: true)
        .where('isApproved', isEqualTo: true)
        .where('addresses', arrayContains: selectedCity.id)
        .orderBy('price', descending: false)
        .getDocuments();
    List<RoomModel> rooms = [];
    for (int i = 0; i < query.documents.length; i++) {
      DocumentSnapshot doc = query.documents[i];
      RoomModel room = RoomModel.fromMap(doc.data, doc.documentID);
      rooms.add(room);
    }
    setState(() {
      this.rooms = rooms;
    });
  }

  Future<void> fetchSuggestListRoom() async {
    QuerySnapshot query = await Firestore.instance
        .collection("rooms")
        .where('isVerified', isEqualTo: true)
        .where('isApproved', isEqualTo: true)
        .where('price', isGreaterThanOrEqualTo: widget.sameRoom.price - 1000000)
        .where('price', isLessThanOrEqualTo: widget.sameRoom.price + 1000000)
        .where('addresses', arrayContains: widget.sameRoom.address.district.id)
        .getDocuments();
    List<RoomModel> rooms = [];
    for (int i = 0; i < query.documents.length; i++) {
      DocumentSnapshot doc = query.documents[i];
      RoomModel room = RoomModel.fromMap(doc.data, doc.documentID);
      rooms.add(room);
    }
    setState(() {
      this.rooms = rooms;
    });
  }


  Future<void> fetchHomeListNewestRoom() async {
    QuerySnapshot query = await _rooms
        .where('isVerified', isEqualTo: true)
        .where('isApproved', isEqualTo: true)
        .where('addresses', arrayContains: selectedCity.id)
        .orderBy('lastUpdatedAt', descending: true)
        .getDocuments();
    List<RoomModel> rooms = [];
    for (int i = 0; i < query.documents.length; i++) {
      DocumentSnapshot doc = query.documents[i];
      RoomModel room = RoomModel.fromMap(doc.data, doc.documentID);
      rooms.add(room);
    }
    setState(() {
      this.rooms = rooms;
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          getTypeRoomListTitle(),
        ),
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.only(top: Const.MARGIN),
        padding: EdgeInsets.only(left: Const.MARGIN, right: Const.MARGIN),
        child: GridView.count(
          primary: true,
          shrinkWrap: true,
          crossAxisSpacing: Const.MARGIN,
          mainAxisSpacing: Const.MARGIN,
          childAspectRatio: ((size.width - 3 * Const.MARGIN) / 2) / 260,
          crossAxisCount: 2,
          children: rooms.map((room) => _buildPostItem(room)).toList(),
        ),
      ),
    );
  }

  String getTypeRoomListTitle() {
    String title = "";
    switch (widget.type) {
      case 'good_price':
        title = 'Phòng giá tốt';
        break;
      case 'newest':
        title = 'Phòng mới nhất';
        break;
      case 'same_room':
        title = 'Phòng cùng tiêu chí';
        break;
      case 'owner':
        title =
            'Phòng của ${widget.sameRoom.owner.firstName} ${widget.sameRoom.owner.lastName}';
        break;
    }
    return title;
  }

  Widget _buildPostItem(RoomModel room) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => RoomDetailWidget(
                    room: room,
                  )),
        );
      },
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  child: Image.network(
                    room.images[0],
                    fit: BoxFit.cover,
                    width: 1000,
                    height: 110,
                  ),
                ),
                Container(
                  width: 1000,
                  height: 110,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment(0, 0.1),
                        colors: [
                          Colors.black.withAlpha(120),
                          Colors.transparent
                        ],
                      )),
                ),
                if (widget.type == 'good_price' || widget.type == 'newest')
                  Positioned(
                      top: 5,
                      right: 5,
                      child: Icon(
                        widget.type == 'good_price'
                            ? Icons.monetization_on
                            : Icons.fiber_new,
                        color: Color(widget.type == 'good_price'
                            ? Const.primaryColor
                            : Const.secondaryColor),
                        size: 18,
                      )),
              ],
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Row(
                children: [
                  Text(
                    "${Helper.getDisplayTextRoomType(room.type).toUpperCase()} . ",
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 12,
                    ),
                  ),
                  Text(
                      "${room.capacity} ${Helper.getDisplayTextUnicodeGender(room.gender)}",
                      style: TextStyle(
                        color: Colors.grey[400],
                        fontSize: 12,
                      )),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
              child: Text(
                room.title,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.w600),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.start,
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
              child: Text(
                "${(room.price / 1000000).formattedValue} triệu VNĐ/${Helper.getUnitTextRoomType(room.type)}",
                style: TextStyle(
                  color: Colors.blueAccent,
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.justify,
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
              child: Text(
                "${room.address.addressDescription}",
                style: TextStyle(
                  color: Colors.grey[600],
                  fontSize: 13,
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.start,
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
                child: Text(
                  "${room.address.district != null ? room.address.district.displayText : ''}",
                  style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 13,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
