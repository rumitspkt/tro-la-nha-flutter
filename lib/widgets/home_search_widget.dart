import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:trolanha/Const.dart';
import 'package:trolanha/helper.dart';
import 'package:trolanha/models/room_model.dart';
import 'package:trolanha/models/simple_model.dart';
import 'package:trolanha/widgets/room_detail/room_detail_widget.dart';
import 'package:trolanha/widgets/room_list/room_list_widget.dart';
import 'package:trolanha/widgets/room_search/room_search_widget.dart';

import '../enums.dart';
import '../repository.dart';
import '../shared_preferences_helper.dart';

class HomeSearchWidget extends StatefulWidget {
  const HomeSearchWidget(
      {Key key, this.onSearchClick, this.onMapClick, this.onPostClick})
      : super(key: key);

  final VoidCallback onSearchClick, onPostClick, onMapClick;

  @override
  HomeSearchState createState() => HomeSearchState();
}

class HomeSearchState extends State<HomeSearchWidget> {
  SimpleModel selectedCity;
  List<RoomModel> listGoodPrice = [];
  List<RoomModel> listNewest = [];

  final CollectionReference _rooms = Firestore.instance.collection("rooms");
  final FirebaseAnalytics analytics = FirebaseAnalytics();
  RemoteConfig remoteConfig;
  bool enableFeatureSearchByDistrict = true;
  bool enableFeatureTrendingSearch = true;
  bool enableFeatureGoodPriceList = true;
  bool enableFeatureNewestList = true;

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    initRemoteConfig();
  }

  void initRemoteConfig() async {
    remoteConfig = await RemoteConfig.instance;
    await remoteConfig.fetch(expiration: const Duration(seconds: 3));
    await remoteConfig.activateFetched();
    setState(() {
      enableFeatureSearchByDistrict =
          remoteConfig.getBool("enableFeatureSearchByDistrict");
      enableFeatureTrendingSearch =
          remoteConfig.getBool("enableFeatureTrendingSearch");
      enableFeatureGoodPriceList =
          remoteConfig.getBool("enableFeatureGoodPriceList");
      enableFeatureNewestList = remoteConfig.getBool("enableFeatureNewestList");
    });
  }

  void handleSelectedCity() async {
    setState(() {
      selectedCity = Repository.shared.selectedCity;
    });
    fetchHomeListGoodPriceRoom();
    fetchHomeListNewestRoom();
  }

  void fetchHomeListGoodPriceRoom() async {
    QuerySnapshot query = await _rooms
        .where('isVerified', isEqualTo: true)
        .where('isApproved', isEqualTo: true)
        .where('addresses', arrayContains: selectedCity.id)
        .orderBy('price', descending: false)
        .limit(10)
        .getDocuments();
    List<RoomModel> rooms = [];
    for (int i = 0; i < query.documents.length; i++) {
      DocumentSnapshot doc = query.documents[i];
      RoomModel room = RoomModel.fromMap(doc.data, doc.documentID);
      rooms.add(room);
    }
    setState(() {
      listGoodPrice = rooms;
    });
  }

  void fetchHomeListNewestRoom() async {
    QuerySnapshot query = await _rooms
        .where('isVerified', isEqualTo: true)
        .where('isApproved', isEqualTo: true)
        .where('addresses', arrayContains: selectedCity.id)
        .orderBy('lastUpdatedAt', descending: true)
        .limit(10)
        .getDocuments();
    List<RoomModel> rooms = [];
    for (int i = 0; i < query.documents.length; i++) {
      DocumentSnapshot doc = query.documents[i];
      RoomModel room = RoomModel.fromMap(doc.data, doc.documentID);
      rooms.add(room);
    }
    setState(() {
      listNewest = rooms;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      onRefresh: () async {
        initRemoteConfig();
        handleSelectedCity();
        Future.delayed(Duration(seconds: 2), () {
          _refreshController.refreshCompleted();
        });
      },
      controller: _refreshController,
      child: ListView(
        cacheExtent: 9999,
        addAutomaticKeepAlives: true,
        primary: true,
        children: [
          _buildBannerSlider(),
          Stack(
            alignment: AlignmentDirectional.topCenter,
            overflow: Overflow.visible,
            children: [
              Container(
                height: 140,
              ),
              Positioned(
                top: -25,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          offset: Offset(2.0, 2.0), //(x,y)
                          blurRadius: 20,
                        )
                      ]),
                  child: Container(
                    child: ControlPanel(
                      enableFeatureSearchByDistrict:
                          enableFeatureSearchByDistrict,
                      selectedCity: selectedCity,
                      onPostClick: this.widget.onPostClick,
                      onMapClick: this.widget.onMapClick,
                      onSearchClick: this.widget.onSearchClick,
                      onDistrictClick: () {
                        analytics.logEvent(name: 'view_search_by_district');
                        _askDistrict();
                      },
                      onCityClick: () {
                        _askCity();
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
          if (enableFeatureTrendingSearch)
            Container(
              margin: EdgeInsets.fromLTRB(Const.MARGIN, 0, 0, Const.MARGIN),
              child: Text(
                "Xu hướng tìm kiếm",
                style: TextStyle(fontSize: 18, color: Colors.black54),
              ),
            ),
          if (enableFeatureTrendingSearch)
            Container(
              margin: EdgeInsets.fromLTRB(Const.MARGIN, 0, Const.MARGIN, 0),
              child: _buildSuggestGrid(),
            ),
          if (enableFeatureGoodPriceList)
            _buildListComponent('good_price', listGoodPrice),
          if (enableFeatureNewestList)
            _buildListComponent('newest', listNewest),
          Container(
            height: 100,
          )
        ],
      ),
    );
  }

  Future<void> _askDistrict() async {
    if (selectedCity == null) return;
    SimpleModel result = await showDialog<SimpleModel>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Chọn Quận/Huyện'),
            children: Repository.shared
                .districts(selectedCity.id)
                .map((e) => SimpleDialogOption(
                      child: _buildOptionWidget(e.displayText),
                      onPressed: () {
                        Navigator.pop(context, e);
                      },
                    ))
                .toList(),
          );
        });
    if (result != null) {
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => RoomSearchWidget(
          initDistrict: result,
        ),
      ));
    }
  }

  Future<void> _askCity() async {
    SimpleModel result = await showDialog<SimpleModel>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Chọn thành phố'),
            children: Repository.shared.cities
                .map((e) => SimpleDialogOption(
                      child: _buildOptionWidget(e.displayText),
                      onPressed: () {
                        Navigator.pop(context, e);
                      },
                    ))
                .toList(),
          );
        });
    if (result != null) {
      await PreferencesHelper.setString(Const.KEY_SELECTED_CITY, result.id);
      await Repository.shared.fetchCitiesAndCache();
      setState(() {
        selectedCity = Repository.shared.selectedCity;
        fetchHomeListNewestRoom();
        fetchHomeListGoodPriceRoom();
      });
    }
  }

  Widget _buildOptionWidget(String title) {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 5),
      child: Text(
        title,
        style: TextStyle(
          color: Colors.black87,
          fontSize: 15,
        ),
      ),
    );
  }

  Widget _buildBannerSlider() {
    return Container(
      child: CarouselSlider(
        options: CarouselOptions(
          enlargeCenterPage: false,
          viewportFraction: 1,
          autoPlay: true,
          height: 250,
        ),
        items: (selectedCity == null ? [] : selectedCity.banners)
            .map((item) => Container(
                  child: Center(
                    child: Image.network(
                      item,
                      fit: BoxFit.cover,
                      height: 250,
                      width: 1000,
                    ),
                  ),
                ))
            .toList(),
      ),
    );
  }

  Widget _buildListComponent(String type, List<RoomModel> rooms) {
    return Column(
      children: [
        Container(
          margin:
              EdgeInsets.fromLTRB(Const.MARGIN, Const.MARGIN, 0, Const.MARGIN),
          child: Row(
            children: [
              Container(
                child: Text(
                  type == 'good_price' ? 'Giá tốt' : 'Mới nhất',
                  style: TextStyle(fontSize: 18, color: Colors.black54),
                ),
                margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
              ),
              Icon(
                type == 'good_price' ? Icons.monetization_on : Icons.fiber_new,
                color: Color(type == 'good_price'
                    ? Const.primaryColor
                    : Const.secondaryColor),
                size: 18,
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(Const.MARGIN, 0, Const.MARGIN, 0),
          child: _buildListPost(type, rooms),
        ),
        Container(
          height: 1,
          margin:
              EdgeInsets.fromLTRB(Const.MARGIN, Const.MARGIN, Const.MARGIN, 0),
          decoration: BoxDecoration(color: Colors.grey[300]),
        ),
        FlatButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => RoomListWidget(
                        type: type,
                      )),
            );
          },
          child: Text(
            "Xem tất cả",
            style: TextStyle(
                color: Colors.blueGrey,
                fontSize: 15,
                fontWeight: FontWeight.w900),
          ),
        ),
        Container(
          height: 20,
          decoration: BoxDecoration(color: Colors.grey[200]),
        ),
      ],
    );
  }

  GridView _buildListPost(String type, List<RoomModel> rooms) {
    var size = MediaQuery.of(context).size;
    return GridView.count(
      primary: false,
      shrinkWrap: true,
      crossAxisSpacing: Const.MARGIN,
      mainAxisSpacing: Const.MARGIN,
      childAspectRatio: ((size.width - 3 * Const.MARGIN) / 2) / 260,
      crossAxisCount: 2,
      children: rooms.map((room) => _buildPostItem(type, room)).toList(),
    );
  }

  Widget _buildPostItem(String type, RoomModel room) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => RoomDetailWidget(
                    room: room,
                  )),
        );
      },
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  child: Image.network(
                    room.images[0],
                    fit: BoxFit.cover,
                    width: 1000,
                    height: 110,
                  ),
                ),
                Container(
                  width: 1000,
                  height: 110,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment(0, 0.1),
                        colors: [
                          Colors.black.withAlpha(120),
                          Colors.transparent
                        ],
                      )),
                ),
                Positioned(
                    top: 5,
                    right: 5,
                    child: Icon(
                      type == 'good_price'
                          ? Icons.monetization_on
                          : Icons.fiber_new,
                      color: Color(type == 'good_price'
                          ? Const.primaryColor
                          : Const.secondaryColor),
                      size: 18,
                    )),
              ],
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Row(
                children: [
                  Text(
                    "${Helper.getDisplayTextRoomType(room.type).toUpperCase()} . ",
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 12,
                    ),
                  ),
                  Text(
                      "${room.capacity} ${Helper.getDisplayTextUnicodeGender(room.gender)}",
                      style: TextStyle(
                        color: Colors.grey[400],
                        fontSize: 12,
                      )),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
              child: Text(
                room.title,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.w600),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.start,
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
              child: Text(
                "${(room.price / 1000000).formattedValue} triệu VNĐ/${Helper.getUnitTextRoomType(room.type)}",
                style: TextStyle(
                  color: Colors.blueAccent,
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.justify,
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
              child: Text(
                "${room.address.addressDescription}",
                style: TextStyle(
                  color: Colors.grey[600],
                  fontSize: 13,
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.start,
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
                child: Text(
                  "${room.address.district != null ? room.address.district.displayText : ''}",
                  style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 13,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSuggestGrid() {
    double width = MediaQuery.of(context).size.width - 40;
    double itemWidth = (width - 2 * 5) / 3;
    List<SimpleModel> districts = selectedCity == null
        ? []
        : Repository.shared.districts(selectedCity.id);
    return Column(
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              if (districts.length > 0)
                _buildSuggestItem(districts[0], itemWidth),
              if (districts.length > 1)
                _buildSuggestItem(districts[1], itemWidth),
              if (districts.length > 2)
                _buildSuggestItem(districts[2], itemWidth),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              if (districts.length > 3)
                _buildSuggestItem(districts[3], itemWidth),
              if (districts.length > 4)
                _buildSuggestItem(districts[4], itemWidth),
              if (districts.length > 5)
                _buildSuggestItem(districts[5], itemWidth),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildSuggestItem(SimpleModel district, double width) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => RoomSearchWidget(
            initDistrict: district,
          ),
        ));
      },
      child: Stack(
        alignment: Alignment.center,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            child: Image.network(
              district.image,
              fit: BoxFit.cover,
              width: width,
              height: width,
            ),
          ),
          Container(
            width: width,
            height: width,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomCenter,
                end: Alignment(0, 0.2),
                colors: [Colors.black, Colors.transparent],
              ),
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
          ),
          Positioned(
            bottom: 8,
            child: Container(
              child: Text(
                district.displayText,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 12),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class ControlPanel extends StatelessWidget {
  const ControlPanel(
      {this.selectedCity,
      this.enableFeatureSearchByDistrict,
      this.onSearchClick,
      this.onPostClick,
      this.onDistrictClick,
      this.onMapClick,
      this.onCityClick})
      : super();

  final VoidCallback onSearchClick,
      onPostClick,
      onMapClick,
      onDistrictClick,
      onCityClick;
  final SimpleModel selectedCity;
  final bool enableFeatureSearchByDistrict;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width - 2 * Const.MARGIN;

    Color color = Theme.of(context).primaryColor;
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => RoomSearchWidget(),
            ));
          },
          child: Container(
            width: width,
            child: Container(
              height: 35,
              margin: EdgeInsets.fromLTRB(15, 15, 15, 5),
              decoration: BoxDecoration(
                  color: Colors.black.withAlpha(10),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: _buildSearchBarRow(),
            ),
          ),
        ),
        Container(
          width: width,
          height: 80,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              if (enableFeatureSearchByDistrict)
                InkWell(
                  child:
                      _buildButtonColumn(color, Icons.search, "Tìm theo quận"),
                  onTap: onDistrictClick,
                ),
              InkWell(
                child: _buildButtonColumn(color, Icons.create, "Đăng tin"),
                onTap: onPostClick,
              ),
              InkWell(
                child: _buildButtonColumn(color, Icons.map, "Gần đây"),
                onTap: onMapClick,
              ),
            ],
          ),
        )
      ],
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w900,
              color: color,
            ),
          ),
        ),
      ],
    );
  }

  Row _buildSearchBarRow() {
    return Row(
      children: [
        InkWell(
          onTap: () {
            onCityClick();
          },
          child: Container(
            height: 35,
            padding: EdgeInsets.symmetric(horizontal: 10),
            decoration: BoxDecoration(
              color: Colors.black.withAlpha(20),
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Icon(
                  Icons.person_pin_circle,
                  color: Colors.blueAccent,
                  size: 15,
                ),
                Text(
                  selectedCity == null ? "" : selectedCity.displayText,
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w900,
                      color: Colors.blueAccent),
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(),
            child: Center(
              child: Text(
                "Tìm theo quận, tên đường, địa chỉ...",
                style: TextStyle(
                  color: Colors.black26,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        )
      ],
    );
  }
}
