import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:trolanha/enums.dart';
import 'package:trolanha/models/friend_filter_model.dart';
import 'package:trolanha/models/simple_model.dart';

import '../../Const.dart';
import '../../repository.dart';

class RoomUtilityEntry {
  const RoomUtilityEntry(this.name, this.key, this.icon);

  final String name;
  final RoomUtility key;
  final IconData icon;
}

class FriendSearchWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => FriendSearchState();
}

class FriendSearchState extends State<FriendSearchWidget> {
  List<RoomUtility> _filterFacilities = <RoomUtility>[];
  List<SimpleModel> _filterAddresses = <SimpleModel>[];
  var _filterPriceRange = RangeValues(5, 100);
  Gender _filterGenderType = Gender.all;

  final List<RoomUtilityEntry> _cast = <RoomUtilityEntry>[
    const RoomUtilityEntry('Thú cưng', RoomUtility.pet, Icons.pets),
    const RoomUtilityEntry('Tivi', RoomUtility.tivi, Icons.tv),
    const RoomUtilityEntry('Tủ đồ', RoomUtility.closet, Icons.inbox),
    const RoomUtilityEntry('Giường', RoomUtility.bed, Icons.hotel),
    const RoomUtilityEntry('Gác lửng', RoomUtility.mezz, Icons.trending_up),
    const RoomUtilityEntry('Máy giặt', RoomUtility.wash, Icons.crop_portrait),
    const RoomUtilityEntry('Tủ lạnh', RoomUtility.frid, Icons.ac_unit),
    const RoomUtilityEntry('Nhà bếp', RoomUtility.kit, Icons.kitchen),
    const RoomUtilityEntry('Máy nước nóng', RoomUtility.heat, Icons.whatshot),
    const RoomUtilityEntry('WC riêng', RoomUtility.wc, Icons.wc),
    const RoomUtilityEntry('Chỗ để xe', RoomUtility.park, Icons.local_parking),
    const RoomUtilityEntry('An ninh', RoomUtility.secure, Icons.lock),
    const RoomUtilityEntry('Cửa sổ', RoomUtility.window, Icons.crop_square),
    const RoomUtilityEntry('Wifi', RoomUtility.wifi, Icons.wifi),
    const RoomUtilityEntry('Tự do', RoomUtility.free, Icons.motorcycle),
    const RoomUtilityEntry(
        'Chủ riêng', RoomUtility.privacy, Icons.account_circle),
  ];

  SimpleModel selectedCity = Repository.shared.selectedCity;
  List<SimpleModel> districts = [];

  String getDisplayTextUtility(RoomUtility utility) {
    switch (utility) {
      case RoomUtility.pet:
        return "Thú cưng";
      case RoomUtility.tivi:
        return "Tivi";
      case RoomUtility.closet:
        return "Tủ đồ";
      case RoomUtility.bed:
        return "Giường";
      case RoomUtility.mezz:
        return "Gác lửng";
      case RoomUtility.wash:
        return "Máy giặt";
      case RoomUtility.frid:
        return "Tủ lạnh";
      case RoomUtility.kit:
        return "Nhà bếp";
      case RoomUtility.heat:
        return "Máy nước nóng";
      case RoomUtility.wc:
        return "WC riêng";
      case RoomUtility.park:
        return "Chỗ để xe";
      case RoomUtility.secure:
        return "An ninh";
      case RoomUtility.window:
        return "Cửa sổ";
      case RoomUtility.wifi:
        return "Wifi";
      case RoomUtility.free:
        return "Tự do";
      case RoomUtility.privacy:
        return "Chủ riêng";
    }
    return "";
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      districts = Repository.shared.districts(selectedCity.id);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Bộ lọc",
          style: TextStyle(fontSize: 17),
        ),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.settings_backup_restore),
            onPressed: () {},
          )
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: Center(
              child: ListView(
                children: [
                  _buildFilterTypePriceWidget(),
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    height: 5,
                    decoration: BoxDecoration(color: Colors.grey[200]),
                  ),
                  _buildFilterGenderWidget(),
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    height: 5,
                    decoration: BoxDecoration(color: Colors.grey[200]),
                  ),
                  _buildAddressWidget(),
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    height: 5,
                    decoration: BoxDecoration(color: Colors.grey[200]),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 3,
            decoration: BoxDecoration(
                color: Colors.grey,
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [Colors.grey.withAlpha(30), Colors.transparent],
                )),
          ),
          Container(
            padding:
                EdgeInsets.symmetric(vertical: 15, horizontal: Const.MARGIN),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: RaisedButton(
                      padding: EdgeInsets.all(15),
                      onPressed: () {
                        int numberOfFilter = 0;
                        Query query = Firestore.instance.collection("friends");
                        if (_filterPriceRange.start != 5 || _filterPriceRange.end != 100) {
                          numberOfFilter++;
                          query = query.where('maximumPrice', isLessThanOrEqualTo: _filterPriceRange.end * 100000);
                        }
                        if (_filterGenderType != Gender.all) {
                          numberOfFilter++;
                          query = query.where('gender', isEqualTo: _filterGenderType.enumValue);
                        }
                        if (_filterAddresses.length > 0) {
                          numberOfFilter += _filterAddresses.length;
                          List<String> ids = _filterAddresses.map((e) => e.id).toList();
                          query = query.where('addresses', arrayContainsAny: ids);
                        }
                        query = query.orderBy('maximumPrice', descending: false);

                        FriendFilterModel filterModel = FriendFilterModel(query, numberOfFilter);
                        Navigator.pop(context, filterModel);
                      },
                      color: Colors.blueAccent,
                      child: Text(
                        "Áp dụng bộ lọc".toUpperCase(),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildFilterGenderWidget() {
    return Container(
      margin: EdgeInsets.only(
          left: Const.MARGIN, right: Const.MARGIN, bottom: Const.MARGIN),
      child: Column(
        children: [
          Container(
            height: 50,
            child: Row(
              children: [
                Container(
                  padding: EdgeInsets.only(right: 20),
                  child: Text(
                    "Giới tính",
                    style: Const.styleH2,
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        _buildGenderItemWidget(Gender.all),
                        _buildGenderItemWidget(Gender.male),
                        _buildGenderItemWidget(Gender.female),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildGenderItemWidget(Gender gender) {
    return Expanded(
      child: InkWell(
        onTap: () {
          setState(() {
            _filterGenderType = gender;
          });
        },
        child: Container(
          alignment: Alignment.center,
          height: 40,
          decoration: _filterGenderType == gender
              ? BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            color: Colors.white,
            border: Border.all(width: 1, color: Colors.blueAccent),
          )
              : BoxDecoration(),
          child: Text(
            getDisplayTextGender(gender),
            style: TextStyle(
                color: _filterGenderType == gender
                    ? Colors.blueAccent
                    : Colors.black),
          ),
        ),
      ),
    );
  }

  String getDisplayTextGender(Gender gender) {
    switch (gender) {
      case Gender.all:
        return "Tất cả";
      case Gender.male:
        return "Nam";
      case Gender.female:
        return "Nữ";
    }
    return "";
  }


  Widget _buildFilterTypePriceWidget() {
    return Container(
      padding: EdgeInsets.only(
          left: Const.MARGIN, right: Const.MARGIN, top: Const.MARGIN),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 20, ),
            child: Text(
              "Giá",
              style: Const.styleH2,
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("${formatPrice(_filterPriceRange.start)}"),
                Text("${formatPrice(_filterPriceRange.end)}"),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: Const.MARGIN, right: Const.MARGIN),
            height: 70,
            child: RangeSlider(
                min: 5,
                max: 100,
                divisions: 19,
                values: _filterPriceRange,
                labels: RangeLabels('${formatPrice(_filterPriceRange.start)}',
                    '${formatPrice(_filterPriceRange.end)}'),
                onChanged: (RangeValues newRange) {
                  setState(() {
                    _filterPriceRange = newRange;
                  });
                }),
          ),
        ],
      ),
    );
  }

  String formatPrice(double price) {
    var formatter = new NumberFormat("##,###,###");
    return formatter.format(price * 100000) + " VNĐ";
  }

  Widget _buildAddressWidget() {
    return Container(
      padding: EdgeInsets.all(Const.MARGIN),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Text(
              "Quận",
              style: Const.styleH2,
            ),
          ),
          Wrap(
            children: addressWidgets.toList(),
          )
        ],
      ),
    );
  }

  Iterable<Widget> get addressWidgets sync* {
    for (final SimpleModel district in districts) {
      yield Container(
        margin: EdgeInsets.only(right: Const.MARGIN),
        child: FilterChip(
          selectedColor: Colors.blueAccent,
          backgroundColor: Colors.grey[200],
          padding: EdgeInsets.all(5),
          label: Text(district.displayText),
          selected: _filterAddresses.contains(district),
          onSelected: (bool value) {
            setState(() {
              if (value) {
                _filterAddresses.add(district);
              } else {
                _filterAddresses.removeWhere((SimpleModel name) {
                  return name.displayText == district.displayText;
                });
              }
            });
          },
        ),
      );
    }
  }



  Widget _buildUtilityWidget() {
    return Container(
      padding: EdgeInsets.all(Const.MARGIN),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Text(
              "Tiện ích",
              style: Const.styleH2,
            ),
          ),
          Wrap(
            children: actorWidgets.toList(),
          )
        ],
      ),
    );
  }

  Iterable<Widget> get actorWidgets sync* {
    for (final RoomUtilityEntry utility in _cast) {
      yield Container(
        margin: EdgeInsets.only(right: Const.MARGIN),
        child: FilterChip(
          selectedColor: Colors.blueAccent,
          backgroundColor: Colors.grey[200],
          padding: EdgeInsets.all(5),
          avatar: CircleAvatar(
              child: Icon(
            utility.icon,
            size: 12,
          )),
          label: Text(utility.name),
          selected: _filterFacilities.contains(utility.key),
          onSelected: (bool value) {
            setState(() {
              if (value) {
                _filterFacilities.add(utility.key);
              } else {
                _filterFacilities.removeWhere((RoomUtility name) {
                  return name == utility.key;
                });
              }
            });
          },
        ),
      );
    }
  }

  RoomUtilityEntry getUtilityEntry(RoomUtility utility) {
    switch (utility) {
      case RoomUtility.pet:
        return RoomUtilityEntry('Thú cưng', RoomUtility.pet, Icons.pets);
      case RoomUtility.tivi:
        return RoomUtilityEntry('Tivi', RoomUtility.tivi, Icons.tv);
      case RoomUtility.closet:
        return RoomUtilityEntry('Tủ đồ', RoomUtility.closet, Icons.inbox);
      case RoomUtility.bed:
        return RoomUtilityEntry('Giường', RoomUtility.bed, Icons.hotel);
      case RoomUtility.mezz:
        return RoomUtilityEntry(
            'Gác lửng', RoomUtility.mezz, Icons.trending_up);
      case RoomUtility.wash:
        return RoomUtilityEntry(
            'Máy giặt', RoomUtility.wash, Icons.crop_portrait);
      case RoomUtility.frid:
        return RoomUtilityEntry('Tủ lạnh', RoomUtility.frid, Icons.ac_unit);
      case RoomUtility.kit:
        return RoomUtilityEntry('Nhà bếp', RoomUtility.kit, Icons.kitchen);
      case RoomUtility.heat:
        return RoomUtilityEntry(
            'Máy nước nóng', RoomUtility.heat, Icons.whatshot);
      case RoomUtility.wc:
        return RoomUtilityEntry('WC riêng', RoomUtility.wc, Icons.wc);
      case RoomUtility.park:
        return RoomUtilityEntry(
            'Chỗ để xe', RoomUtility.park, Icons.local_parking);
      case RoomUtility.secure:
        return RoomUtilityEntry('An ninh', RoomUtility.secure, Icons.lock);
      case RoomUtility.window:
        return RoomUtilityEntry(
            'Cửa sổ', RoomUtility.window, Icons.crop_square);
      case RoomUtility.wifi:
        return RoomUtilityEntry('Wifi', RoomUtility.wifi, Icons.wifi);
      case RoomUtility.free:
        return RoomUtilityEntry('Tự do', RoomUtility.free, Icons.motorcycle);
      case RoomUtility.privacy:
        return RoomUtilityEntry(
            'Chủ riêng', RoomUtility.privacy, Icons.account_circle);
    }
  }
}
