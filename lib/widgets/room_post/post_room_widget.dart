import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:trolanha/customs/dialogs.dart';
import 'package:trolanha/models/room_model.dart';
import 'package:trolanha/models/user_model.dart';
import 'package:trolanha/widgets/room_detail/room_detail_widget.dart';
import 'package:trolanha/widgets/room_post/post_room_address_widget.dart';
import 'package:trolanha/widgets/room_post/post_room_confirm_widget.dart';
import 'package:trolanha/widgets/room_post/post_room_info_widget.dart';
import 'package:trolanha/widgets/room_post/post_room_utility_widget.dart';
import 'package:uuid/uuid.dart';

import '../../Const.dart';
import '../../shared_preferences_helper.dart';

class PostRoomWidget extends StatefulWidget {

  final RoomModel initialRoom;

  PostRoomWidget({this.initialRoom}) : super();

  @override
  State createState() {
    return PostRoomState();
  }
}

class PostRoomState extends State<PostRoomWidget>
    with SingleTickerProviderStateMixin {
  List<Widget> _children;

  final GlobalKey<PostRoomInfoState> _roomInfoKey =
      GlobalKey<PostRoomInfoState>();
  final GlobalKey<PostRoomAddressState> _roomAddressKey =
      GlobalKey<PostRoomAddressState>();
  final GlobalKey<PostRoomUtilityState> _roomUtilityKey =
      GlobalKey<PostRoomUtilityState>();
  final GlobalKey<PostRoomConfirmState> _roomConfirmKey =
      GlobalKey<PostRoomConfirmState>();
  final GlobalKey<State> _loadingKey = new GlobalKey<State>();

  TabController _tabController;

  List<bool> _isDisabled = [false, true, true, true];

  RoomModel _room;

  FirebaseStorage _storage;

  bool isUpdateFlow = false;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: 4, vsync: this);
    _tabController.addListener(() {
      if (_isDisabled[_tabController.index]) {
        int index = _tabController.previousIndex;
        setState(() {
          _tabController.index = index;
        });
      }
    });
    _storage = FirebaseStorage(
        app: FirebaseApp.instance,
        storageBucket: "gs://trolanha-with-firebase.appspot.com");
    isUpdateFlow = widget.initialRoom != null;
    if (isUpdateFlow) {
      _room = widget.initialRoom;
      _isDisabled = [false, false, false, false];
      setState(() {
        _tabController.index = 3;
      });
    } else {
      _room = new RoomModel();
    }
    _children = [
      PostRoomInfoWidget(
        initialRoom: widget.initialRoom,
        key: _roomInfoKey,
        onContinue: () {
          RoomModel roomInfo = this._roomInfoKey.currentState.getRoom();
          _room.type = roomInfo.type;
          _room.gender = roomInfo.gender;
          _room.quantity = roomInfo.quantity;
          _room.capacity = roomInfo.capacity;
          _room.area = roomInfo.area;
          _room.price = roomInfo.price;
          _room.deposit = roomInfo.deposit;
          _room.priceElectricPerUnit = roomInfo.priceElectricPerUnit;
          _room.priceWaterPerUnit = roomInfo.priceWaterPerUnit;
          _room.priceInternet = roomInfo.priceInternet;
          _room.priceParking = roomInfo.priceParking;

          _isDisabled[1] = false;
          setState(() {
            _tabController.index = 1;
          });
        },
      ),
      PostRoomAddressWidget(
        initialRoom: widget.initialRoom,
        key: _roomAddressKey,
        onContinue: () {
          _room.address = _roomAddressKey.currentState.getAddress();
          _room.latLng = _roomAddressKey.currentState.getLatLng();
          print('search text ok latlng: ${_room.latLng.latitude}');

          _isDisabled[2] = false;
          setState(() {
            _tabController.index = 2;
          });
        },
      ),
      PostRoomUtilityWidget(
        initialRoom: widget.initialRoom,
        key: _roomUtilityKey,
        onContinue: () {
          RoomModel room = _roomUtilityKey.currentState.getRoom();
          _room.utilities = room.utilities;
          _room.images = room.images;

          _isDisabled[3] = false;
          setState(() {
            _tabController.index = 3;
          });
        },
      ),
      PostRoomConfirmWidget(
        initialRoom: widget.initialRoom,
        key: _roomConfirmKey,
        onPost: this.postRoom,
      ),
    ];
  }

  void postRoom() async {
    Dialogs.showLoadingDialog(context, _loadingKey);
    RoomModel room = _roomConfirmKey.currentState.getRoom();
    _room.title = room.title;
    _room.description = room.description;

    UserModel user = UserModel.fromMap(
        jsonDecode(await PreferencesHelper.getString(Const.KEY_CACHED_USER)),
        '');

    FirebaseUser fbUser = await FirebaseAuth.instance.currentUser();
    _room.user = fbUser.uid;
    _room.lastUpdatedAt = Timestamp.now();
    _room.isApproved = false;
    _room.isVerified = false;

    // Update phoneNumber and check isPostedRoom
    Firestore.instance.collection("users").document(fbUser.uid).updateData({
      'phoneNumber': _roomConfirmKey.currentState.getPhoneNumber(),
      'isPostedRoom': true,
    });

    try {
      // Upload image
      List<String> imgUrls = [];
      for (int i = 0; i < _room.images.length; i++) {
        String image = _room.images[i];
        if (image.contains("http")) {
          imgUrls.add(image);
        } else {
          StorageReference ref = _storage
              .ref()
              .child("images")
              .child("${fbUser.uid}/${Uuid().v1()}");
          StorageUploadTask uploadTask = ref.putFile(File(image));
          await uploadTask.onComplete;
          String imageUrl = await ref.getDownloadURL();
          imgUrls.add(imageUrl);
        }
      }
      _room.images = imgUrls;

      // Save room
      if (isUpdateFlow) {
        await Firestore.instance.collection("rooms").document(_room.uuid).updateData(_room.toMap());
      } else {
        await Firestore.instance.collection("rooms").add(_room.toMap());
      }
    } catch (error) {
      Scaffold.of(context).showSnackBar(const SnackBar(
        content: Text('Có lỗi xảy ra, vui lòng thử lại'),
      ));
      print("Failed to add rooms: $error");
    } finally {
      Navigator.of(_loadingKey.currentContext).pop();
      Navigator.pop(context);
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => RoomDetailWidget(
                  room: _room,
                )),
      );
    }
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.blueAccent,
              bottom: TabBar(
                controller: _tabController,
                tabs: [
                  Tab(
                    icon: Icon(Icons.place),
                    text: "Thông tin",
                  ),
                  Tab(
                    icon: Icon(Icons.info),
                    text: "Địa chỉ",
                  ),
                  Tab(
                    icon: Icon(Icons.assignment),
                    text: "Tiện ích",
                  ),
                  Tab(
                    icon: Icon(Icons.verified_user),
                    text: "Xác nhận",
                  ),
                ],
              ),
              title: Text('Đăng bài'),
              centerTitle: true,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              )),
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            controller: _tabController,
            children: _children,
          ),
        ),
      ),
    );
  }
}
