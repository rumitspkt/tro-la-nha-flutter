import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:trolanha/enums.dart';
import 'package:trolanha/models/room_model.dart';

import '../../Const.dart';

class RoomUtilityEntry {
  const RoomUtilityEntry(this.name, this.key, this.icon);

  final String name;
  final RoomUtility key;
  final IconData icon;
}

class PostRoomUtilityWidget extends StatefulWidget {
  PostRoomUtilityWidget({Key key, this.onContinue, this.initialRoom})
      : super(key: key);
  final VoidCallback onContinue;
  final RoomModel initialRoom;

  @override
  State<StatefulWidget> createState() => PostRoomUtilityState();
}

class PostRoomUtilityState extends State<PostRoomUtilityWidget>
    with AutomaticKeepAliveClientMixin {
  final List<RoomUtilityEntry> _cast = <RoomUtilityEntry>[
    const RoomUtilityEntry('Thú cưng', RoomUtility.pet, Icons.pets),
    const RoomUtilityEntry('Tivi', RoomUtility.tivi, Icons.tv),
    const RoomUtilityEntry('Tủ đồ', RoomUtility.closet, Icons.inbox),
    const RoomUtilityEntry('Giường', RoomUtility.bed, Icons.hotel),
    const RoomUtilityEntry('Gác lửng', RoomUtility.mezz, Icons.trending_up),
    const RoomUtilityEntry('Máy giặt', RoomUtility.wash, Icons.crop_portrait),
    const RoomUtilityEntry('Tủ lạnh', RoomUtility.frid, Icons.ac_unit),
    const RoomUtilityEntry('Nhà bếp', RoomUtility.kit, Icons.kitchen),
    const RoomUtilityEntry('Máy nước nóng', RoomUtility.heat, Icons.whatshot),
    const RoomUtilityEntry('WC riêng', RoomUtility.wc, Icons.wc),
    const RoomUtilityEntry('Chỗ để xe', RoomUtility.park, Icons.local_parking),
    const RoomUtilityEntry('An ninh', RoomUtility.secure, Icons.lock),
    const RoomUtilityEntry('Cửa sổ', RoomUtility.window, Icons.crop_square),
    const RoomUtilityEntry('Wifi', RoomUtility.wifi, Icons.wifi),
    const RoomUtilityEntry('Tự do', RoomUtility.free, Icons.motorcycle),
    const RoomUtilityEntry(
        'Chủ riêng', RoomUtility.privacy, Icons.account_circle),
  ];
  List<RoomUtility> _filters = <RoomUtility>[];

  final int _maxNumberImage = 10;
  List<String> _images = [];

  RoomModel getRoom() {
    RoomModel room = new RoomModel();
    room.utilities = _filters;
    room.images = _images;
    return room;
  }

  final picker = ImagePicker();

  Future getImage(ImageSource imageSource) async {
    final pickedFile =
        await picker.getImage(source: imageSource, imageQuality: 30);
    setState(() {
      _images.add(pickedFile.path);
    });
  }

  Iterable<Widget> get actorWidgets sync* {
    for (final RoomUtilityEntry utility in _cast) {
      yield Container(
        margin: EdgeInsets.only(right: Const.MARGIN),
        child: FilterChip(
          selectedColor: Colors.blueAccent,
          backgroundColor: Colors.grey[200],
          padding: EdgeInsets.all(5),
          avatar: CircleAvatar(
              child: Icon(
            utility.icon,
            size: 12,
          )),
          label: Text(utility.name),
          selected: _filters.contains(utility.key),
          onSelected: (bool value) {
            setState(() {
              if (value) {
                _filters.add(utility.key);
              } else {
                _filters.removeWhere((RoomUtility name) {
                  return name == utility.key;
                });
              }
            });
          },
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    if (widget.initialRoom != null) {
      setState(() {
        this._filters = widget.initialRoom.utilities;
        this._images = widget.initialRoom.images;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Const.MARGIN),
      child: ListView(
        children: [
          _buildImageWidget(),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN, top: Const.MARGIN),
            child: Text(
              "Tiện ích".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Wrap(
            children: actorWidgets.toList(),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN, top: Const.MARGIN),
          ),
          FlatButton(
            child: Text(
              "Tiếp theo",
              style: TextStyle(color: Colors.blueAccent, fontSize: 18),
            ),
            onPressed: () {
              if (_images.length < 4) {
                Scaffold.of(context).showSnackBar(const SnackBar(
                  content: Text('Vui lòng chọn ít nhất 4 ảnh'),
                ));
                return;
              }
              widget.onContinue();
            },
          )
        ],
      ),
    );
  }

  Widget _buildImageWidget() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: Text(
              "Thông tin hình ảnh và tiện ích",
              style: Const.styleH2,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: Text(
              "Hình ảnh".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(
                    color: Colors.grey[300],
                    style: BorderStyle.solid,
                    width: 1)),
            child: _buildListPost(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              OutlineButton.icon(
                onPressed: () {
                  getImage(ImageSource.gallery);
                },
                icon: Icon(
                  Icons.image,
                  size: 15,
                ),
                label: Text("Chọn ảnh"),
              ),
              OutlineButton.icon(
                  onPressed: () {
                    getImage(ImageSource.camera);
                  },
                  icon: Icon(
                    Icons.camera_alt,
                    size: 15,
                  ),
                  label: Text("Chụp ảnh mới"))
            ],
          ),
        ],
      ),
    );
  }

  GridView _buildListPost() {
    double width =
        MediaQuery.of(context).size.width - 2 * Const.MARGIN - 2 * 10;
    double itemWidth = (width - 3 * 5) / 4;
    return GridView.count(
        primary: false,
        shrinkWrap: true,
        crossAxisSpacing: 5,
        mainAxisSpacing: 5,
        childAspectRatio: 1,
        crossAxisCount: 4,
        children: _getListWidget(itemWidth));
  }

  List<Widget> _getListWidget(double itemWidth) {
    List<Widget> list = [];
    for (int i = 0; i < _images.length; i++) {
      list.add(_buildImageItem(itemWidth, _images[i], i));
    }
    for (int i = list.length; i < _maxNumberImage; i++) {
      list.add(_buildPlaceHolderItem(itemWidth));
    }
    return list;
  }

  Image getImageFromUrl(String url, double width) {
    if (url.contains("http")) {
      return Image.network(
        url,
        fit: BoxFit.cover,
        width: width,
        height: width,
      );
    } else {
      return Image.file(
        File(url),
        fit: BoxFit.cover,
        width: width,
        height: width,
      );
    }
  }

  Widget _buildImageItem(double width, String link, int position) {
    return Stack(
      alignment: Alignment.center,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: getImageFromUrl(link, width),
        ),
        Positioned(
          top: 5,
          right: 5,
          child: InkWell(
            onTap: () {
              setState(() {
                _images.removeAt(position);
              });
            },
            child: Icon(
              Icons.delete,
              size: 30,
              color: Colors.blueAccent,
            ),
          ),
        )
      ],
    );
  }

  Widget _buildPlaceHolderItem(double width) {
    return GestureDetector(
      onTap: () {
        getImage(ImageSource.gallery);
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.grey[200],
        ),
        child: Icon(
          Icons.image,
          size: 30,
          color: Colors.grey[600],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
