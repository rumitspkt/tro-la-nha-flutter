import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trolanha/models/room_model.dart';

import '../../Const.dart';
import '../../enums.dart';

class PostRoomInfoWidget extends StatefulWidget {

  PostRoomInfoWidget({Key key, this.onContinue, this.initialRoom}) : super(key: key);
  final VoidCallback onContinue;
  final RoomModel initialRoom;

  @override
  State<StatefulWidget> createState() => PostRoomInfoState();
}

class PostRoomInfoState extends State<PostRoomInfoWidget> with AutomaticKeepAliveClientMixin {
  RoomType _roomType = RoomType.dorm;
  Gender _gender = Gender.all;
  final _formKey = GlobalKey<FormState>();
  final _quantityRoomController = TextEditingController();
  final _capacityRoomController = TextEditingController();
  final _areaRoomController = TextEditingController();
  final _priceRoomController = TextEditingController();
  final _depositRoomController = TextEditingController();
  final _electricPriceController = TextEditingController();
  final _waterPriceController = TextEditingController();
  final _internetPriceController = TextEditingController();
  final _parkingPriceController = TextEditingController();

  var _isFreeElectric = false;
  var _isFreeWater = false;
  var _isFreeInternet = false;
  var _isFreeParking = false;
  var _isHaveParking = false;

  RoomModel getRoom() {
    RoomModel room = new RoomModel();
    room.type = _roomType;
    room.gender = _gender;
    room.quantity = int.parse(_quantityRoomController.text);
    room.capacity = int.parse(_capacityRoomController.text);
    room.area = int.parse(_areaRoomController.text);
    room.price = int.parse(_priceRoomController.text);
    room.deposit = int.parse(_depositRoomController.text);
    room.priceElectricPerUnit = _isFreeElectric ? 0 : int.parse(_electricPriceController.text);
    room.priceWaterPerUnit = _isFreeWater ? 0 : int.parse(_waterPriceController.text);
    room.priceInternet = _isFreeInternet ? 0 : int.parse(_internetPriceController.text);
    if (_isHaveParking) {
      room.priceParking = _isFreeParking ? 0 : int.parse(_parkingPriceController.text);
    }
    return room;
  }

  @override
  void initState() {
    super.initState();
    if (widget.initialRoom != null) {
      RoomModel room = widget.initialRoom;
      setState(() {
        _isFreeElectric = room.priceElectricPerUnit == 0;
        _isFreeWater = room.priceWaterPerUnit == 0;
        _isFreeInternet = room.priceInternet == 0;
        _isFreeParking = room.priceParking == 0;
        _isHaveParking = room.priceParking != null;

        _roomType = room.type;
        _gender = room.gender;
        _quantityRoomController.text = room.quantity.toString();
        _capacityRoomController.text = room.capacity.toString();
        _areaRoomController.text = room.area.toString();
        _priceRoomController.text = room.price.toString();
        _depositRoomController.text = room.deposit.toString();
        _electricPriceController.text =  _isFreeElectric ? 'Miễn phí' : room.priceElectricPerUnit.toString();
        _waterPriceController.text = _isFreeWater ? 'Miễn phí' : room.priceWaterPerUnit.toString();
        _internetPriceController.text = _isFreeInternet ? 'Miễn phí' : room.priceInternet.toString();
        if (_isHaveParking) {
          _parkingPriceController.text = _isFreeElectric ? 'Miễn phí' : room.priceParking.toString();
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Const.MARGIN),
      child: Form(
        key: _formKey,
        child: ListView(
          children: [
            _buildInfoWidget(),
            _buildPriceWidget(),
            FlatButton(
              child: Text(
                "Tiếp theo",
                style: TextStyle(color: Colors.blueAccent, fontSize: 18),
              ),
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  widget.onContinue();
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _quantityRoomController.dispose();
    _capacityRoomController.dispose();
    super.dispose();
  }

  Widget _buildInfoWidget() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: Text(
              "Thông tin phòng",
              style: Const.styleH2,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: Text(
              "Loại phòng".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          RadioListTile<RoomType>(
            title: const Text('Ký túc xá/Home stay'),
            value: RoomType.dorm,
            groupValue: _roomType,
            onChanged: (RoomType value) {
              setState(() {
                _roomType = value;
              });
            },
          ),
          RadioListTile<RoomType>(
            title: const Text('Phòng cho thuê'),
            value: RoomType.rent,
            groupValue: _roomType,
            onChanged: (RoomType value) {
              setState(() {
                _roomType = value;
              });
            },
          ),
          RadioListTile<RoomType>(
            title: const Text('Phòng ở ghép'),
            value: RoomType.shared,
            groupValue: _roomType,
            onChanged: (RoomType value) {
              setState(() {
                _roomType = value;
              });
            },
          ),
          RadioListTile<RoomType>(
            title: const Text('Nhà nguyên căn'),
            value: RoomType.whole,
            groupValue: _roomType,
            onChanged: (RoomType value) {
              setState(() {
                _roomType = value;
              });
            },
          ),
          RadioListTile<RoomType>(
            title: const Text('Căn hộ'),
            value: RoomType.apartment,
            groupValue: _roomType,
            onChanged: (RoomType value) {
              setState(() {
                _roomType = value;
              });
            },
          ),
          Container(
            margin: EdgeInsets.only(top: Const.MARGIN),
            child: Text(
              "Số lượng phòng".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: TextFormField(
              controller: _quantityRoomController,
              decoration: InputDecoration(
                hintText: "Nhập số lượng phòng",
                hintStyle: Const.styleHint,
                suffixText: "phòng",
              ),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Vui lòng nhập số lượng phòng';
                }
                return null;
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: Const.MARGIN),
            child: Text(
              "Sức chứa".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: TextFormField(
              controller: _capacityRoomController,
              decoration: InputDecoration(
                hintText: "Nhập sức chứa",
                hintStyle: Const.styleHint,
                suffixText: "người/phòng",
              ),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Vui lòng nhập sức chứa';
                }
                return null;
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: Const.MARGIN, bottom: Const.MARGIN),
            child: Text(
              "Giới tính".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          RadioListTile<Gender>(
            title: const Text('Tất cả'),
            value: Gender.all,
            groupValue: _gender,
            onChanged: (Gender value) {
              setState(() {
                _gender = value;
              });
            },
          ),
          RadioListTile<Gender>(
            title: const Text('Nam'),
            value: Gender.male,
            groupValue: _gender,
            onChanged: (Gender value) {
              setState(() {
                _gender = value;
              });
            },
          ),
          RadioListTile<Gender>(
            title: const Text('Nữ'),
            value: Gender.female,
            groupValue: _gender,
            onChanged: (Gender value) {
              setState(() {
                _gender = value;
              });
            },
          ),
          Container(
            margin: EdgeInsets.only(top: Const.MARGIN),
            child: Text(
              "Diện tích".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: TextFormField(
              controller: _areaRoomController,
              decoration: InputDecoration(
                hintText: "Nhập diện tích",
                hintStyle: Const.styleHint,
                suffixText: "mét vuông",
              ),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Vui lòng nhập diện tích';
                }
                return null;
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPriceWidget() {
    return Container(
      margin: EdgeInsets.only(top: Const.MARGIN_LARGE),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text(
              "Chi phí",
              style: Const.styleH2,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: Const.MARGIN),
            child: Text(
              "Giá cho thuê".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: TextFormField(
              controller: _priceRoomController,
              decoration: InputDecoration(
                hintText: "Nhập giá cho thuê",
                hintStyle: Const.styleHint,
                suffixText: "VND/căn(người)",
              ),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Vui lòng nhập giá cho thuê';
                }
                return null;
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: Const.MARGIN),
            child: Text(
              "Đặt cọc".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: TextFormField(
              controller: _depositRoomController,
              decoration: InputDecoration(
                hintText: "Nhập số tiền (hoặc số tháng)",
                hintStyle: Const.styleHint,
                suffixText: "VND(tháng)",
              ),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Vui lòng nhập khoản cọc';
                }
                return null;
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: Const.MARGIN),
            child: Text(
              "Tiền điện".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: Row(
              children: [
                Expanded(
                  child: TextFormField(
                    controller: _electricPriceController,
                    enabled: !_isFreeElectric,
                    decoration: InputDecoration(
                      hintText: "Nhập số tiền",
                      hintStyle: Const.styleHint,
                      suffixText: "VND",
                    ),
                    keyboardType: TextInputType.number,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Vui lòng nhập số tiền';
                      }
                      return null;
                    },
                  ),
                ),
                Checkbox(
                  value: _isFreeElectric,
                  onChanged: (value) {
                    setState(() {
                      _isFreeElectric = value;
                      _electricPriceController.text = _isFreeElectric ? "Miễn phí" : "";
                    });
                  },
                ),
                Text(
                  "Miễn phí".toUpperCase(),
                  style: Const.styleH3,
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: Const.MARGIN),
            child: Text(
              "Tiền nước".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: Row(
              children: [
                Expanded(
                  child: TextFormField(
                    controller: _waterPriceController,
                    enabled: !_isFreeWater,
                    decoration: InputDecoration(
                      hintText: "Nhập số tiền",
                      hintStyle: Const.styleHint,
                      suffixText: "VND",
                    ),
                    keyboardType: TextInputType.number,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Vui lòng nhập ';
                      }
                      return null;
                    },
                  ),
                ),
                Checkbox(
                  value: _isFreeWater,
                  onChanged: (value) {
                    setState(() {
                      _isFreeWater = value;
                      _waterPriceController.text = _isFreeWater ? "Miễn phí" : "";
                    });
                  },
                ),
                Text(
                  "Miễn phí".toUpperCase(),
                  style: Const.styleH3,
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: Const.MARGIN),
            child: Text(
              "Internet/Truyền hình cáp".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: Row(
              children: [
                Expanded(
                  child: TextFormField(
                    controller: _internetPriceController,
                    enabled: !_isFreeInternet,
                    decoration: InputDecoration(
                      hintText: "Nhập số tiền",
                      hintStyle: Const.styleHint,
                      suffixText: "VND",
                    ),
                    keyboardType: TextInputType.number,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Vui lòng nhập số tiền';
                      }
                      return null;
                    },
                  ),
                ),
                Checkbox(
                  value: _isFreeInternet,
                  onChanged: (value) {
                    setState(() {
                      _isFreeInternet = value;
                      _internetPriceController.text = _isFreeInternet ? "Miễn phí" : "";
                    });
                  },
                ),
                Text(
                  "Miễn phí".toUpperCase(),
                  style: Const.styleH3,
                )
              ],
            ),
          ),
          Row(
            children: [
              SizedBox(
                width: 20,
                height: 20,
                child: Checkbox(
                  value: _isHaveParking,
                  onChanged: (value) {
                    setState(() {
                      _isHaveParking = value;
                    });
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10),
                child: Text(
                  "Có chỗ để xe".toUpperCase(),
                  style: Const.styleH3,
                ),
              )
            ],
          ),
          if (_isHaveParking)
            Container(
              margin: EdgeInsets.only(top: Const.MARGIN),
              child: Text(
                "Phí giữ xe".toUpperCase(),
                style: Const.styleH3,
              ),
            ),
          if (_isHaveParking)
            Container(
              margin: EdgeInsets.only(bottom: Const.MARGIN),
              child: Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      controller: _parkingPriceController,
                      enabled: !_isFreeParking,
                      decoration: InputDecoration(
                        hintText: "Nhập số tiền",
                        hintStyle: Const.styleHint,
                        suffixText: "VND/tháng",
                      ),
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Vui lòng nhập số tiền';
                        }
                        return null;
                      },
                    ),
                  ),
                  Checkbox(
                    value: _isFreeParking,
                    onChanged: (value) {
                      setState(() {
                        _isFreeParking = value;
                        _parkingPriceController.text = _isFreeParking ? "Miễn phí" : "";
                      });
                    },
                  ),
                  Text(
                    "Miễn phí".toUpperCase(),
                    style: Const.styleH3,
                  )
                ],
              ),
            )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
