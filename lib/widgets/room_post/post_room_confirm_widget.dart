import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:trolanha/models/room_model.dart';
import 'package:trolanha/models/user_model.dart';

import '../../Const.dart';
import '../../shared_preferences_helper.dart';

class PostRoomConfirmWidget extends StatefulWidget {

  PostRoomConfirmWidget({Key key, this.onPost, this.initialRoom}) : super(key: key);
  final VoidCallback onPost;
  final RoomModel initialRoom;

  @override
  State<StatefulWidget> createState() => PostRoomConfirmState();
}

class PostRoomConfirmState extends State<PostRoomConfirmWidget> with AutomaticKeepAliveClientMixin {
  final _formKey = GlobalKey<FormState>();
  final _phoneController = TextEditingController();
  final _titleController = TextEditingController();
  final _descriptionController = TextEditingController();

  RoomModel getRoom() {
    RoomModel roomModel = new RoomModel();
    roomModel.title = _titleController.text;
    roomModel.description = _descriptionController.text;
    return roomModel;
  }

  String getPhoneNumber() {
    return _phoneController.text;
  }

  @override
  void initState() {
    super.initState();
    getUserCached();
    if (widget.initialRoom != null) {
      _titleController.text = widget.initialRoom.title;
      _descriptionController.text = widget.initialRoom.description;
    }
  }

  void getUserCached() async {
    UserModel user = UserModel.fromMap(
        jsonDecode(await PreferencesHelper.getString(Const.KEY_CACHED_USER)),
        '');
    _phoneController.text = user.phoneNumber;
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Const.MARGIN),
      child: Form(
        key: _formKey,
        child: ListView(
          children: [
            _buildConfirmWidget(),
            RaisedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  widget.onPost();
                }
              },
              color: Colors.blueAccent,
              textColor: Colors.white,
              child: Text("Đăng tin".toUpperCase(),
                  style: TextStyle(fontSize: 15)),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildConfirmWidget() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: Text(
              "Xác nhận",
              style: Const.styleH2,
            ),
          ),
          Container(
            child: Text(
              "Số điện thoại".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: TextFormField(
              controller: _phoneController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: "Nhập số điện thoại",
                hintStyle: Const.styleHint,
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Vui lòng nhập số điện thoại';
                }
                return null;
              },
            ),
          ),
          Container(
            child: Text(
              "Tiêu đề bài đăng".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: TextFormField(
              controller: _titleController,
              decoration: InputDecoration(
                hintText: "Nhập tiêu đề",
                hintStyle: Const.styleHint,
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Vui lòng hập tiêu đề';
                }
                return null;
              },
            ),
          ),
          Container(
            child: Text(
              "Nội dung mô tả".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: TextFormField(
              controller: _descriptionController,
              decoration: InputDecoration(
                hintText: "Nnhập mô tả",
                hintStyle: Const.styleHint,
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Vui lòng nhập mô tả';
                }
                return null;
              },
            ),
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
