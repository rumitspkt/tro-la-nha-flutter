import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:trolanha/customs/dialogs.dart';
import 'package:trolanha/models/address_model.dart';
import 'package:trolanha/models/room_model.dart';
import 'package:trolanha/models/simple_model.dart';
import 'package:trolanha/repository.dart';

import '../../Const.dart';

class PostRoomAddressWidget extends StatefulWidget {
  PostRoomAddressWidget({Key key, this.onContinue, this.initialRoom}) : super(key: key);
  final VoidCallback onContinue;
  final RoomModel initialRoom;

  @override
  State<StatefulWidget> createState() => PostRoomAddressState();
}

class PostRoomAddressState extends State<PostRoomAddressWidget>
    with AutomaticKeepAliveClientMixin {
  final _formKey = GlobalKey<FormState>();
  final _cityController = TextEditingController();
  final _districtController = TextEditingController();
  final _addressDescriptionController = TextEditingController();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  SimpleModel selectedCity, selectedDistrict;

  LatLng latLng;

  final _places =
      new GoogleMapsPlaces(apiKey: "AIzaSyBB-sESaK33kjh5mPFHIVYWmAaNL3PfbkA");

  AddressModel getAddress() {
    AddressModel address = AddressModel();
    address.city = selectedCity;
    address.district = selectedDistrict;
    address.addressDescription = _addressDescriptionController.text;
    return address;
  }

  LatLng getLatLng() {
    return latLng;
  }

  @override
  void initState() {
    super.initState();
    if (widget.initialRoom != null) {
      RoomModel room = widget.initialRoom;
      setState(() {
        _cityController.text = room.address.city.displayText;
        _districtController.text = room.address.district.displayText;
        _addressDescriptionController.text = room.address.addressDescription;
        selectedCity = room.address.city;
        selectedDistrict = room.address.district;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Const.MARGIN),
      child: Form(
        key: _formKey,
        child: ListView(
          children: [
            _buildAddressWidget(),
            FlatButton(
              child: Text(
                "Tiếp theo",
                style: TextStyle(color: Colors.blueAccent, fontSize: 18),
              ),
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  Dialogs.showLoadingDialog(context, _keyLoader);
                  String searchText = '${_addressDescriptionController.text}, ${selectedDistrict.displayText}, ${selectedCity.displayText}';
                  print('search text: ' + searchText);
                  PlacesSearchResponse placeResponse = await _places.searchByText(
                      searchText,
                      language: 'vi');
                  Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                  if (placeResponse.results.length > 0) {
                    PlacesSearchResult firstResult = placeResponse.results[0];
                    latLng = LatLng(firstResult.geometry.location.lat, firstResult.geometry.location.lng);
                    print('search text ok: ${latLng.latitude}');
                    widget.onContinue();
                  } else {
                    Scaffold.of(context).showSnackBar(const SnackBar(
                      content: Text('Địa chỉ bạn đã nhập không tồn tại, vui lòng kiểm tra lại'),
                    ));
                  }
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAddressWidget() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: Text(
              "Địa chỉ",
              style: Const.styleH2,
            ),
          ),
          Container(
            child: Text(
              "Thành phố".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: TextFormField(
              onTap: () {
                _askCity();
              },
              readOnly: true,
              controller: _cityController,
              decoration: InputDecoration(
                hintText: "Bấm để chọn thành phố",
                hintStyle: Const.styleHint,
                suffixIcon: Icon(Icons.keyboard_arrow_down),
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Vui lòng chọn thành phố';
                }
                return null;
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: Const.MARGIN),
            child: Text(
              "Quận/Huyện".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: TextFormField(
              onTap: () {
                _askDistrict();
              },
              readOnly: true,
              controller: _districtController,
              decoration: InputDecoration(
                hintText: "Bấm để chọn Quận/Huyện",
                hintStyle: Const.styleHint,
                suffixIcon: Icon(Icons.keyboard_arrow_down),
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Vui lòng chọn Quận/Huyện';
                }
                return null;
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: Const.MARGIN),
            child: Text(
              "Số nhà, tên đường, phường/xã".toUpperCase(),
              style: Const.styleH3,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN),
            child: TextFormField(
              controller: _addressDescriptionController,
              decoration: InputDecoration(
                hintText: "Ví dụ: 453/3 Nguyễn Kiệm, Phường 9",
                hintStyle: Const.styleHint,
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Vui lòng nhập địa chỉ';
                }
                return null;
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _askCity() async {
    SimpleModel result = await showDialog<SimpleModel>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Chọn thành phố'),
            children: Repository.shared.cities
                .map((e) => SimpleDialogOption(
                      child: _buildOptionWidget(e.displayText),
                      onPressed: () {
                        Navigator.pop(context, e);
                      },
                    ))
                .toList(),
          );
        });
    if (result != null) {
      _cityController.text = result.displayText;
      selectedCity = result;
      _districtController.text = "";
      selectedDistrict = null;
    }
  }

  Future<void> _askDistrict() async {
    if (selectedCity == null) return;
    SimpleModel result = await showDialog<SimpleModel>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Chọn Quận/Huyện'),
            children: Repository.shared
                .districts(selectedCity.id)
                .map((e) => SimpleDialogOption(
                      child: _buildOptionWidget(e.displayText),
                      onPressed: () {
                        Navigator.pop(context, e);
                      },
                    ))
                .toList(),
          );
        });
    if (result != null) {
      _districtController.text = result.displayText;
      selectedDistrict = result;
    }
  }

  Widget _buildOptionWidget(String title) {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 5),
      child: Text(
        title,
        style: TextStyle(
          color: Colors.black87,
          fontSize: 15,
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
