import 'dart:io' show Platform;
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:lottie/lottie.dart' hide Marker;
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:trolanha/customs/marker_generator.dart';
import 'package:trolanha/enums.dart';
import 'package:trolanha/models/room_model.dart';
import 'package:trolanha/models/simple_model.dart';
import 'package:trolanha/repository.dart';
import 'package:trolanha/widgets/place/place_search_widget.dart';
import 'package:trolanha/widgets/room_detail/room_detail_widget.dart';

import '../Const.dart';
import '../helper.dart';

class HomeMapWidget extends StatefulWidget {
  const HomeMapWidget({Key key}) : super(key: key);

  @override
  HomeMapState createState() => HomeMapState();
}

class HomeMapState extends State<HomeMapWidget> with TickerProviderStateMixin {
  GoogleMapController _controller;
  Map<MarkerId, Marker> _markers = <MarkerId, Marker>{};
  MarkerId _selectedMarker;
  Position _cachedCurrentPosition;
  AnimationController _pinMarkerController;
  List<RoomModel> _rooms = [];
  List<RoomModel> _nearRooms = [];
  LatLng centerPoint;

  SimpleModel selectedCity;

  RemoteConfig remoteConfig;
  bool enableFeatureSearchByPlace = false;

  void _onMapCreated(GoogleMapController controller) {
    _controller = controller;
  }

  @override
  void initState() {
    super.initState();
    _pinMarkerController = AnimationController(vsync: this);
    Geolocator().getCurrentPosition().then((value) {
      _cachedCurrentPosition = value;
      _controller.animateCamera(CameraUpdate.newLatLngZoom(
        LatLng(value.latitude, value.longitude),
        16,
      ));
    });
    initRemoteConfig();
  }

  void initRemoteConfig() async {
    await Future.delayed(Duration(seconds: 3));
    remoteConfig = await RemoteConfig.instance;
    await remoteConfig.fetch(expiration: const Duration(seconds: 3));
    await remoteConfig.activateFetched();
    setState(() {
      enableFeatureSearchByPlace =
          remoteConfig.getBool("enableFeatureSearchByPlace");
    });
  }


  void fetchAllRooms() async {
    selectedCity = Repository.shared.selectedCity;
    QuerySnapshot query = await Firestore.instance
        .collection("rooms")
        .where('isVerified', isEqualTo: true)
        .where('isApproved', isEqualTo: true)
        .where('addresses', arrayContains: selectedCity.id)
        .getDocuments();
    List<RoomModel> rooms = [];
    for (int i = 0; i < query.documents.length; i++) {
      DocumentSnapshot doc = query.documents[i];
      RoomModel room = RoomModel.fromMap(doc.data, doc.documentID);
      rooms.add(room);
    }
    this._rooms = rooms;
    MarkerGenerator(getListRoomPriceWidget(), (bitmaps) {
      drawBitmapsMarker(bitmaps);
    }).generate(context);
  }

  void drawBitmapsMarker(List<Uint8List> bitmaps) {
    bitmaps.asMap().forEach((index, bitmap) {
      final String markerIdVal = _rooms[index].uuid;
      final MarkerId markerId = MarkerId(markerIdVal);
      final Marker marker = Marker(
        onTap: () {
          Future.delayed(Duration(seconds: 1), () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => RoomDetailWidget(
                room: _rooms[index],
              ),
            ));
          });
        },
        icon: BitmapDescriptor.fromBytes(bitmap),
        position: _rooms[index].latLng,
        markerId: markerId,
      );
      _markers[markerId] = marker;
    });
    setState(() {});
  }

  List<Widget> getListRoomPriceWidget() {
    List<Widget> widgets = [];
    _rooms.asMap().forEach((index, room) {
      widgets.add(buildMarkerPriceWidget(room.price));
    });
    return widgets;
  }

  Widget buildMarkerPriceWidget(int price) {
    double height = Platform.isIOS ? 34 : 24;
    return Container(
      alignment: Alignment.center,
      width: Platform.isIOS ? 80 : 60,
      height: height,
      decoration: BoxDecoration(
          color: Colors.blueAccent,
          borderRadius: BorderRadius.all(Radius.circular(height / 2))),
      padding: EdgeInsets.only(left: 5, right: 5),
      child: Text(
        formatSimplePrice(price),
        style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: Platform.isIOS ? 17 : 11),
      ),
    );
  }

  String formatSimplePrice(int price) {
    if (price < 1000000) {
      return (price ~/ 1000).toString() + "K ₫";
    } else {
      return (price / 1000000).toStringAsFixed(1) + "M ₫";
    }
  }

  @override
  void dispose() {
    _pinMarkerController.dispose();
    super.dispose();
  }

  void _onMarkerTapped(MarkerId markerId) {
    final Marker tappedMarker = _markers[markerId];
    if (tappedMarker != null) {
      setState(() {
        if (_markers.containsKey(_selectedMarker)) {
          final Marker resetOld = _markers[_selectedMarker]
              .copyWith(iconParam: BitmapDescriptor.defaultMarker);
          _markers[_selectedMarker] = resetOld;
        }
        _selectedMarker = markerId;
        final Marker newMarker = tappedMarker.copyWith(
          iconParam: BitmapDescriptor.defaultMarkerWithHue(
            BitmapDescriptor.hueGreen,
          ),
        );
        _markers[markerId] = newMarker;
      });
    }
  }

  static final CameraPosition _kHCMUTE = CameraPosition(
    target: LatLng(10.850705, 106.771004),
    zoom: 15,
  );

  final List<String> imgList = [
    'https://d3hne3c382ip58.cloudfront.net/files/uploads/bookmundi/resized/cms/places-to-visit-in-vietnam-hoi-an-1579079158-735X412.jpg',
    'https://www.planetware.com/photos-large/VIE/vietnam-hue-imperial-palace-gate.jpg',
    'https://blog.myproguide.com/wp-content/uploads/2019/07/nha-trang-vietnam.jpg',
    'https://www.roughguides.com/wp-content/uploads/2017/01/3-Ha-Long-BayCN0AG9-1578x1050.jpg',
    'https://media.vietravel.net/Images/NewsPicture/hoi-an_7.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        GoogleMap(
          mapType: MapType.terrain,
          padding: EdgeInsets.only(bottom: 60, top: 40),
          zoomControlsEnabled: false,
          myLocationButtonEnabled: false,
          myLocationEnabled: true,
          initialCameraPosition: _kHCMUTE,
          onMapCreated: _onMapCreated,
          onCameraMove: (CameraPosition position) {
            centerPoint = position.target;
          },
          onCameraIdle: () {
            _pinMarkerController.duration =
                Duration(seconds: 1, milliseconds: 1000);
            _pinMarkerController.forward();
          },
          onCameraMoveStarted: () {
            _pinMarkerController.reset();
            _pinMarkerController.stop();
          },
          markers: Set<Marker>.of(_markers.values),
        ),
        Positioned(
          top: 0,
          bottom: 55,
          child: IgnorePointer(
            child: Lottie.asset(
              'assets/pin_marker.json',
              width: 120,
              height: 120,
              controller: _pinMarkerController,
              repeat: false,
            ),
          ),
        ),
        if (enableFeatureSearchByPlace) Positioned(
          top: 50,
          child: FloatingActionButton.extended(
            heroTag: "search",
            isExtended: true,
            backgroundColor: Colors.white,
            onPressed: () async {
              LatLng selectedPosition =
                  await Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => PlaceSearchWidget(
                  currentPosition: _cachedCurrentPosition,
                ),
              ));
              if (selectedPosition != null) {
                _controller.animateCamera(
                  CameraUpdate.newLatLngZoom(
                    selectedPosition,
                    16,
                  ),
                );
              }
            },
            label: Text(
              "Tìm kiếm địa điểm",
              style: TextStyle(color: Colors.black87, fontSize: 12),
            ),
            icon: Icon(
              Icons.location_on,
              color: Colors.black87,
            ),
          ),
        ),
        Positioned(
          bottom: 100,
          left: Const.MARGIN,
          child: FloatingActionButton(
            heroTag: "locate",
            child: Icon(
              Icons.my_location,
              color: Colors.black87,
            ),
            isExtended: true,
            backgroundColor: Colors.white,
            onPressed: () async {
              if (await Permission.location.isDenied) {
                [
                  Permission.location,
                  Permission.locationAlways,
                  Permission.locationWhenInUse
                ].request();
              } else if (await Permission.location.isPermanentlyDenied) {
                _showRequestLocationPermissionDialog();
              } else {
                Geolocator().getCurrentPosition().then((value) {
                  _cachedCurrentPosition = value;
                });
                if (_cachedCurrentPosition != null) {
                  _controller.animateCamera(
                    CameraUpdate.newLatLngZoom(
                      LatLng(_cachedCurrentPosition.latitude,
                          _cachedCurrentPosition.longitude),
                      16,
                    ),
                  );
                }
              }
            },
          ),
        ),
        Positioned(
          bottom: 170,
          left: Const.MARGIN,
          child: FloatingActionButton(
            heroTag: "list",
            child: Icon(
              Icons.format_list_bulleted,
              color: Colors.black87,
            ),
            isExtended: true,
            backgroundColor: Colors.white,
            onPressed: () {
              showNearRooms();
            },
          ),
        )
      ],
    );
  }

  void showNearRooms() async {
    _nearRooms.clear();

    for (int i = 0; i < _rooms.length; i++) {
      LatLng roomPoint = _rooms[i].latLng;
      if (roomPoint != null &&
          roomPoint.latitude != 0 &&
          roomPoint.longitude != 0) {
        double distance = Helper.calculateDistance(centerPoint, roomPoint);
        _rooms[i].distance = distance;
        _nearRooms.add(_rooms[i]);
      }
    }

    _nearRooms.sort((a, b) => a.distance.compareTo(b.distance));

    showBarModalBottomSheet(
      expand: false,
      barrierColor: Colors.black38,
      context: context,
      builder: (context) => Container(
        child: Material(
          child: Column(
            children: [
              Container(
                alignment: Alignment.center,
                height: 50,
                child: Text(
                  "Phòng gần đây",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                  ),
                ),
              ),
              if (false)
                Container(
                  height: 200,
                  alignment: Alignment.center,
                  child: Text("Không có phòng nào gần vị trí bạn đã chọn"),
                ),
              Expanded(
                child: ListView(
                  children: _nearRooms.map((e) => _buildPostItem(e)).toList(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildPostItem(RoomModel room) {
    double height = 165;

    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => RoomDetailWidget(
                    room: room,
                  )),
        );
      },
      child: Container(
        height: height,
        margin: EdgeInsets.only(
            left: Const.MARGIN,
            right: Const.MARGIN,
            bottom: Const.MARGIN,
            top: Const.MARGIN),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(width: 1, color: Colors.grey[200]),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  child: Image.network(
                    room.images[0],
                    fit: BoxFit.cover,
                    width: 120,
                    height: height,
                  ),
                ),
                Container(
                  width: 120,
                  height: height,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10)),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment(0, 0.1),
                        colors: [
                          Colors.black.withAlpha(120),
                          Colors.transparent
                        ],
                      )),
                ),
                Positioned(
                    top: 5,
                    left: 5,
                    child: Icon(
                      Icons.security,
                      color: Color(Const.primaryColor),
                      size: 18,
                    )),
                Positioned(
                    top: 28,
                    left: 5,
                    child: Icon(
                      Icons.monetization_on,
                      color: Color(Const.secondaryColor),
                      size: 18,
                    ))
              ],
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 10, right: 5),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                        bottomRight: Radius.circular(10))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Row(
                        children: [
                          Text(
                            "${Helper.getDisplayTextRoomType(room.type).toUpperCase()} . ",
                            style: TextStyle(
                              color: Colors.grey[400],
                              fontSize: 12,
                            ),
                          ),
                          Text(
                            "${room.capacity} ${Helper.getDisplayTextUnicodeGender(room.gender)}",
                            style: TextStyle(
                              color: Colors.grey[400],
                              fontSize: 12,
                            ),
                          ),
                          Expanded(
                            child: Container(),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
                      child: Text(
                        "${room.title}",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w600),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Text(
                        "${(room.price / 1000000).formattedValue} M₫/${Helper.getUnitTextRoomType(room.type)}",
                        style: TextStyle(
                          color: Colors.blueAccent,
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                      child: Text(
                        "${room.address.district != null ? room.address.district.displayText : ''}",
                        style: TextStyle(
                          color: Colors.grey[600],
                          fontSize: 13,
                        ),
                        maxLines: 2,
                      ),
                    ),
                    Expanded(
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              child: Row(
                                children: [
                                  Container(
                                    child: Text(
                                      "Cách",
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 12),
                                    ),
                                    margin: EdgeInsets.only(right: 5),
                                  ),
                                  Container(
                                    child: Text(
                                        room.distance < 1
                                            ? "${(room.distance * 1000).toInt()}m"
                                            : "${room.distance.formattedValue}km",
                                        style: TextStyle(
                                            color: Colors.blueAccent,
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold)),
                                  )
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            child: Container(
                              height: 30,
                              padding: EdgeInsets.only(left: 10, right: 15),
                              decoration: BoxDecoration(
                                  color: Colors.blueAccent,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.location_on,
                                    size: 18,
                                    color: Colors.white,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 3),
                                    child: Text(
                                      "Định vị",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              Navigator.of(context).pop();
                              _controller.animateCamera(CameraUpdate.newLatLngZoom(
                                room.latLng,
                                16,
                              ));
                              Future.delayed(Duration(seconds: 2), () {
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => RoomDetailWidget(
                                    room: room,
                                  ),
                                ));
                              });
                            },
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> _showRequestLocationPermissionDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Yêu cầu bật định vị'),
          content: Text(
              'Ứng dụng cần quyền truy cập vị trí để tìm phòng dễ dàng hơn'),
          actions: <Widget>[
            FlatButton(
              child: Text('Quay lại'.toUpperCase()),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text(
                'Đồng ý'.toUpperCase(),
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              onPressed: () {
                openAppSettings();
              },
            )
          ],
        );
      },
    );
  }
}
