import 'package:flutter/material.dart';

class HomeMessageWidget extends StatefulWidget {
  @override
  _HomeMessageState createState() => _HomeMessageState();
}

class _HomeMessageState extends State<HomeMessageWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Home Message',
          ),
        ],
      ),
    );
  }
}