import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class ListImageOverlay extends ModalRoute<void> {
  List<String> images;
  CarouselController carouselController = CarouselController();
  int currentPage = 0;

  ListImageOverlay({this.images});

  @override
  Duration get transitionDuration => Duration(milliseconds: 200);

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => false;

  @override
  Color get barrierColor => Colors.black.withOpacity(0.95);

  @override
  String get barrierLabel => null;

  @override
  bool get maintainState => true;

  @override
  Widget buildPage(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
  ) {
    // This makes sure that text and other content follows the material style
    return Material(
      type: MaterialType.transparency,
      // make sure that the overlay content is not cut off
      child: SafeArea(
        child: _buildOverlayContent(context),
      ),
    );
  }

  Widget _buildOverlayContent(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return Stack(
      alignment: Alignment.center,
      children: [
        CarouselSlider(
          carouselController: carouselController,
          options: CarouselOptions(
            height: height,
            viewportFraction: 1.0,
            enlargeCenterPage: true,
            enableInfiniteScroll: false,
            onPageChanged: (index, reason) {
              setState(() {
                currentPage = index;
                changedExternalState();
              });
            },
          ),
          items: images
              .map(
                (item) => Container(
                  child: Center(
                    child: Image.network(
                      item,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                ),
              )
              .toList(),
        ),
        Positioned(
          top: 20,
          left: 20,
          child: IconButton(
              color: Colors.white,
              iconSize: 30,
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop();
              }),
        ),
        Positioned(
          left: 30,
          bottom: 20,
          child: Text(
            "${currentPage + 1}/${images.length}",
            style: TextStyle(
              color: Colors.white,
              fontSize: 15
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    // You can add your own animations for the overlay content
    return FadeTransition(
      opacity: animation,
      child: ScaleTransition(
        scale: animation,
        child: child,
      ),
    );
  }
}
