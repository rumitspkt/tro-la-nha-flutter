import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:trolanha/customs/dialogs.dart';
import 'package:trolanha/enums.dart';
import 'package:trolanha/models/invite_model.dart';
import 'package:trolanha/models/room_model.dart';
import 'package:trolanha/models/user_model.dart';
import 'package:trolanha/widgets/room_detail/room_detail_list_image_overlay.dart';
import 'package:trolanha/widgets/room_list/room_list_widget.dart';
import 'package:trolanha/widgets/room_post/post_room_widget.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../Const.dart';
import '../../helper.dart';
import '../../shared_preferences_helper.dart';

class RoomUtilityEntry {
  const RoomUtilityEntry(this.name, this.key, this.icon);

  final String name;
  final RoomUtility key;
  final IconData icon;
}

class RoomDetailWidget extends StatefulWidget {
  final RoomModel room;

  const RoomDetailWidget({Key key, this.room}) : super(key: key);

  @override
  State<StatefulWidget> createState() => RoomDetailState();
}

class RoomDetailState extends State<RoomDetailWidget>
    with AutomaticKeepAliveClientMixin {
  RoomModel room;
  List<RoomModel> ownerRooms;
  List<RoomModel> suggestRooms = [];
  UserModel user;
  bool isMe = false;
  final CollectionReference _users = Firestore.instance.collection("users");
  final GlobalKey<State> _loadingKey = new GlobalKey<State>();

  @override
  void initState() {
    super.initState();
    room = widget.room;
    print(room.address.district.id);
    getUserInfo(room.user);
    fetchOwnerListRoom();
    fetchSuggestListRoom();
  }

  void getUserInfo(String userId) async {
    FirebaseUser fbUser = await FirebaseAuth.instance.currentUser();
    DocumentSnapshot docSnapshot = await _users.document(userId).get();
    if (docSnapshot.exists) {
      UserModel user =
          UserModel.fromMap(docSnapshot.data, docSnapshot.documentID);
      setState(() {
        room.owner = user;
        this.user = user;
        isMe = fbUser != null && fbUser.uid == user.id;
      });
    }
  }

  void fetchOwnerListRoom() async {
    QuerySnapshot query = await Firestore.instance
        .collection("rooms")
        .where('user', isEqualTo: room.user)
        .orderBy('lastUpdatedAt', descending: false)
        .getDocuments();
    List<RoomModel> rooms = [];
    for (int i = 0; i < query.documents.length; i++) {
      DocumentSnapshot doc = query.documents[i];
      RoomModel room = RoomModel.fromMap(doc.data, doc.documentID);
      rooms.add(room);
    }
    setState(() {
      ownerRooms = rooms;
    });
  }

  Future<void> fetchSuggestListRoom() async {
    QuerySnapshot query = await Firestore.instance
        .collection("rooms")
        .where('isVerified', isEqualTo: true)
        .where('isApproved', isEqualTo: true)
        .where('price', isGreaterThanOrEqualTo: room.price - 1000000)
        .where('price', isLessThanOrEqualTo: room.price + 1000000)
        .where('addresses', arrayContains: room.address.district.id)
        .limit(8)
        .getDocuments();
    List<RoomModel> rooms = [];
    for (int i = 0; i < query.documents.length; i++) {
      DocumentSnapshot doc = query.documents[i];
      if (doc.documentID != room.uuid) {
        RoomModel room = RoomModel.fromMap(doc.data, doc.documentID);
        rooms.add(room);
      }
    }
    setState(() {
      this.suggestRooms = rooms;
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Chi tiết phòng",
          style: TextStyle(fontSize: 17),
        ),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.share),
            onPressed: () {},
          )
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              cacheExtent: 9999,
              children: [
                _buildImageWidget(),
                _buildMainInfoWidget(),
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  height: 10,
                  decoration: BoxDecoration(color: Colors.grey[200]),
                ),
                _buildUtilityWidget(),
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  height: 10,
                  decoration: BoxDecoration(color: Colors.grey[200]),
                ),
                _buildAddressWidget(),
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  height: 10,
                  decoration: BoxDecoration(color: Colors.grey[200]),
                ),
                _buildDateInfoWidget(),
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  height: 10,
                  decoration: BoxDecoration(color: Colors.grey[200]),
                ),
                _buildDescriptionWidget(),
                if (user != null)
                  Container(
                    height: 10,
                    decoration: BoxDecoration(color: Colors.grey[200]),
                  ),
                if (user != null) _buildUserInfoWidget(),
                if (!isMe)
                  Container(
                    height: 10,
                    decoration: BoxDecoration(color: Colors.grey[200]),
                  ),
                if (!isMe) _buildReportWidget(),
                if (!isMe)
                  Container(
                    height: 10,
                    decoration: BoxDecoration(color: Colors.grey[200]),
                  ),
                if (!isMe && suggestRooms.length > 0) _buildListPostWidget(),
              ],
            ),
          ),
          Container(
            height: 3,
            decoration: BoxDecoration(
                color: Colors.grey,
                gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [Colors.grey.withAlpha(30), Colors.transparent],
                )),
          ),
          if (!isMe)
            Container(
              padding:
                  EdgeInsets.symmetric(vertical: 15, horizontal: Const.MARGIN),
              child: Row(
                children: [
                  OutlineButton.icon(
                    onPressed: () async {
                      if (user != null &&
                          user.facebook != null &&
                          user.facebook.isNotEmpty) {
                        print(user.facebook);
                        if (await canLaunch(user.facebook)) {
                          await launch(user.facebook);
                        }
                      }
                    },
                    padding: EdgeInsets.all(10),
                    borderSide: BorderSide(
                      color: Colors.blueAccent,
                      style: BorderStyle.solid,
                    ),
                    icon: Image.asset(
                      "assets/ic_facebook_blue.png",
                      width: 20,
                      height: 20,
                    ),
                    label: Text(
                      "Kết nối",
                      style: TextStyle(color: Colors.blueAccent, fontSize: 13),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: RaisedButton(
                        padding: EdgeInsets.all(15),
                        onPressed: () async {
                          Dialogs.showLoadingDialog(context, _loadingKey);

                          FirebaseUser fbUser =
                              await FirebaseAuth.instance.currentUser();

                          UserModel userCached = UserModel.fromMap(
                              jsonDecode(await PreferencesHelper.getString(
                                  Const.KEY_CACHED_USER)),
                              "");

                          var invites =
                              Firestore.instance.collection("invites");

                          InviteModel invite = InviteModel(
                            isSeen: false,
                            lastUpdatedAt: Timestamp.now(),
                            message: "Tôi muốn xem phòng",
                            room: room.uuid,
                            roomTitleSnippet: room.title,
                            owner: room.user,
                            user: fbUser.uid,
                            userNameSnippet:
                                '${userCached.firstName} ${userCached.lastName}',
                            userPhoneSnippet: userCached.phoneNumber,
                          );

                          await invites
                              .document(room.uuid + fbUser.uid)
                              .setData(invite.toJson());
                          Navigator.of(_loadingKey.currentContext,
                                  rootNavigator: true)
                              .pop();
                        },
                        color: Colors.blueAccent,
                        child: Text(
                          "Đặt hẹn ngay",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ),
                  ),
                  OutlineButton.icon(
                    onPressed: () async {
                      print(user.phoneNumber);
                      if (user != null &&
                          user.phoneNumber != null &&
                          user.phoneNumber.isNotEmpty) {
//                        FlutterPhoneState.startPhoneCall(user.phoneNumber);
                        if (await canLaunch('tel:${user.phoneNumber}')) {
                          await launch('tel:${user.phoneNumber}');
                        }
                      }
                    },
                    padding: EdgeInsets.all(10),
                    borderSide: BorderSide(
                      color: Colors.blueAccent,
                      style: BorderStyle.solid,
                    ),
                    icon: Icon(
                      Icons.phone,
                      color: Colors.blueAccent,
                    ),
                    label: Text(
                      "Gọi",
                      style: TextStyle(color: Colors.blueAccent),
                    ),
                  )
                ],
              ),
            ),
          if (isMe)
            Container(
              padding:
                  EdgeInsets.symmetric(vertical: 15, horizontal: Const.MARGIN),
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: Const.MARGIN),
                        child: Text(
                          "Trạng thái:",
                          style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 5, right: 8),
                        height: 30,
                        decoration: BoxDecoration(
                            color: room.isVerified
                                ? (room.isApproved ? Colors.green : Colors.red)
                                : Colors.amber,
                            borderRadius:
                                BorderRadius.all(Radius.circular(12))),
                        child: Row(
                          children: [
                            Icon(
                              room.isVerified
                                  ? (room.isApproved
                                      ? Icons.check
                                      : Icons.close)
                                  : Icons.timelapse,
                              color: Colors.white,
                              size: 18,
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 5),
                              child: Text(
                                room.isVerified
                                    ? (room.isApproved
                                        ? "Đã duyệt"
                                        : "Bị từ chối")
                                    : "Đang chờ duyệt",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.delete_forever,
                          color: Colors.blueAccent,
                        ),
                        iconSize: 30,
                        onPressed: () async {
                          Dialogs.showLoadingDialog(context, _loadingKey);
                          await Firestore.instance
                              .collection("rooms")
                              .document(room.uuid)
                              .delete();
                          Navigator.of(_loadingKey.currentContext).pop();
                          Navigator.of(context).pop();
                        },
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.edit,
                          color: Colors.blueAccent,
                        ),
                        iconSize: 30,
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PostRoomWidget(
                                      initialRoom: room,
                                    )),
                          );
                        },
                      ),
                    ],
                  ),
                  if (room.isVerified && room.isApproved)
                    Container(
                      child: FlatButton(
                        child: Text(
                          "Tự bỏ duyệt".toUpperCase(),
                          style: TextStyle(
                              color: Colors.red, fontWeight: FontWeight.bold),
                        ),
                        onPressed: () async {
                          Dialogs.showLoadingDialog(context, _loadingKey);
                          await Firestore.instance
                              .collection("rooms")
                              .document(room.uuid)
                              .updateData({
                            'isApproved': false,
                            'reason': 'Tự bỏ duyệt'
                          });
                          Navigator.of(_loadingKey.currentContext).pop();
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  if (room.isVerified && !room.isApproved)
                    Container(
                      margin: EdgeInsets.only(top: Const.MARGIN),
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: Const.MARGIN),
                            child: Text(
                              "Lý do:",
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 5, right: 8),
                            child: Text(
                              room.reason == null ? "" : room.reason,
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                ],
              ),
            )
        ],
      ),
    );
  }

  Widget _buildImageWidget() {
    double marginImage = 2;
    double ratio = 2 / 3;
    double width = MediaQuery.of(context).size.width;
    double itemWidth = (width - marginImage * 3) / 2;
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(ListImageOverlay(images: room.images));
      },
      child: Container(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(bottom: marginImage, top: marginImage),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Image.network(
                    room.images[0],
                    fit: BoxFit.cover,
                    width: itemWidth,
                    height: itemWidth,
                  ),
                  Image.network(
                    room.images[1],
                    fit: BoxFit.cover,
                    width: itemWidth,
                    height: itemWidth,
                  )
                ],
              ),
            ),
            if (room.images.length == 4)
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.network(
                      room.images[2],
                      fit: BoxFit.cover,
                      width: itemWidth,
                      height: itemWidth * ratio,
                    ),
                    Image.network(
                      room.images[3],
                      fit: BoxFit.cover,
                      width: itemWidth,
                      height: itemWidth * ratio,
                    )
                  ],
                ),
              )
            else if (room.images.length > 4)
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.network(
                      room.images[2],
                      fit: BoxFit.cover,
                      width: itemWidth * ratio,
                      height: itemWidth * ratio,
                    ),
                    Image.network(
                      room.images[3],
                      fit: BoxFit.cover,
                      width: itemWidth * ratio,
                      height: itemWidth * ratio,
                    ),
                    Stack(
                      alignment: Alignment.center,
                      children: [
                        Image.network(
                          room.images[4],
                          fit: BoxFit.cover,
                          width: itemWidth * ratio,
                          height: itemWidth * ratio,
                        ),
                        if (room.images.length > 5)
                          Container(
                            width: itemWidth * ratio,
                            height: itemWidth * ratio,
                            decoration: BoxDecoration(
                                color: Colors.black.withAlpha(150)),
                          ),
                        if (room.images.length > 5)
                          Text(
                            "${room.images.length - 5}+",
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          )
                      ],
                    )
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }

  Widget _buildMainInfoWidget() {
    return Container(
      padding: EdgeInsets.all(Const.MARGIN),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Text(
              "${Helper.getDisplayTextRoomType(room.type).toUpperCase()} . ${room.capacity} ${Helper.getDisplayTextUnicodeGender(room.gender)}",
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 15,
                  fontWeight: FontWeight.w300),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Text(
              room.title,
              maxLines: 3,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.w300),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Center(
              child: Text(
                "GIÁ: ${room.price / 1000000} triệu VNĐ/${Helper.getUnitTextRoomType(room.type)}",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.blueAccent,
                    fontSize: 18,
                    fontWeight: FontWeight.w600),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: Text(
                        "Diện tích".toUpperCase(),
                        style: Const.styleH3,
                      ),
                    ),
                    Text(
                      "${room.area} m2",
                      style: TextStyle(
                          color: Colors.blueAccent,
                          fontWeight: FontWeight.w600),
                    )
                  ],
                ),
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: Text(
                        "Đặt cọc".toUpperCase(),
                        style: Const.styleH3,
                      ),
                    ),
                    Text(
                        "${room.deposit < 10 ? "${room.deposit} tháng" : "${room.deposit / 1000000} triệu"}",
                        style: TextStyle(
                            color: Colors.blueAccent,
                            fontWeight: FontWeight.w600))
                  ],
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 20),
            height: 1,
            decoration: BoxDecoration(color: Colors.grey[300]),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: Icon(
                        Icons.lightbulb_outline,
                        color: Colors.grey[400],
                      ),
                    ),
                    Text(
                      "${room.priceElectricPerUnit == 0 ? "Miễn phí" : "${room.priceElectricPerUnit / 1000}k"}",
                      style: TextStyle(
                          color: Colors.blueAccent,
                          fontSize: 12,
                          fontWeight: FontWeight.w600),
                    )
                  ],
                ),
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: Icon(
                        Icons.free_breakfast,
                        color: Colors.grey[400],
                      ),
                    ),
                    Text(
                      "${room.priceWaterPerUnit == 0 ? "Miễn phí" : "${room.priceWaterPerUnit / 1000}k"}",
                      style: TextStyle(
                          color: Colors.blueAccent,
                          fontSize: 12,
                          fontWeight: FontWeight.w600),
                    )
                  ],
                ),
                if (room.priceParking != null && room.priceParking >= 0)
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 5),
                        child: Icon(
                          Icons.directions_car,
                          color: Colors.grey[400],
                        ),
                      ),
                      Text(
                        "${room.priceParking == 0 ? "Miễn phí" : "${room.priceParking / 1000}k"}",
                        style: TextStyle(
                            color: Colors.blueAccent,
                            fontSize: 12,
                            fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: Icon(
                        Icons.wifi,
                        color: Colors.grey[400],
                      ),
                    ),
                    Text(
                      "${room.priceInternet == 0 ? "Miễn phí" : "${room.priceInternet / 1000}k"}",
                      style: TextStyle(
                          color: Colors.blueAccent,
                          fontSize: 12,
                          fontWeight: FontWeight.w600),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildUtilityWidget() {
    return Container(
      padding: EdgeInsets.all(Const.MARGIN),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Text(
              "Tiện ích",
              style: Const.styleH2,
            ),
          ),
          Wrap(
            children: actorWidgets.toList(),
          )
        ],
      ),
    );
  }

  Iterable<Widget> get actorWidgets sync* {
    for (final RoomUtility utility in room.utilities) {
      RoomUtilityEntry entry = getUtilityEntry(utility);
      yield Container(
        margin: EdgeInsets.only(right: Const.MARGIN),
        child: FilterChip(
          selectedColor: Colors.blueAccent,
          backgroundColor: Colors.grey[200],
          padding: EdgeInsets.all(5),
          avatar: CircleAvatar(
              child: Icon(
            entry.icon,
            size: 12,
          )),
          label: Text(entry.name),
          onSelected: (value) {},
        ),
      );
    }
  }

  RoomUtilityEntry getUtilityEntry(RoomUtility utility) {
    switch (utility) {
      case RoomUtility.pet:
        return RoomUtilityEntry('Thú cưng', RoomUtility.pet, Icons.pets);
      case RoomUtility.tivi:
        return RoomUtilityEntry('Tivi', RoomUtility.tivi, Icons.tv);
      case RoomUtility.closet:
        return RoomUtilityEntry('Tủ đồ', RoomUtility.closet, Icons.inbox);
      case RoomUtility.bed:
        return RoomUtilityEntry('Giường', RoomUtility.bed, Icons.hotel);
      case RoomUtility.mezz:
        return RoomUtilityEntry(
            'Gác lửng', RoomUtility.mezz, Icons.trending_up);
      case RoomUtility.wash:
        return RoomUtilityEntry(
            'Máy giặt', RoomUtility.wash, Icons.crop_portrait);
      case RoomUtility.frid:
        return RoomUtilityEntry('Tủ lạnh', RoomUtility.frid, Icons.ac_unit);
      case RoomUtility.kit:
        return RoomUtilityEntry('Nhà bếp', RoomUtility.kit, Icons.kitchen);
      case RoomUtility.heat:
        return RoomUtilityEntry(
            'Máy nước nóng', RoomUtility.heat, Icons.whatshot);
      case RoomUtility.wc:
        return RoomUtilityEntry('WC riêng', RoomUtility.wc, Icons.wc);
      case RoomUtility.park:
        return RoomUtilityEntry(
            'Chỗ để xe', RoomUtility.park, Icons.local_parking);
      case RoomUtility.secure:
        return RoomUtilityEntry('An ninh', RoomUtility.secure, Icons.lock);
      case RoomUtility.window:
        return RoomUtilityEntry(
            'Cửa sổ', RoomUtility.window, Icons.crop_square);
      case RoomUtility.wifi:
        return RoomUtilityEntry('Wifi', RoomUtility.wifi, Icons.wifi);
      case RoomUtility.free:
        return RoomUtilityEntry('Tự do', RoomUtility.free, Icons.motorcycle);
      case RoomUtility.privacy:
        return RoomUtilityEntry(
            'Chủ riêng', RoomUtility.privacy, Icons.account_circle);
    }
  }

  Widget _buildAddressWidget() {
    return Container(
      padding: EdgeInsets.all(Const.MARGIN),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Text(
              "Địa chỉ",
              style: Const.styleH2,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              children: [
                Icon(
                  Icons.pin_drop,
                  size: 25,
                  color: Colors.grey,
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      "${room.address.addressDescription}, ${room.address.district.displayText}, ${room.address.city.displayText}",
                      maxLines: 3,
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              children: [
                Icon(
                  Icons.phone,
                  size: 25,
                  color: Colors.grey,
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      "Số điện thoại: ${user == null || user.phoneNumber == null ? "" : user.phoneNumber}",
                      maxLines: 1,
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildDateInfoWidget() {
    DateTime dateTime = room.lastUpdatedAt.toDate();
    var newFormat = DateFormat("dd/MM/yyyy");
    String updatedDt = newFormat.format(dateTime);
    return Container(
      padding: EdgeInsets.all(Const.MARGIN),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Text(
              "Ngày đăng",
              style: Const.styleH2,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              children: [
                Icon(
                  Icons.calendar_today,
                  size: 25,
                  color: Colors.grey,
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      "${Helper.isToday(dateTime) ? "Hôm nay:" : Helper.isYesterday(dateTime) ? "Hôm qua:" : "Ngày:"} $updatedDt",
                      maxLines: 3,
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDescriptionWidget() {
    return Container(
      padding: EdgeInsets.all(Const.MARGIN),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Text(
              "Mô tả",
              style: Const.styleH2,
            ),
          ),
          Container(
            child: Text(
              room.description,
              style: TextStyle(
                fontSize: 14,
                color: Colors.grey,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildUserInfoWidget() {
    return InkWell(
      onTap: () {
        if (ownerRooms.length > 0) {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => RoomListWidget(
                      type: 'owner',
                      sameRoom: room,
                      ownerRooms: ownerRooms,
                    )),
          );
        }
      },
      child: Container(
        padding: EdgeInsets.all(Const.MARGIN),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              child: Image.network(
                user.avatar,
                fit: BoxFit.cover,
                width: 50,
                height: 50,
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: Const.MARGIN),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${user.firstName} ${user.lastName}",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          color: Colors.black87),
                    ),
                    Text(
                      "${ownerRooms == null ? 1 : ownerRooms.length} phòng",
                      style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          color: Colors.blueAccent),
                    )
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(right: 10),
              child: Icon(
                Icons.arrow_forward_ios,
                size: 20,
                color: Colors.grey,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildReportWidget() {
    return InkWell(
      onTap: () {},
      child: Container(
        padding: EdgeInsets.all(Const.MARGIN),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.report,
              size: 25,
              color: Colors.redAccent,
            ),
            Container(
              margin: EdgeInsets.only(left: 10),
              child: Text(
                "Báo cáo vi phạm",
                style: TextStyle(
                  color: Colors.redAccent,
                  fontSize: 18,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListPostWidget() {
    return Column(
      children: [
        Container(
          margin:
              EdgeInsets.fromLTRB(Const.MARGIN, Const.MARGIN, 0, Const.MARGIN),
          child: Row(
            children: [
              Container(
                child: Text(
                  "Gợi ý",
                  style: TextStyle(fontSize: 18, color: Colors.black54),
                ),
                margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(Const.MARGIN, 0, Const.MARGIN, 0),
          child: _buildListPost(),
        ),
        Container(
          height: 1,
          margin:
              EdgeInsets.fromLTRB(Const.MARGIN, Const.MARGIN, Const.MARGIN, 0),
          decoration: BoxDecoration(color: Colors.grey[300]),
        ),
        FlatButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => RoomListWidget(
                    type: 'same_room',
                    sameRoom: room,
                  )),
            );
          },
          child: Text(
            "Xem tất cả",
            style: TextStyle(
                color: Colors.blueGrey,
                fontSize: 15,
                fontWeight: FontWeight.w900),
          ),
        ),
        Container(
          height: 20,
          decoration: BoxDecoration(color: Colors.grey[200]),
        ),
      ],
    );
  }

  GridView _buildListPost() {
    var size = MediaQuery.of(context).size;
    return GridView.count(
      primary: false,
      shrinkWrap: true,
      crossAxisSpacing: Const.MARGIN,
      mainAxisSpacing: Const.MARGIN,
      childAspectRatio: ((size.width - 3 * Const.MARGIN) / 2) / 260,
      crossAxisCount: 2,
      children: suggestRooms.map((room) => _buildPostItem(room)).toList(),
    );
  }

  Widget _buildPostItem(RoomModel room) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => RoomDetailWidget(
                    room: room,
                  )),
        );
      },
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  child: Image.network(
                    room.images[0],
                    fit: BoxFit.cover,
                    width: 1000,
                    height: 110,
                  ),
                ),
                Container(
                  width: 1000,
                  height: 110,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment(0, 0.1),
                        colors: [
                          Colors.black.withAlpha(120),
                          Colors.transparent
                        ],
                      )),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: Row(
                children: [
                  Text(
                    "${Helper.getDisplayTextRoomType(room.type).toUpperCase()} . ",
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 12,
                    ),
                  ),
                  Text(
                      "${room.capacity} ${Helper.getDisplayTextUnicodeGender(room.gender)}",
                      style: TextStyle(
                        color: Colors.grey[400],
                        fontSize: 12,
                      )),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
              child: Text(
                room.title,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.w600),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.start,
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
              child: Text(
                "${(room.price / 1000000).formattedValue} triệu VNĐ/${Helper.getUnitTextRoomType(room.type)}",
                style: TextStyle(
                  color: Colors.blueAccent,
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.justify,
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
              child: Text(
                "${room.address.addressDescription}",
                style: TextStyle(
                  color: Colors.grey[600],
                  fontSize: 13,
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.start,
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
                child: Text(
                  "${room.address.district != null ? room.address.district.displayText : ''}",
                  style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 13,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
