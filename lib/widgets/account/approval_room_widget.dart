import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:trolanha/customs/dialogs.dart';
import 'package:trolanha/enums.dart';
import 'package:trolanha/models/room_model.dart';
import 'package:trolanha/widgets/room_detail/room_detail_widget.dart';

import '../../Const.dart';
import '../../helper.dart';

class ApprovalRoomWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ApprovalRoomState();
}

class ApprovalRoomState extends State<ApprovalRoomWidget> {
  List<RoomModel> listRoom = [];
  final CollectionReference _rooms = Firestore.instance.collection("rooms");
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      fetchHomeListGoodPriceRoom();
    });
  }

  void fetchHomeListGoodPriceRoom() async {
    Dialogs.showLoadingDialog(context, _keyLoader);
    QuerySnapshot query = await _rooms
        .where('isVerified', isEqualTo: false)
        .orderBy('lastUpdatedAt', descending: false)
        .getDocuments();
    List<RoomModel> rooms = [];
    for (int i = 0; i < query.documents.length; i++) {
      DocumentSnapshot doc = query.documents[i];
      RoomModel room = RoomModel.fromMap(doc.data, doc.documentID);
      rooms.add(room);
    }
    setState(() {
      listRoom = rooms;
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Duyệt phòng",
        ),
        centerTitle: true,
      ),
      body: ListView(
        children: getListRoomWidget(),
      ),
    );
  }

  List<Widget> getListRoomWidget() {
    List<Widget> list = [];
    for (int i = 0; i < listRoom.length; i++) {
      list.add(_buildPostItem(i, listRoom[i]));
    }
    return list;
  }

  Widget _buildPostItem(int index, RoomModel room) {
    double height = 150;

    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => RoomDetailWidget(
                    room: room,
                  )),
        );
      },
      child: Container(
        height: height,
        margin: EdgeInsets.only(
            left: Const.MARGIN,
            right: Const.MARGIN,
            bottom: Const.MARGIN,
            top: Const.MARGIN),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(width: 1, color: Colors.grey[200]),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  child: Image.network(
                    room.images[0],
                    fit: BoxFit.cover,
                    width: 120,
                    height: height,
                  ),
                ),
                Container(
                  width: 120,
                  height: height,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10)),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment(0, 0.1),
                        colors: [
                          Colors.black.withAlpha(120),
                          Colors.transparent
                        ],
                      )),
                ),
              ],
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 10, right: 5),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                        bottomRight: Radius.circular(10))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Row(
                        children: [
                          Text(
                            "${Helper.getDisplayTextRoomType(room.type).toUpperCase()} . ",
                            style: TextStyle(
                              color: Colors.grey[400],
                              fontSize: 12,
                            ),
                          ),
                          Text(
                            "${room.capacity} ${Helper.getDisplayTextUnicodeGender(room.gender)}",
                            style: TextStyle(
                              color: Colors.grey[400],
                              fontSize: 12,
                            ),
                          ),
                          Expanded(
                            child: Container(),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
                      child: Text(
                        "${room.title}",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w600),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
                      child: Text(
                        "${(room.price / 1000000).formattedValue} M₫/${Helper.getUnitTextRoomType(room.type)}",
                        style: TextStyle(
                          color: Colors.blueAccent,
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 6, 0, 10),
                      child: Text(
                        "${room.address.district != null ? room.address.district.displayText : ''}",
                        style: TextStyle(
                          color: Colors.grey[600],
                          fontSize: 13,
                        ),
                        maxLines: 2,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            InkWell(
                              child: Container(
                                padding: EdgeInsets.only(left: 5, right: 8),
                                height: 30,
                                decoration: BoxDecoration(
                                    color: Colors.green,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(12))),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.check,
                                      color: Colors.white,
                                      size: 18,
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 5),
                                      child: Text(
                                        "Duyệt",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              onTap: () async {
                                Dialogs.showLoadingDialog(context, _keyLoader);
                                try {
                                  await _rooms.document(room.uuid).updateData({
                                    'isVerified': true,
                                    'isApproved': true,
                                  });
                                  setState(() {
                                    listRoom.removeAt(index);
                                  });
                                } catch (error) {} finally {
                                  Navigator.pop(_keyLoader.currentContext);
                                }
                              },
                            ),
                            InkWell(
                              child: Container(
                                padding: EdgeInsets.only(left: 5, right: 8),
                                height: 30,
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(12))),
                                child: Row(
                                  children: [
                                    Icon(
                                      Icons.close,
                                      color: Colors.white,
                                      size: 18,
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(left: 5),
                                      child: Text(
                                        "Từ chối",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              onTap: () async {
                                Widget _buildOptionWidget(String title) {
                                  return Container(
                                    padding: EdgeInsets.only(top: 5, bottom: 5),
                                    child: Text(
                                      title,
                                      style: TextStyle(
                                        color: Colors.black87,
                                        fontSize: 15,
                                      ),
                                    ),
                                  );
                                }

                                String reason = await showDialog<String>(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return SimpleDialog(
                                        title: const Text('Chọn lý do'),
                                        children: <Widget>[
                                          SimpleDialogOption(
                                            onPressed: () {
                                              Navigator.pop(context,
                                                  "Hình ảnh không phù hợp");
                                            },
                                            child: _buildOptionWidget(
                                                'Hình ảnh không phù hợp'),
                                          ),
                                          SimpleDialogOption(
                                            onPressed: () {
                                              Navigator.pop(context,
                                                  "Nộp dung chưa đầy đủ");
                                            },
                                            child: _buildOptionWidget(
                                                'Nộp dung chưa đầy đủ'),
                                          ),
                                          SimpleDialogOption(
                                            onPressed: () {
                                              Navigator.pop(
                                                  context, "Giá cả bất hợp lý");
                                            },
                                            child: _buildOptionWidget(
                                                'Giá cả bất hợp lý'),
                                          ),
                                        ],
                                      );
                                    });

                                if (reason == null) return;

                                Dialogs.showLoadingDialog(context, _keyLoader);
                                try {
                                  await _rooms.document(room.uuid).updateData({
                                    'isVerified': true,
                                    'isApproved': false,
                                    'reason': reason
                                  });
                                  setState(() {
                                    listRoom.removeAt(index);
                                  });
                                } catch (error) {} finally {
                                  Navigator.pop(_keyLoader.currentContext);
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
