import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:trolanha/customs/dialogs.dart';
import 'package:trolanha/models/user_model.dart';
import 'package:uuid/uuid.dart';

import '../../Const.dart';
import '../../enums.dart';
import '../../helper.dart';
import '../../shared_preferences_helper.dart';

class AccountDetailWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AccountDetailState();
}

class AccountDetailState extends State<AccountDetailWidget> {
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _linkController = TextEditingController();
  Gender _filterGenderType = Gender.male;
  DateTime _selectedDate = DateTime.now();
  String _selectedAvatar = "";
  UserModel cachedUser;
  String selectedJob = "";

  final GlobalKey<State> _loadingKey = new GlobalKey<State>();
  final GlobalKey<State> _scaffoldKey = new GlobalKey<State>();

  final picker = ImagePicker();

  Future getImage(ImageSource imageSource) async {
    final pickedFile = await picker.getImage(source: imageSource);
    setState(() {
      _selectedAvatar = pickedFile.path;
    });
  }

  Future<void> _askedPickAvatar() async {
    switch (await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Chọn'),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, "camera");
                },
                child: const Text('Chụp ảnh mới'),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, "gallery");
                },
                child: const Text('Chọn từ thư viện'),
              ),
            ],
          );
        })) {
      case "camera":
        getImage(ImageSource.camera);
        break;
      case "gallery":
        getImage(ImageSource.gallery);
        break;
    }
  }

  @override
  void initState() {
    super.initState();
    getAccountInfo();
  }

  void getAccountInfo() async {
    String cachedUser =
        await PreferencesHelper.getString(Const.KEY_CACHED_USER);
    if (cachedUser.isEmpty) return;
    UserModel user = UserModel.fromMap(jsonDecode(cachedUser), '');
    this.cachedUser = user;
    setState(() {
      _selectedAvatar = user.avatar;
      if (user.birthDateInMillis != null) {
        _selectedDate =
            DateTime.fromMillisecondsSinceEpoch(user.birthDateInMillis);
      }
      if (user.gender != null) {
        _filterGenderType = EnumToString.fromString(Gender.values, user.gender);
      }
      _firstNameController.text = user.firstName == null ? '' : user.firstName;
      _lastNameController.text = user.lastName == null ? '' : user.lastName;
      _phoneController.text = user.phoneNumber == null ? '' : user.phoneNumber;
      _linkController.text = user.facebook == null ? '' : user.facebook;
      selectedJob = user.job;
    });
  }

  Image getImageFromUrl(String url) {
    if (url.contains("http")) {
      return Image.network(
        url,
        fit: BoxFit.cover,
        width: 92,
        height: 92,
      );
    } else {
      return Image.file(
        File(url),
        fit: BoxFit.cover,
        width: 92,
        height: 92,
      );
    }
  }

  void updateAccountInfo() async {
    Dialogs.showLoadingDialog(context, _loadingKey);
    cachedUser.firstName = _firstNameController.text;
    cachedUser.lastName = _lastNameController.text;
    cachedUser.phoneNumber = _phoneController.text;
    cachedUser.facebook = _linkController.text;
    cachedUser.gender = _filterGenderType.enumValue;
    cachedUser.birthDate = Timestamp.fromDate(_selectedDate);
    cachedUser.job = selectedJob;

    FirebaseUser firebaseUser = await FirebaseAuth.instance.currentUser();

    if (_selectedAvatar.contains("http")) {
    } else {
      StorageReference ref = FirebaseStorage.instance
          .ref()
          .child("images")
          .child("${firebaseUser.uid}/${Uuid().v1()}");
      StorageUploadTask uploadTask = ref.putFile(File(_selectedAvatar));
      await uploadTask.onComplete;
      String avatar = await ref.getDownloadURL();
      cachedUser.avatar = avatar;
    }

    await Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .updateData(cachedUser.toMap());

    cachedUser.birthDateInMillis = cachedUser.birthDate.millisecondsSinceEpoch;
    cachedUser.birthDate = null;
    await PreferencesHelper.setString(
        Const.KEY_CACHED_USER, jsonEncode(cachedUser.toMap()));

    Navigator.of(_loadingKey.currentContext).pop();
    Navigator.of(context).pop(cachedUser);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        heroTag: "update_profile",
        onPressed: updateAccountInfo,
        label: Text("Cập nhật hồ sơ"),
        backgroundColor: Colors.blueAccent,
        icon: Icon(Icons.account_circle),
      ),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.only(left: 4, top: 10),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: Colors.blueAccent,
            ),
            child: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
                size: 25,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: Const.MARGIN * 2),
            child: Stack(
              alignment: Alignment.topCenter,
              children: [
                Positioned(
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
                Positioned(
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 4),
                        borderRadius: BorderRadius.all(Radius.circular(50))),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(46)),
                          child:
                              _selectedAvatar == null || _selectedAvatar.isEmpty
                                  ? Image.network(
                                      "https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png",
                                      fit: BoxFit.cover,
                                      width: 92,
                                      height: 92,
                                    )
                                  : getImageFromUrl(_selectedAvatar),
                        ),
                        Container(
                          width: 92,
                          height: 92,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.bottomCenter,
                              end: Alignment(0, 0.2),
                              colors: [Colors.black, Colors.transparent],
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(46)),
                          ),
                        ),
                        Positioned(
                          bottom: -5,
                          child: Container(
                            alignment: Alignment.center,
                            child: IconButton(
                              icon: Icon(
                                Icons.edit,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                _askedPickAvatar();
                              },
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                child: Container(
                  height: 50,
                  padding:
                      EdgeInsets.only(left: Const.MARGIN, right: Const.MARGIN),
                  child: TextFormField(
                    controller: _firstNameController,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.blueAccent, width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      ),
                      labelStyle: TextStyle(color: Colors.grey, fontSize: 15),
                      border: InputBorder.none,
                      labelText: "Tên",
                      hintText: "Nhập tên",
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Vui lòng nhập tên';
                      }
                      return null;
                    },
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 50,
                  padding:
                      EdgeInsets.only(left: Const.MARGIN, right: Const.MARGIN),
                  child: TextFormField(
                    controller: _lastNameController,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.blueAccent, width: 1.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      ),
                      labelStyle: TextStyle(color: Colors.grey, fontSize: 15),
                      border: InputBorder.none,
                      labelText: "Họ",
                      hintText: "Nhập họ",
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Vui lòng nhập họ';
                      }
                      return null;
                    },
                  ),
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(
                top: Const.MARGIN * 2, left: Const.MARGIN, right: Const.MARGIN),
            height: 50,
            child: Row(
              children: [
                Container(
                  padding: EdgeInsets.only(right: 20),
                  child: Text(
                    "Giới tính",
                    style: TextStyle(fontSize: 13, color: Colors.grey),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        _buildGenderItemWidget(Gender.male),
                        _buildGenderItemWidget(Gender.female),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
                top: Const.MARGIN * 2, left: Const.MARGIN, right: Const.MARGIN),
            height: 50,
            child: Row(
              children: [
                Container(
                  padding: EdgeInsets.only(right: 20),
                  child: Text(
                    "Ngày sinh",
                    style: TextStyle(fontSize: 13, color: Colors.grey),
                  ),
                ),
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      DateTime pickedDate = await showDatePicker(
                        context: context,
                        initialDate: _selectedDate,
                        firstDate: DateTime(1900),
                        lastDate: DateTime.now(),
                      );
                      if (pickedDate == null) return;
                      setState(() {
                        _selectedDate = pickedDate;
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.only(left: Const.MARGIN * 2),
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      alignment: Alignment.center,
                      height: 40,
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Icon(
                            Icons.date_range,
                            size: 20,
                          ),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(left: Const.MARGIN),
                              child: Text(
                                DateFormat("dd-MM-yyyy").format(_selectedDate),
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
                top: Const.MARGIN * 2, left: Const.MARGIN, right: Const.MARGIN),
            height: 50,
            child: Row(
              children: [
                Container(
                  padding: EdgeInsets.only(right: 20),
                  child: Text(
                    "Nghề nghiệp",
                    style: TextStyle(fontSize: 13, color: Colors.grey),
                  ),
                ),
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      _askForJob();
                    },
                    child: Container(
                      padding: EdgeInsets.only(left: Const.MARGIN * 2),
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      alignment: Alignment.center,
                      height: 40,
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Icon(
                            Icons.nature_people,
                            size: 20,
                          ),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(left: Const.MARGIN),
                              child: Text(
                                selectedJob == null || selectedJob.isEmpty
                                    ? "Bấm vào để chọn nghề nghiệp"
                                    : Helper.getJobDisplayText(selectedJob),
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 50,
            margin: EdgeInsets.only(top: Const.MARGIN_LARGE),
            padding: EdgeInsets.only(left: Const.MARGIN, right: Const.MARGIN),
            child: TextFormField(
              controller: _phoneController,
              decoration: InputDecoration(
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueAccent, width: 1.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                ),
                labelStyle: TextStyle(color: Colors.grey, fontSize: 15),
                border: InputBorder.none,
                labelText: "Số điện thoại",
                hintText: "Nhập số điện thoại",
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Vui lòng nhập số điện thoại';
                }
                return null;
              },
            ),
          ),
          Container(
            height: 50,
            margin: EdgeInsets.only(top: Const.MARGIN_LARGE),
            padding: EdgeInsets.only(left: Const.MARGIN, right: Const.MARGIN),
            child: TextFormField(
              controller: _linkController,
              decoration: InputDecoration(
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueAccent, width: 1.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey, width: 1.0),
                ),
                labelStyle: TextStyle(color: Colors.grey, fontSize: 15),
                border: InputBorder.none,
                labelText: "Tài khoản liên kết",
                hintText: "Nhập đường dẫn tài khoản liên kết (Facebook,...)",
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Vui lòng nhập đường dẫn';
                }
                return null;
              },
            ),
          ),
          Container(
            height: 80,
          ),
        ],
      ),
    );
  }

  Widget _buildGenderItemWidget(Gender gender) {
    return Expanded(
      child: InkWell(
        onTap: () {
          setState(() {
            _filterGenderType = gender;
          });
        },
        child: Container(
          alignment: Alignment.center,
          height: 40,
          decoration: _filterGenderType == gender
              ? BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  border: Border.all(width: 1, color: Colors.blueAccent),
                )
              : BoxDecoration(),
          child: Text(
            getDisplayTextGender(gender),
            style: TextStyle(
                color: _filterGenderType == gender
                    ? Colors.blueAccent
                    : Colors.black),
          ),
        ),
      ),
    );
  }

  Widget _buildOptionWidget(String title) {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 5),
      child: Text(
        title,
        style: TextStyle(
          color: Colors.black87,
          fontSize: 15,
        ),
      ),
    );
  }

  Future<void> _askForJob() async {
    List<String> jobs = ['worker', 'officer', 'student', 'schoolboy', 'free'];
    String selectedJob = await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Liên hệ qua'),
            children: jobs
                .map((e) => SimpleDialogOption(
                      child: _buildOptionWidget(Helper.getJobDisplayText(e)),
                      onPressed: () async {
                        Navigator.pop(context, e);
                      },
                    ))
                .toList(),
          );
        });
    setState(() {
      this.selectedJob = selectedJob;
    });
  }

  String getDisplayTextGender(Gender gender) {
    switch (gender) {
      case Gender.all:
        return "Tất cả";
      case Gender.male:
        return "Nam";
      case Gender.female:
        return "Nữ";
    }
    return "";
  }
}
