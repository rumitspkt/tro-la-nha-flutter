import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:trolanha/customs/dialogs.dart';
import 'package:trolanha/models/invite_model.dart';
import 'package:trolanha/models/room_model.dart';
import 'package:trolanha/widgets/room_detail/room_detail_widget.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../Const.dart';

class AccountRoomScheduleWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AccountRoomScheduleState();
}

class AccountRoomScheduleState extends State<AccountRoomScheduleWidget>
    with AutomaticKeepAliveClientMixin {
  List<InviteModel> invites = [];

  final CollectionReference _invites = Firestore.instance.collection("invites");

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    fetchInviteList();
  }

  Future<void> fetchInviteList() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    QuerySnapshot query = await _invites
        .where('owner', isEqualTo: user.uid)
        .where('isSeen', isEqualTo: false)
        .orderBy('lastUpdatedAt', descending: true)
        .getDocuments();
    List<InviteModel> invites = [];
    for (int i = 0; i < query.documents.length; i++) {
      DocumentSnapshot doc = query.documents[i];
      InviteModel room = InviteModel.fromMap(doc.data, doc.documentID);
      invites.add(room);
    }
    setState(() {
      this.invites = invites;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: [
          SmartRefresher(
            onRefresh: () async {
              await fetchInviteList();
              _refreshController.refreshCompleted();
            },
            controller: _refreshController,
            child: ListView(
              children: getListInviteWidget(),
            ),
          ),
          Positioned(
            bottom: Const.MARGIN,
            child: FloatingActionButton.extended(
              heroTag: "check_all",
              onPressed: () async {
                Dialogs.showLoadingDialog(context, _keyLoader);
                for (int i = 0; i < invites.length; i++) {
                  InviteModel invite = invites[i];
                  if (!invite.isSeen) {
                    await _invites
                        .document(invite.room + invite.user)
                        .updateData({"isSeen": true});
                    invite.isSeen = true;
                  }
                }
                setState(() {});
                Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                    .pop();
              },
              label: Text("Đánh dấu tất cả là đã xem"),
              icon: Icon(Icons.check_circle),
            ),
          )
        ],
      ),
    );
  }

  List<Widget> getListInviteWidget() {
    List<Widget> list = [];
    for (int i = 0; i < invites.length; i++) {
      list.add(buildRequestWidget(invites[i], i));
    }
    list.add(
      Container(
        height: 60,
      ),
    );
    return list;
  }

  Widget buildRequestWidget(InviteModel invite, int index) {
    DateTime dateTime = invite.lastUpdatedAt.toDate();
    var newFormat = DateFormat("dd/MM/yyyy - HH:mm");
    String dateFormatted = newFormat.format(dateTime);
    return Container(
      margin: EdgeInsets.all(Const.MARGIN),
      padding: EdgeInsets.only(left: Const.MARGIN),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: Colors.grey[300])),
      child: Column(
        children: [
          Container(
            child: Row(
              children: [
                Container(
                  child: Text(
                    "Phòng: ",
                    style: Const.styleH3,
                  ),
                ),
                Expanded(
                  child: Container(
                    child: Text(
                      invite.roomTitleSnippet,
                      style: TextStyle(
                          color: Colors.black87, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                FlatButton(
                  onPressed: () async {
                    Dialogs.showLoadingDialog(context, _keyLoader);
                    DocumentSnapshot doc = await Firestore.instance
                        .collection("rooms")
                        .document(invite.room)
                        .get();
                    if (doc.exists) {
                      RoomModel room =
                          RoomModel.fromMap(doc.data, doc.documentID);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RoomDetailWidget(
                                  room: room,
                                )),
                      );
                    }
                    Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                        .pop();
                  },
                  child: Text(
                    "Chi tiết phòng",
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                )
              ],
            ),
          ),
          Container(
            child: Row(
              children: [
                Container(
                  child: Text(
                    "Người hẹn: ",
                    style: Const.styleH3,
                  ),
                ),
                Expanded(
                  child: Container(
                    child: Text(
                        "${invite.userNameSnippet} - ${invite.userPhoneSnippet}",
                        style: TextStyle(
                            color: Colors.black87,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
                FlatButton(
                  onPressed: () async {
                    if (invite.userPhoneSnippet != null &&
                        invite.userPhoneSnippet.isNotEmpty) {
                      if (await canLaunch('tel:${invite.userPhoneSnippet}')) {
                        await launch('tel:${invite.userPhoneSnippet}');
                      }
                    }
                  },
                  child: Text("Gọi ngay",
                      style: TextStyle(color: Colors.blueAccent)),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 5, bottom: 5),
            child: Row(
              children: [
                Container(
                  child: Text(
                    "Ngày hẹn: ",
                    style: Const.styleH3,
                  ),
                ),
                Container(
                  child: Text(dateFormatted,
                      style: TextStyle(
                          color: Colors.green,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
          Container(
            child: Row(
              children: [
                Container(
                  child: Text(
                    "Trạng thái: ",
                    style: Const.styleH3,
                  ),
                ),
                Container(
                  child: Switch.adaptive(
                      value: invite.isSeen,
                      onChanged: (value) {
                        setState(() {
                          invite.isSeen = value;
                        });
                        _invites
                            .document(invite.room + invite.user)
                            .updateData({"isSeen": value});
                      }),
                ),
                Container(
                  child: Text(
                    invite.isSeen ? "Đã xem" : "Chưa xem",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: invite.isSeen
                            ? Colors.blueAccent
                            : Colors.redAccent),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
