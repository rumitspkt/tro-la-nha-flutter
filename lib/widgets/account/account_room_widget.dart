import 'package:flutter/material.dart';
import 'package:trolanha/widgets/account/account_room_manage_widget.dart';
import 'package:trolanha/widgets/account/account_room_schedule_widget.dart';

class AccountRoomWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
              bottom: TabBar(
                tabs: [
                  Tab(
                    icon: Icon(Icons.schedule),
                    text: "Lịch hẹn",
                  ),
                  Tab(
                    icon: Icon(Icons.playlist_play),
                    text: "Phòng của tôi",
                  ),
                ],
              ),
              title: Text('Lịch hẹn - Phòng của tôi'),
              centerTitle: true,
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              )),
          body: TabBarView(
            children: [
              AccountRoomScheduleWidget(),
              AccountRoomManageWidget(),
            ],
          ),
        ),
      ),
    );
  }
}
