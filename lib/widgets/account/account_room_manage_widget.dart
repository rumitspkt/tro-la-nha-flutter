import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:trolanha/enums.dart';
import 'package:trolanha/models/room_model.dart';
import 'package:trolanha/widgets/room_detail/room_detail_widget.dart';
import 'package:trolanha/widgets/room_post/post_room_widget.dart';

import '../../Const.dart';
import '../../helper.dart';

class AccountRoomManageWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AccountRoomManageState();
}

class AccountRoomManageState extends State<AccountRoomManageWidget>
    with AutomaticKeepAliveClientMixin {
  List<RoomModel> listRoom = [];
  final CollectionReference _rooms = Firestore.instance.collection("rooms");
  RefreshController _refreshController = RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      fetchMyListRoom();
    });
  }

  Future<void> fetchMyListRoom() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    QuerySnapshot query = await _rooms
        .where('user', isEqualTo: user.uid)
        .orderBy('lastUpdatedAt', descending: false)
        .getDocuments();
    List<RoomModel> rooms = [];
    for (int i = 0; i < query.documents.length; i++) {
      DocumentSnapshot doc = query.documents[i];
      RoomModel room = RoomModel.fromMap(doc.data, doc.documentID);
      rooms.add(room);
    }
    setState(() {
      listRoom = rooms;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: [
          SmartRefresher(
            onRefresh: () async {
              await fetchMyListRoom();
              _refreshController.refreshCompleted();
            },
            controller: _refreshController,
            child: ListView(
              children: getListRoomWidget(),
            ),
          ),
          Positioned(
            bottom: Const.MARGIN,
            child: FloatingActionButton.extended(
              heroTag: "new_post",
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PostRoomWidget()),
                );
              },
              label: Text("Đăng bài mới"),
              icon: Icon(Icons.home),
            ),
          )
        ],
      ),
    );
  }

  List<Widget> getListRoomWidget() {
    List<Widget> list = [];
    for (int i = 0; i < listRoom.length; i++) {
      list.add(_buildPostItem(listRoom[i]));
    }
    list.add(
      Container(
        height: 60,
      ),
    );
    return list;
  }

  Widget _buildPostItem(RoomModel room) {
    double height = 185;

    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => RoomDetailWidget(
                    room: room,
                  )),
        );
      },
      child: Container(
        height: height,
        margin: EdgeInsets.only(
            left: Const.MARGIN,
            right: Const.MARGIN,
            bottom: Const.MARGIN,
            top: Const.MARGIN),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(width: 1, color: Colors.grey[200]),
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10)),
                  child: Image.network(
                    room.images[0],
                    fit: BoxFit.cover,
                    width: 120,
                    height: height,
                  ),
                ),
                Container(
                  width: 120,
                  height: height,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          bottomLeft: Radius.circular(10)),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment(0, 0.1),
                        colors: [
                          Colors.black.withAlpha(120),
                          Colors.transparent
                        ],
                      )),
                ),
                Positioned(
                    top: 5,
                    left: 5,
                    child: Icon(
                      Icons.security,
                      color: Color(Const.primaryColor),
                      size: 18,
                    )),
                Positioned(
                    top: 28,
                    left: 5,
                    child: Icon(
                      Icons.monetization_on,
                      color: Color(Const.secondaryColor),
                      size: 18,
                    ))
              ],
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 10, right: 5),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                        bottomRight: Radius.circular(10))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Row(
                        children: [
                          Text(
                            "${Helper.getDisplayTextRoomType(room.type).toUpperCase()} . ",
                            style: TextStyle(
                              color: Colors.grey[400],
                              fontSize: 12,
                            ),
                          ),
                          Text(
                            "${room.capacity} ${Helper.getDisplayTextUnicodeGender(room.gender)}",
                            style: TextStyle(
                              color: Colors.grey[400],
                              fontSize: 12,
                            ),
                          ),
                          Expanded(
                            child: Container(),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
                      child: Text(
                        "${room.title}",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.w600),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
                      child: Text(
                        "${(room.price / 1000000).formattedValue} M₫/${Helper.getUnitTextRoomType(room.type)}",
                        style: TextStyle(
                          color: Colors.blueAccent,
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 4, 0, 10),
                      child: Text(
                        "${room.address.district != null ? room.address.district.displayText : ''}",
                        style: TextStyle(
                          color: Colors.grey[600],
                          fontSize: 13,
                        ),
                        maxLines: 2,
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Container(
                            child: Text(
                              "Trạng thái: ",
                              style: Const.styleH3,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 5, right: 8),
                            height: 30,
                            decoration: BoxDecoration(
                                color: room.isVerified
                                    ? (room.isApproved
                                        ? Colors.green
                                        : Colors.red)
                                    : Colors.amber,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(12))),
                            child: Row(
                              children: [
                                Icon(
                                  room.isVerified
                                      ? (room.isApproved
                                          ? Icons.check
                                          : Icons.close)
                                      : Icons.timelapse,
                                  color: Colors.white,
                                  size: 18,
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 5),
                                  child: Text(
                                    room.isVerified
                                        ? (room.isApproved
                                            ? "Đã duyệt"
                                            : "Bị từ chối")
                                        : "Đang chờ duyệt",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    if (room.isVerified && !room.isApproved)
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.fromLTRB(0, 4, 0, 10),
                          child: Row(
                            children: [
                              Text(
                                "Lý do: ",
                                style: Const.styleH3,
                              ),
                              Expanded(
                                child: Text(
                                  room.reason == null ? '' : room.reason,
                                  style: TextStyle(fontSize: 12),
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
