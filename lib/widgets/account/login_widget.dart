import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:trolanha/customs/dialogs.dart';
import 'package:trolanha/models/user_model.dart';
import 'package:trolanha/shared_preferences_helper.dart';

import '../../Const.dart';

class LoginWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginState();
}

class LoginState extends State<LoginWidget> {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final CollectionReference _users = Firestore.instance.collection("users");

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            margin: EdgeInsets.only(right: 15, top: 10),
            alignment: Alignment.centerRight,
            child: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(
                Icons.close,
                color: Colors.black87,
                size: 35,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            alignment: Alignment.center,
            child: Text(
              "Đăng nhập",
              style: TextStyle(
                  color: Colors.black87,
                  fontSize: 35,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
                top: Const.MARGIN_LARGE,
                right: Const.MARGIN_LARGE,
                left: Const.MARGIN_LARGE),
            alignment: Alignment.center,
            child: Text(
              "Đăng nhập để có thể sử dụng các tính năng cộng đồng của Trọ Là Nhà như Đặt lịch hẹn, Kết nối chủ phòng, Tìm bạn ở ghép, Đăng tin cho thuê phòng, Đánh giá chất lượng,...",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontSize: 16,
                  fontWeight: FontWeight.w300),
            ),
          ),
          Expanded(
            child: Container(
              height: 320,
            ),
          ),
          Container(
            width: 1000,
            margin: EdgeInsets.only(
                left: Const.MARGIN_LARGE,
                right: Const.MARGIN_LARGE,
                bottom: Const.MARGIN),
            height: 45,
            child: GestureDetector(
              onTap: () async {
                final _phoneController = TextEditingController();
                String phoneNumber = await showDialog<String>(
                    context: context,
                    builder: (BuildContext context) {
                      return SimpleDialog(
                        title: const Text('Nhập số điện thoại'),
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(
                                left: Const.MARGIN + 10,
                                right: Const.MARGIN + 10,
                                bottom: Const.MARGIN),
                            child: TextFormField(
                              controller: _phoneController,
                              decoration: InputDecoration(
                                hintText: "Số điện thoại",
                                hintStyle: Const.styleHint,
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Vui lòng nhập số điện thoại';
                                }
                                return null;
                              },
                            ),
                          ),
                          FlatButton(
                            child: Text(
                              "Gửi mã",
                              style: TextStyle(
                                  color: Colors.blueAccent, fontSize: 18),
                            ),
                            onPressed: () async {
                              if (_phoneController.text.isEmpty) {
                                return;
                              }
                              Navigator.pop(context, _phoneController.text);
                            },
                          )
                        ],
                      );
                    });
                final phoneFinal = phoneNumber;
                _auth.verifyPhoneNumber(
                    phoneNumber: phoneNumber,
                    timeout: Duration(seconds: 60),
                    verificationCompleted: (AuthCredential authCredential) {
                      print('phone auto');
                      _handleCredential(authCredential);
                    },
                    verificationFailed: (AuthException authException) {
                      print('phone fail ' + authException.message);
                    },
                    codeSent: (String verificationId,
                        [int forceResendingToken]) async {
                      print('phone sent');
                      _phoneController.text = "";
                      String otp = await showDialog<String>(
                          context: context,
                          builder: (BuildContext context) {
                            return SimpleDialog(
                              title: Text(
                                  'Nhập mã OTP đã gửi vào số ' + phoneFinal),
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(
                                      left: Const.MARGIN + 10,
                                      right: Const.MARGIN + 10,
                                      bottom: Const.MARGIN),
                                  child: TextFormField(
                                    controller: _phoneController,
                                    decoration: InputDecoration(
                                      hintText: "Mã OTP",
                                      hintStyle: Const.styleHint,
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Vui lòng nhập số điện thoại';
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                                FlatButton(
                                  child: Text(
                                    "Xác thực",
                                    style: TextStyle(
                                        color: Colors.blueAccent, fontSize: 18),
                                  ),
                                  onPressed: () async {
                                    if (_phoneController.text.isEmpty) {
                                      return;
                                    }
                                    Navigator.pop(
                                        context, _phoneController.text);
                                  },
                                )
                              ],
                            );
                          });
                      print('phone otp:' + otp);
                      AuthCredential credential = PhoneAuthProvider.getCredential(verificationId: verificationId, smsCode: otp);
                      final FirebaseUser user =
                          (await _auth.signInWithCredential(credential)).user;

                      if (user == null) return;

                      print('phone login ' + user.uid + '   phone:' + user.phoneNumber);

                      Dialogs.showLoadingDialog(context, _keyLoader);

                      _users.document(user.uid).get().then((value) async {
                        if (value.exists) {
                          UserModel user = UserModel.fromMap(value.data, value.documentID);
                          await PreferencesHelper.setString(
                              Const.KEY_CACHED_USER, jsonEncode(user.toMap()));

                          UserModel userCached = UserModel.fromMap(
                              jsonDecode(
                                  await PreferencesHelper.getString(Const.KEY_CACHED_USER)),
                              "");
                          print('user cached name ${userCached.firstName}');
                        } else {
                          List<String> names;
                          if (user.displayName == null) {
                            names = ['TroLaNha', 'User'];
                          } else {
                            names = user.displayName.split(' ');
                          }
                          UserModel newUser = UserModel(
                              firstName: names.length > 0 ? names[0] : '',
                              lastName: names.length > 1 ? names[1] : '',
                              avatar: user.photoUrl,
                              phoneNumber: user.phoneNumber);
                          await _users.document(user.uid).setData(newUser.toMap());
                          print('user added');
                        }
                        //
                        _firebaseMessaging.getToken().then((String token) async {
                          assert(token != null);
                          print("fcm token: $token");
                          FirebaseUser user = await FirebaseAuth.instance.currentUser();
                          if (user != null) {
                            Firestore.instance
                                .collection("users")
                                .document(user.uid)
                                .updateData({'fcmToken': token});
                          }
                        });

                        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                        Navigator.of(context).pop(true);
                      }).catchError((error) {
                        Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                      });
                    },
                    codeAutoRetrievalTimeout: null);
              },
              child: Material(
                color: Colors.blueAccent,
                borderRadius: BorderRadius.all(Radius.circular(8)),
                elevation: 4,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Text(
                      "Đăng nhập bằng số điện thoại",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    Positioned(
                      left: 20,
                      child: Icon(
                        Icons.phone_android,
                        color: Colors.white,
                        size: 20,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Container(
            width: 1000,
            margin: EdgeInsets.only(
                left: Const.MARGIN_LARGE,
                right: Const.MARGIN_LARGE,
                bottom: Const.MARGIN),
            height: 45,
            child: GestureDetector(
              onTap: () {
                _handleSignIn();
              },
              child: Material(
                color: Colors.white,
                elevation: 2,
                borderRadius: BorderRadius.all(Radius.circular(8)),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Text(
                      "Đăng nhập bằng Google",
                      style: TextStyle(
                          color: Colors.black87, fontWeight: FontWeight.bold),
                    ),
                    Positioned(
                      left: 20,
                      child: Image.asset("assets/ic_google.png",
                          width: 18, height: 18),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            width: 1000,
            margin: EdgeInsets.only(
                left: Const.MARGIN_LARGE,
                right: Const.MARGIN_LARGE,
                bottom: Const.MARGIN_LARGE * 2),
            height: 45,
            child: GestureDetector(
              onTap: () {
                _handleSignInFacebook();
              },
              child: Material(
                elevation: 4,
                color: Color(0xff4267B2),
                borderRadius: BorderRadius.all(Radius.circular(8)),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Text(
                      "Đăng nhập bằng Facebook",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    Positioned(
                      left: 20,
                      child: Image.asset("assets/ic_facebook.png",
                          width: 18, height: 18),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  _handleCredential(AuthCredential credential) async {
    final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;

    print("signed in " + user.uid);

    _users.document(user.uid).get().then((value) async {
      if (value.exists) {
        UserModel user = UserModel.fromMap(value.data, value.documentID);
        await PreferencesHelper.setString(
            Const.KEY_CACHED_USER, jsonEncode(user.toMap()));

        UserModel userCached = UserModel.fromMap(
            jsonDecode(
                await PreferencesHelper.getString(Const.KEY_CACHED_USER)),
            "");
        print('user cached name ${userCached.firstName}');
      } else {
        print('displayname ' + user.displayName);
        List<String> names = user.displayName.split(' ');
        UserModel newUser = UserModel(
            firstName: names.length > 0 ? names[0] : '',
            lastName: names.length > 1 ? names[1] : '',
            avatar: user.photoUrl,
            phoneNumber: user.phoneNumber);
        await _users.document(user.uid).setData(newUser.toMap());
        print('user added');
      }
      //
      _firebaseMessaging.getToken().then((String token) async {
        assert(token != null);
        print("fcm token: $token");
        FirebaseUser user = await FirebaseAuth.instance.currentUser();
        if (user != null) {
          Firestore.instance
              .collection("users")
              .document(user.uid)
              .updateData({'fcmToken': token});
        }
      });

      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      Navigator.of(context).pop(true);
    }).catchError((error) {
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
    });
  }

  Future<void> _handleSignInFacebook() async {
    final facebookLogin = FacebookLogin();
    final result = await facebookLogin.logIn(['email', 'public_profile']);
    if (result == null ||
        result.accessToken == null ||
        result.accessToken.token == null) {
      print("fb error:" + result.errorMessage);
      return;
    }
    Dialogs.showLoadingDialog(context, _keyLoader);

    print("fb token " + result.accessToken.token);

    final AuthCredential credential = FacebookAuthProvider.getCredential(
      accessToken: result.accessToken.token,
    );

    final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;

    print("signed in " + user.uid);

    _users.document(user.uid).get().then((value) async {
      if (value.exists) {
        UserModel user = UserModel.fromMap(value.data, value.documentID);
        await PreferencesHelper.setString(
            Const.KEY_CACHED_USER, jsonEncode(user.toMap()));

        UserModel userCached = UserModel.fromMap(
            jsonDecode(
                await PreferencesHelper.getString(Const.KEY_CACHED_USER)),
            "");
        print('user cached name ${userCached.firstName}');
      } else {
        print('displayname ' + user.displayName);
        List<String> names = user.displayName.split(' ');
        UserModel newUser = UserModel(
            firstName: names.length > 0 ? names[0] : '',
            lastName: names.length > 1 ? names[1] : '',
            avatar: user.photoUrl,
            phoneNumber: user.phoneNumber);
        await _users.document(user.uid).setData(newUser.toMap());
        print('user added');
      }
      //
      _firebaseMessaging.getToken().then((String token) async {
        assert(token != null);
        print("fcm token: $token");
        FirebaseUser user = await FirebaseAuth.instance.currentUser();
        if (user != null) {
          Firestore.instance
              .collection("users")
              .document(user.uid)
              .updateData({'fcmToken': token});
        }
      });

      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      Navigator.of(context).pop(true);
    }).catchError((error) {
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
    });

    return user;
  }

  Future<void> _handleSignIn() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    if (googleUser == null) return;
    Dialogs.showLoadingDialog(context, _keyLoader);

    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;

    print("signed in " + user.uid);

    _users.document(user.uid).get().then((value) async {
      if (value.exists) {
        UserModel user = UserModel.fromMap(value.data, value.documentID);
        await PreferencesHelper.setString(
            Const.KEY_CACHED_USER, jsonEncode(user.toMap()));

        UserModel userCached = UserModel.fromMap(
            jsonDecode(
                await PreferencesHelper.getString(Const.KEY_CACHED_USER)),
            "");
        print('user cached name ${userCached.firstName}');
      } else {
        List<String> names = user.displayName.split(' ');
        UserModel newUser = UserModel(
            firstName: names.length > 0 ? names[0] : '',
            lastName: names.length > 1 ? names[1] : '',
            avatar: user.photoUrl,
            phoneNumber: user.phoneNumber);
        await _users.document(user.uid).setData(newUser.toMap());
        print('user added');
      }
      //
      _firebaseMessaging.getToken().then((String token) async {
        assert(token != null);
        print("fcm token: $token");
        FirebaseUser user = await FirebaseAuth.instance.currentUser();
        if (user != null) {
          Firestore.instance
              .collection("users")
              .document(user.uid)
              .updateData({'fcmToken': token});
        }
      });

      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
      Navigator.of(context).pop(true);
    }).catchError((error) {
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
    });

    return user;
  }
}
