import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';

import '../../Const.dart';

class PlaceSearchWidget extends StatefulWidget {
  final Position currentPosition;

  PlaceSearchWidget({this.currentPosition});

  @override
  State<StatefulWidget> createState() => FriendSearchState();
}

class FriendSearchState extends State<PlaceSearchWidget> {
  final _searchController = TextEditingController();
  final _places =
      new GoogleMapsPlaces(apiKey: "AIzaSyBB-sESaK33kjh5mPFHIVYWmAaNL3PfbkA");
  List<Prediction> _predictions = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Tìm kiếm địa điểm",
          style: TextStyle(fontSize: 17),
        ),
        centerTitle: true,
      ),
      body: Container(
        child: Material(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.all(Const.MARGIN),
                decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                alignment: Alignment.center,
                height: 40,
                child: TextFormField(
                  controller: _searchController,
                  onChanged: (value) {
                    print("ok ok:" + _searchController.text);
                    if (_searchController.text.isEmpty) return;
                    _places
                        .autocomplete(
                      _searchController.text,
                      language: "vi",
                      radius: 50000,
                      location: widget.currentPosition == null
                          ? null
                          : Location(widget.currentPosition.latitude,
                              widget.currentPosition.longitude),
                    )
                        .then(
                      (value) {
                        setState(() {
                          _predictions = value.predictions;
                        });
                        print("count _predictions: " +
                            _predictions.length.toString() +
                            "  " +
                            widget.currentPosition.toString());
                      },
                    );
                  },
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.only(top: 7, bottom: 5, right: Const.MARGIN),
                    border: InputBorder.none,
                    hintText: "Nhập địa chỉ cần tìm",
                    prefixIcon: Icon(
                      Icons.search,
                      size: 24,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(
                      left: Const.MARGIN * 2, right: Const.MARGIN * 2),
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      return _buildSuggestItemWidget(index);
                    },
                    itemCount: _predictions.length,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSuggestItemWidget(int index) {
    return InkWell(
      onTap: () {
        _places
            .getDetailsByPlaceId(_predictions[index].placeId, language: "vi")
            .then((value) {
          Navigator.of(context).pop(LatLng(value.result.geometry.location.lat,
              value.result.geometry.location.lng));
        });
      },
      child: Container(
        decoration: BoxDecoration(),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 12, bottom: 12),
              child: Row(
                children: [
                  Icon(
                    Icons.location_on,
                    color: Colors.grey,
                    size: 20,
                  ),
                  Flexible(
                    child: Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        _predictions[index].description,
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 15,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 0.5,
              decoration: BoxDecoration(
                color: Colors.grey.withAlpha(50),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
