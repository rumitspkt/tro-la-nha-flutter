import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:trolanha/Const.dart';
import 'package:trolanha/customs/dialogs.dart';
import 'package:trolanha/models/friend_filter_model.dart';
import 'package:trolanha/models/friend_model.dart';
import 'package:trolanha/models/user_model.dart';
import 'package:trolanha/widgets/friend_search/friend_search_widget.dart';
import 'package:url_launcher/url_launcher.dart';

import '../enums.dart';
import '../helper.dart';

class HomeFriendWidget extends StatefulWidget {
  const HomeFriendWidget({Key key}) : super(key: key);

  @override
  HomeFriendState createState() => HomeFriendState();
}

class HomeFriendState extends State<HomeFriendWidget> {
  List<FriendModel> _friends = [];
  int numberOfFilter = 0;
  FriendFilterModel selectedFilter;

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  RefreshController _refreshController = RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
  }

  Future<void> fetchFriendList() async {
    QuerySnapshot query = await Firestore.instance
        .collection("friends")
        .orderBy('lastUpdatedAt', descending: true)
        .getDocuments();
    List<FriendModel> friends = [];
    for (int i = 0; i < query.documents.length; i++) {
      DocumentSnapshot doc = query.documents[i];
      FriendModel room = FriendModel.fromMap(doc.data, doc.documentID);
      friends.add(room);
    }
    setState(() {
      _friends = friends;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(color: Colors.blueAccent),
          width: 1000,
          height: 40,
        ),
        Container(
          width: 1000,
          height: 40,
          decoration: BoxDecoration(color: Colors.blueAccent),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Positioned(
                left: Const.MARGIN,
                child: Text(
                  "Có ${_friends.length} bạn hợp ở ghép",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 16),
                ),
              ),
              Positioned(
                top: 0,
                right: 20,
                child: Row(
                  children: [
                    Text(
                      "Lọc",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(),
                      child: Stack(
                        children: [
                          IconButton(
                            icon: Icon(
                              Icons.filter_list,
                              color: Colors.white,
                              size: 25,
                            ),
                            onPressed: () async {
                              selectedFilter =
                                  await Navigator.of(context)
                                      .push(MaterialPageRoute(
                                builder: (context) => FriendSearchWidget(),
                              ));
                              if (selectedFilter != null) {
                                handleSearchWithFilter();
                              } else {
                                numberOfFilter = 0;
                              }
                            },
                          ),
                          if (numberOfFilter > 0)
                            Positioned(
                              top: 0,
                              right: 0,
                              child: Container(
                                width: 20,
                                height: 20,
                                decoration: BoxDecoration(
                                    color: Colors.redAccent,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                alignment: Alignment.center,
                                child: Text(
                                  "$numberOfFilter",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12),
                                ),
                              ),
                            )
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Expanded(
          child: SmartRefresher(
            onRefresh: () async {
              if (selectedFilter != null) {
                await handleSearchWithFilter();
              } else {
                await fetchFriendList();
              }
              _refreshController.refreshCompleted();
            },
            controller: _refreshController,
            child: ListView.builder(
              itemBuilder: (context, index) {
                return _buildFriendItem(index);
              },
              itemCount: _friends.length,
            ),
          ),
        ),
      ],
    );
  }

  Future<void> handleSearchWithFilter() async {
    setState(() {
      this.numberOfFilter =
          selectedFilter.numberOfFilter;
    });
    QuerySnapshot value = await selectedFilter.query.getDocuments();
    List<FriendModel> friends = [];
    for (int i = 0;
    i < value.documents.length;
    i++) {
      DocumentSnapshot doc = value.documents[i];
      FriendModel room = FriendModel.fromMap(
          doc.data, doc.documentID);
      friends.add(room);
    }
    setState(() {
      _friends = friends;
    });
  }

  Future<void> _askForContact(int index) async {
    Dialogs.showLoadingDialog(context, _keyLoader);

    FriendModel friendModel = _friends[index];

    DocumentSnapshot doc = await Firestore.instance
        .collection("users")
        .document(friendModel.id)
        .get();
    UserModel user = UserModel.fromMap(doc.data, doc.documentID);

    Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();

    showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: const Text('Liên hệ qua'),
            children: [
              SimpleDialogOption(
                child: _buildOptionWidget("Facebook"),
                onPressed: () async {
                  if (user != null &&
                      user.facebook != null &&
                      user.facebook.isNotEmpty) {
                    print(user.facebook);
                    if (await canLaunch(user.facebook)) {
                      await launch(user.facebook);
                    }
                  }
                  Navigator.pop(context);
                },
              ),
              SimpleDialogOption(
                child: _buildOptionWidget("Nhắn tin SMS"),
                onPressed: () async {
                  if (user != null &&
                      user.phoneNumber != null &&
                      user.phoneNumber.isNotEmpty) {
                    if (await canLaunch('sms:${user.phoneNumber}')) {
                      await launch('sms:${user.phoneNumber}');
                    }
                  }
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  Widget _buildOptionWidget(String title) {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 5),
      child: Text(
        title,
        style: TextStyle(
          color: Colors.black87,
          fontSize: 15,
        ),
      ),
    );
  }

  Widget _buildFriendItem(int index) {
    FriendModel friend = _friends[index];
    return InkWell(
      onTap: () {
        _askForContact(index);
      },
      child: Container(
        padding:
            EdgeInsets.fromLTRB(Const.MARGIN, Const.MARGIN, Const.MARGIN, 0),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, Const.MARGIN),
              child: Row(
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        child: Image.network(
                          friend.avatarSnippet == null
                              ? "https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_1280.png"
                              : friend.avatarSnippet,
                          fit: BoxFit.cover,
                          width: 120,
                          height: 180,
                        ),
                      ),
                      Container(
                        width: 120,
                        height: 180,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.bottomCenter,
                            end: Alignment(0, 0.2),
                            colors: [Colors.black, Colors.transparent],
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                      ),
                      Positioned(
                        bottom: 8,
                        child: Container(
                          child: Text(
                            "${Helper.getJobDisplayText(friend.jobSnippet)}, ${Timestamp.now().toDate().year - friend.birthDateSnippet.toDate().year}"
                                .toUpperCase(),
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                              fontSize: 13,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  Expanded(
                    child: Container(
                      height: 180,
                      padding: EdgeInsets.fromLTRB(
                          Const.MARGIN, Const.MARGIN, 0, Const.MARGIN),
                      alignment: Alignment.topLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            friend.nameSnippet,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontWeight: FontWeight.w600),
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Khoản chi",
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 13),
                                ),
                                Text(
                                  "${formatPrice(friend.minimumPrice)} - ${formatPrice(friend.maximumPrice)}",
                                  style: TextStyle(
                                      color: Colors.blueAccent,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w600),
                                )
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Tìm",
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 13),
                                ),
                                Text(
                                  (friend.gender == Gender.all
                                      ? "Nam/Nữ"
                                      : Helper.getDisplayTextGender(
                                          friend.gender)),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w600),
                                )
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Vị trí tìm kiếm",
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 13),
                                ),
                                Text(
                                  friend.address.district.displayText,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w600),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 1,
              decoration: BoxDecoration(
                color: Colors.grey[300],
              ),
            )
          ],
        ),
      ),
    );
  }

  String formatPrice(int price) {
    var formatter = new NumberFormat("##,###,###");
    return formatter.format(price) + "đ";
  }
}
