import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierColor: Colors.black38,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  children: <Widget>[
                    Center(
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Lottie.asset(
                            'assets/loading_indicator.json',
                            width: 70,
                            height: 70,
                            repeat: true,
                          ),
                          Positioned(
                            top: 25,
                            child: Lottie.asset(
                              'assets/loading_indicator_2.json',
                              width: 90,
                              height: 90,
                              repeat: true,
                            ),
                          ),
                        ],
                      ),
                    )
                  ]));
        });
  }
}
