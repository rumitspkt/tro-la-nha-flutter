import 'dart:math';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:trolanha/enums.dart';

class Helper {
  static List<String> convertListUtilitiesToString(List<RoomUtility> list) {
    List<String> results = [];
    for (int i = 0; i < list.length; i++) {
      results.add(list[i].enumValue);
    }
    return results;
  }

  static bool isToday(DateTime dateTime) {
    final now = DateTime.now();
    return now.day == dateTime.day &&
        now.month == dateTime.month &&
        now.year == dateTime.year;
  }

  static bool isYesterday(DateTime dateTime) {
    final yesterday = DateTime.now().subtract(Duration(days: 1));
    return yesterday.day == dateTime.day &&
        yesterday.month == dateTime.month &&
        yesterday.year == dateTime.year;
  }

  static String getDisplayTextRoomType(RoomType roomType) {
    switch (roomType) {
      case RoomType.dorm:
        return "Ký túc xá/Home stay";
      case RoomType.rent:
        return "Phòng cho thuê";
      case RoomType.shared:
        return "Phòng ở ghép";
      case RoomType.whole:
        return "Nhà nguyên căn";
      case RoomType.apartment:
        return "Căn hộ";
    }
    return "";
  }

  static String getDisplayTextGender(Gender gender) {
    switch (gender) {
      case Gender.all:
        return "Tất cả";
      case Gender.male:
        return "Nam";
      case Gender.female:
        return "Nữ";
    }
    return "";
  }

  static String getDisplayTextUnicodeGender(Gender gender) {
    switch (gender) {
      case Gender.all:
        return "♀/♂";
      case Gender.male:
        return "♀";
      case Gender.female:
        return "♂";
    }
    return "";
  }

  static String getUnitTextRoomType(RoomType roomType) {
    switch (roomType) {
      case RoomType.dorm:
        return "người";
      case RoomType.rent:
        return "phòng";
      case RoomType.shared:
        return "người";
      case RoomType.whole:
        return "căn";
      case RoomType.apartment:
        return "căn";
    }
    return "";
  }

  static String getJobDisplayText(String job) {
    String result = '';
    switch (job) {
      case 'worker':
        result = 'Công nhân';
        break;
      case 'officer':
        result = 'Văn phòng';
        break;
      case 'student':
        result = 'Sinh viên';
        break;
      case 'schoolboy':
        result = 'Học sinh';
        break;
      case 'free':
        result = 'Nghề tự do';
        break;
    }
    return result;
  }

  static double calculateDistance(LatLng pos1, LatLng pos2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((pos2.latitude - pos1.latitude) * p) / 2 +
        c(pos1.latitude * p) *
            c(pos2.latitude * p) *
            (1 - c((pos2.longitude - pos1.longitude) * p)) /
            2;
    return 12742 * asin(sqrt(a));
  }
}
