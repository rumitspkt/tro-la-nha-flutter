import 'package:cloud_firestore/cloud_firestore.dart';

class UserModel {
  String id;
  bool isAdmin;
  bool isPostedRoom;
  String firstName;
  String lastName;
  String avatar;
  String about;
  String phoneNumber;
  String facebook;
  String job;
  Timestamp birthDate;
  String gender;
  int birthDateInMillis;

  UserModel(
      {this.isAdmin,
      this.firstName,
      this.lastName,
      this.avatar,
      this.about,
      this.phoneNumber,
      this.facebook});

  UserModel.fromMap(Map data, String id)
      : id = id ?? '',
        isAdmin = data['isAdmin'],
        isPostedRoom = data['isPostedRoom'],
        firstName = data['firstName'],
        lastName = data['lastName'],
        avatar = data['avatar'],
        about = data['about'],
        phoneNumber = data['phoneNumber'],
        facebook = data['facebook'],
        job = data['job'],
        gender = data['gender'],
        birthDateInMillis = data['birthDateInMillis'],
        birthDate = data['birthDate'];

  toMap() {
    return {
      'id': id,
      'isAdmin': isAdmin,
      'isPostedRoom': isPostedRoom,
      'firstName': firstName,
      'lastName': lastName,
      'avatar': avatar,
      'about': about,
      'phoneNumber': phoneNumber,
      'facebook': facebook,
      'job': job,
      'gender': gender,
      'birthDateInMillis': birthDateInMillis
    };
  }
}
