import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:trolanha/enums.dart';
import 'package:trolanha/helper.dart';
import 'package:trolanha/models/address_model.dart';
import 'package:trolanha/models/user_model.dart';
import 'package:trolanha/repository.dart';

class RoomModel {
  String uuid;
  String title;
  String description;
  RoomType type;
  int quantity;
  int capacity;
  Gender gender;
  int area;
  int price;
  int deposit;
  int priceElectricPerUnit;
  int priceWaterPerUnit;
  int priceInternet;
  int priceParking;
  AddressModel address;
  List<RoomUtility> utilities;
  List<String> images;
  UserModel owner;
  LatLng latLng;
  String user;
  Timestamp lastUpdatedAt;
  bool isApproved;
  bool isVerified;
  String reason;
  double distance;

  RoomModel(
      {this.type,
      this.uuid,
      this.title,
      this.description,
      this.quantity,
      this.capacity,
      this.gender,
      this.area,
      this.price,
      this.deposit,
      this.priceElectricPerUnit,
      this.priceWaterPerUnit,
      this.priceInternet,
      this.priceParking,
      this.address,
      this.utilities,
      this.images,
      this.latLng,
      this.owner});

  toMap() {
    return {
      'title': title,
      'description': description,
      'type': type.enumValue,
      'quantity': quantity,
      'capacity': capacity,
      'gender': gender.enumValue,
      'area': area,
      'price': price,
      'deposit': deposit,
      'priceElectricPerUnit': priceElectricPerUnit,
      'priceWaterPerUnit': priceWaterPerUnit,
      'priceInternet': priceInternet,
      'priceParking': priceParking,
      'addressDescription': address.addressDescription,
      'addresses': [address.city.id, address.district.id],
      'utilities': Helper.convertListUtilitiesToString(utilities),
      'images': images,
      'user': user,
      'location': latLng == null
          ? GeoPoint(0, 0)
          : new GeoPoint(latLng.latitude, latLng.longitude),
      'isApproved': isApproved,
      'isGoodPrice': false,
      'isVerified': isVerified,
      'lastUpdatedAt': lastUpdatedAt,
      'reason': reason,
    };
  }

  RoomModel.fromMap(Map data, String id)
      : uuid = id ?? '',
        title = data['title'],
        description = data['description'],
        type = EnumToString.fromString(RoomType.values, data['type']),
        quantity = data['quantity'],
        capacity = data['capacity'],
        gender = EnumToString.fromString(Gender.values, data['gender']),
        area = data['area'],
        price = data['price'],
        deposit = data['deposit'],
        priceElectricPerUnit = data['priceElectricPerUnit'],
        priceWaterPerUnit = data['priceWaterPerUnit'],
        priceInternet = data['priceInternet'],
        priceParking = data['priceParking'],
        address = getAddressFromMap(data),
        utilities = getUtilitiesFromMap(data),
        images = data['images'].cast<String>(),
        latLng = getLatLngFromMap(data),
        user = data['user'],
        lastUpdatedAt = data['lastUpdatedAt'],
        isApproved = data['isApproved'],
        isVerified = data['isVerified'],
        reason = data['reason'];

  static AddressModel getAddressFromMap(Map data) {
    AddressModel address = AddressModel();
    address.addressDescription = data['addressDescription'];
    List<String> addresses = data['addresses'].cast<String>();
    if (addresses != null) {
      if (addresses.length > 0) {
        address.city = Repository.shared.city(addresses[0]);
      }
      if (addresses.length > 1) {
        address.district =
            Repository.shared.district(addresses[0], addresses[1]);
      }
    }
    return address;
  }

  static List<RoomUtility> getUtilitiesFromMap(Map data) {
    if (data['utilities'] == null) return [];
    List<String> utilities = data['utilities'].cast<String>();
    return utilities
        .map((e) => EnumToString.fromString(RoomUtility.values, e))
        .toList();
  }

  static LatLng getLatLngFromMap(Map data) {
    GeoPoint geoPoint = data['location'];
    if (geoPoint == null) return null;
    return LatLng(geoPoint.latitude, geoPoint.longitude);
  }
}
