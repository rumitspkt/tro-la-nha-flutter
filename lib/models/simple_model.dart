import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class SimpleModel {
  String id;
  String text;
  String displayText;
  String image;
  List<SimpleModel> items = [];
  List<String> banners = [];
  LatLng centerPoint;

  SimpleModel(this.id, this.displayText);

  SimpleModel.fromMap(Map data, String id)
      : id = data['id'] ?? id ?? '',
        text = data['text'],
        image = data['image'],
        displayText = data['displayText'],
        centerPoint = getLatLngFromMap(data),
        items = data['items'] == null
            ? []
            : data['items']
                .map((item) => SimpleModel.fromMap(item, ''))
                .toList(),
        banners = data['banners'] == null
            ? []
            : data['banners'].cast<String>();

  toJson() {
    return {'id': id, 'text': text, 'displayText': displayText, 'items': items};
  }

  static LatLng getLatLngFromMap(Map data) {
    GeoPoint geoPoint = data['centerPoint'];
    if (geoPoint == null) return null;
    return LatLng(geoPoint.latitude, geoPoint.longitude);
  }

}
