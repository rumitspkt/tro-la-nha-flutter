import 'package:cloud_firestore/cloud_firestore.dart';

class InviteModel {
  String id;
  String room;
  String roomTitleSnippet;
  String owner;
  String user;
  String userNameSnippet;
  String userPhoneSnippet;
  bool isSeen;
  String message;
  Timestamp lastUpdatedAt;

  InviteModel(
      {this.id,
      this.room,
      this.roomTitleSnippet,
      this.owner,
      this.user,
      this.userNameSnippet,
      this.userPhoneSnippet,
      this.isSeen,
      this.message,
      this.lastUpdatedAt});

  InviteModel.fromMap(Map data, String id)
      : id = data['id'] ?? id ?? '',
        room = data['room'],
        roomTitleSnippet = data['roomTitleSnippet'],
        user = data['user'],
        owner = data['owner'],
        userNameSnippet = data['userNameSnippet'],
        userPhoneSnippet = data['userPhoneSnippet'],
        isSeen = data['isSeen'],
        message = data['message'],
        lastUpdatedAt = data['lastUpdatedAt'];

  toJson() {
    return {
      'id': id,
      'room': room,
      'roomTitleSnippet': roomTitleSnippet,
      'user': user,
      'owner': owner,
      'userNameSnippet': userNameSnippet,
      'userPhoneSnippet': userPhoneSnippet,
      'isSeen': isSeen,
      'message': message,
      'lastUpdatedAt': lastUpdatedAt,
    };
  }
}
