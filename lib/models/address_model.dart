import 'package:trolanha/models/simple_model.dart';

class AddressModel {
  SimpleModel city;
  SimpleModel district;
  String addressDescription;

  AddressModel({this.city, this.district});
}