import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:trolanha/enums.dart';
import 'package:trolanha/helper.dart';
import 'package:trolanha/models/address_model.dart';
import 'package:trolanha/repository.dart';

class FriendModel {
  String id;
  Gender gender;
  AddressModel address;
  List<RoomUtility> utilities;
  Timestamp lastUpdatedAt;
  int maximumPrice;
  int minimumPrice;
  String nameSnippet;
  String jobSnippet;
  String avatarSnippet;
  Timestamp birthDateSnippet;

  FriendModel(
      {this.id,
      this.gender,
      this.address,
      this.utilities,
      this.lastUpdatedAt,
      this.nameSnippet,
      this.jobSnippet,
      this.avatarSnippet,
      this.birthDateSnippet,
      this.maximumPrice,
      this.minimumPrice});

  toMap() {
    return {
      'gender': gender.enumValue,
      'maximumPrice': maximumPrice,
      'minimumPrice': minimumPrice,
      'nameSnippet': nameSnippet,
      'jobSnippet': jobSnippet,
      'avatarSnippet': avatarSnippet,
      'birthDateSnippet': birthDateSnippet,
      'addresses': [address.city.id, address.district.id],
      'utilities': Helper.convertListUtilitiesToString(utilities),
      'lastUpdatedAt': lastUpdatedAt,
    };
  }

  FriendModel.fromMap(Map data, String id)
      : id = id ?? '',
        maximumPrice = data['maximumPrice'],
        minimumPrice = data['minimumPrice'],
        nameSnippet = data['nameSnippet'],
        jobSnippet = data['jobSnippet'],
        avatarSnippet = data['avatarSnippet'],
        birthDateSnippet = data['birthDateSnippet'],
        gender = EnumToString.fromString(Gender.values, data['gender']),
        address = getAddressFromMap(data),
        utilities = getUtilitiesFromMap(data),
        lastUpdatedAt = data['lastUpdatedAt'];

  static AddressModel getAddressFromMap(Map data) {
    AddressModel address = AddressModel();
    address.addressDescription = data['addressDescription'];
    List<String> addresses = data['addresses'].cast<String>();
    if (addresses != null) {
      if (addresses.length > 0) {
        address.city = Repository.shared.city(addresses[0]);
      }
      if (addresses.length > 1) {
        address.district =
            Repository.shared.district(addresses[0], addresses[1]);
      }
    }
    return address;
  }

  static List<RoomUtility> getUtilitiesFromMap(Map data) {
    if (data['utilities'] == null) return [];
    List<String> utilities = data['utilities'].cast<String>();
    return utilities
        .map((e) => EnumToString.fromString(RoomUtility.values, e))
        .toList();
  }
}
