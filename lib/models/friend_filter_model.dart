import 'package:cloud_firestore/cloud_firestore.dart';

class FriendFilterModel {

  Query query;
  int numberOfFilter;

  FriendFilterModel(this.query, this.numberOfFilter);
}